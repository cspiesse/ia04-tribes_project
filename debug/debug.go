package debug

import (
	"fmt"
	"tribes"
	"tribes/agents/caravan"
	"tribes/main_server/mapagent"
	"tribes/main_server/server"
)

func Debug() {

	launchSimu := 1

	serverPort := "8080"
	serverAddress := "http://localhost:" + serverPort + "/"

	caravansAddr := make(map[tribes.CaravanID]string)
	caravansInfo := make(map[tribes.CaravanID]server.SimplifiedCaravanInfo)

	if launchSimu == 1 {

		// ------ 2 Caravan simulation ------ //
		strats, positions := Make2Caravans()
		caravansAddr, caravansInfo = caravan.MakeCaravanes(4, serverAddress, strats, positions)

	} else if launchSimu == 2 {

		// ------ Random caravans simu ------ //
		// 8 Caravanes maximum (à cause du front)
		caravansAddr, caravansInfo = caravan.MakeCaravanes(8, serverAddress, []caravan.CaravanStrategy{}, []tribes.Position{})

	}

	// var m mapagent.Map
	// mapp, _ := os.ReadFile("map.json")
	// _ = json.Unmarshal(mapp, &m)

	m := mapagent.RandomMapGeneration(tribes.Height, tribes.Width, tribes.VanillaBiomes, tribes.EventBiomes, tribes.DefaultBiomeRatio)

	grid := mapagent.Map{
		Matrix:      m.Matrix,
		Biomes:      m.Biomes,
		EventBiomes: m.EventBiomes}

	// data, _ := json.Marshal(m)
	// fmt.Println(string(data))
	// f, _ := os.Create("map.json")
	// f.WriteString(string(data))
	// f.Close()

	rsa := server.NewRestWebService("toto", serverPort, caravansAddr, grid, caravansInfo)

	go rsa.Start()
	//go rsa.LaunchSimulation()

	fmt.Scanln()
}

func Make2Caravans() (strats []caravan.CaravanStrategy, positions []tribes.Position) {
	// -- Caravan 1 --
	caravane1Strat := caravan.CaravanStrategy{
		"Agressive": 12,
		"Defensive": 23,
		"Gathering": 51,
		"Knowledge": 14}
	pos1 := tribes.Position{X: 2, Y: 3}

	strats = append(strats, caravane1Strat)
	positions = append(positions, pos1)

	// -- Caravan 2 --
	caravane2Strat := caravan.CaravanStrategy{
		"Agressive": 100,
		"Defensive": 0,
		"Gathering": 0,
		"Knowledge": 0}
	pos2 := tribes.Position{X: 10, Y: 10}

	positions = append(positions, pos2)
	strats = append(strats, caravane2Strat)

	return
}
