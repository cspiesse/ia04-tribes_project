# IA04-Tribes_Project

Lien du drive : https://drive.google.com/drive/folders/1NkWe-UNxGgfsW0QJJ8SZwTTf3TpMQf0Y?usp=share_link

Lien du git : https://gitlab.utc.fr/cspiesse/ia04-tribes_project

# I - Installation et lancement de la simulation

**Etape préalable :**
- Si besoin installer `yarn` sur la machine

**Effectuer les étapes dans cet ordre :**

### Lancer le serveur/simulation principale
- Ouvrir un terminal dans le dossier principal
```bash
go run .\tribes\tribes.go
```

### Lancer l'interface graphique
- Ouvrir un nouveau terminal
```bash
cd frontend
yarn # au moins une fois pour installer les dépendances
yarn start # pour lancer le serveur web
```

- La fenêtre s'ouvre directement dans votre navigateur, sinon aller à l'adresse http://localhost:3000/

### Lancer la simulation
- Sur l'interface dans le navigateur, cliquer sur le bouton `Launch Simulation`. 

Si la simulation ne se lance pas, verifiez que les différents ponts HTTP se sont bien lancés du côté du _backend_ en Go, puis rafraîchir la page web.
Une série de logs devrait s'afficher sur le terminal une fois la simulation lancée.

## Les paramètres de la simulation

Les paramètres de la simulation se situent dans le fichier `parameters.go`, dans la première section _"Parameters to play with"_. 
Voici une liste de paramètres avec lesquels nous pouvons facilement gérer la simulation :

### La carte :
- **DayDuration** (défaut : 40) : Le nombre de tours que dure un jour (Permet aux petitpois d'effectuer plus d'actions et donc de se déplacer plus loin chaque jour).
- **Height** (défaut : 15) : La hauteur de la carte.
- **Width** (défaut : 15) : La largeur de la carte.
- **NbCaravan** (entre 1 et 8) : Le nombre de tribus (caravanes) à initialiser pour la simulation.

### La caravane :
- **NbstartMembers** (défaut 5) : Nombre de petitpois initiaux.
- **NbStartFood** (défaut 30) : Quantité de nourriture initiale.
- **NbStartMaterials** (défaut 10) : Quantité de matériaux initiale.
- **BaseMaxCaravanHP** (défaut 30) : Vie maximale de la caravane.

#### Caravanes avec une stratégie prédéfinie :

- **WhichSimulation** : Passer cette variable à **1**.

La définition de stratégies et de position initiales se fait par la fonction **MakeCaravans** dans le fichier `.\tribes\tribes.go`. 
La somme des valeurs de la structure **CaravanStrategy** doit être égale à 100.
La position intiale de la caravane doit être comprise dans la taille de la carte (débute à 0).
Si le nombre de stratégies définies est inférieur au nombre de caravanes demandées, les caravanes non définies seront générées aléatoirement.

#### Caravanes aléatoires :

- **WhichSimulation** : Passer cette variable à **2**.

Les caravanes seront générées aléatoirement.

### Les petitpois :
- **BaseMaxPetitpoisHP** (défaut 10) : la vie initiale d'un petitpois.
- **InventoryBaseSize** (défaut 6) : la taille d'inventaire initiale d'un petitpois.

Il est préférable de ne pas toucher aux autres paramètres.

# II - Règles du monde et de la simulation

## Introduction

Les petitpois sont une espèce nomade qui vivent dans le désert. Leur objectif est de survivre à leur environnement aride et dangereux en trouvant de la nourriture et des matériaux nécessaires à leur évolution. Ils se regroupent alors en petites tribus pour faire face aux dangers qui les entourent. Ces tribus doivent se partager des ressources limitées sur une sphère (tore), et sont ainsi souvent amenées à être agressives les unes envers les autres.

## Problématique

**Comment choisir les meilleures stratégies pour s'adapter et gérer les ressources dans un environnement à ressources limitées et avec des informations partielles ?**

## La carte du monde

- **Génération aléatoire :**
La carte est générée aléatoirement. Elle accueille différents biomes complètement paramétrables. Ces biomes ont une taille minimum et maximum à la génération. La génération sef ait en tirant aléatoirement des itérations des biomes depuis la liste de ceux disponibles. Grâce à des poids, le tirage fait en sorte d'avoir une quantité presque équitable de chaque biome. Ils sont ensuite placés un par un selon des positions aléatoires, à la manière de tâches d'encre qui se répandent. Lorsqu'un biome s'étale, on tire aléatoirement les cases qui sont ajoutées à la périphérie encore avec un système de poids pour centrer la génération autour du point de spawn.

- **Ressources naturelles :**
Sur leurs sols, les biomes générent quotidiennement de nouvelles ressources naturelles à une fréquence qui leur est propre. Ils ont aussi une capacité maximum de ressources et de matériaux.

- **Evènements aléatoires:**
Traverser ces biomes peut être dangereux comme profitable car des évènements aléatoires basés sur la chance arrivent aux petitpois.

## Les caravanes

La caravane est le lieu de regroupement de la tribu. C’est un espace de vie commun dans lequel les petitpois d’une tribu peuvent s’abriter des dangers de la nuit. Chaque individu est donc forcé de rentrer à la caravane avant la nuit tombée, sous peine de mourir dans la froideur du désert et de disparaître recouvert par le sable.

### Fonctions de la caravane

- Le lieu de regroupement d’une tribu dans lequel les petitpois s’abritent des dangers de la nuit.
- Le lieu de stockage des ressources.
- Elle regroupe et actualise les connaissances et l’expérience des membres de la tribu chaque soir.
- Elle décide de la stratégie de reproduction de la tribu.
- Chaque jour, la caravane déterminera des objectifs pour la journée et assigne des missions aux petitpois.
- La caravane peut se déplacer si les ressources viennent à manquer ou bien si elle aperçoit des ennemis à proximité.

### Statistiques de la caravane

Chaque action effectuée au cours d'une journée donne plus ou moins d’XP. Lorsqu’un palier est passé (coût expo pour chaque statistique), la caravane peut augmenter une statistique d’un point. Ces niveaux ne peuvent pas être perdus. Ces statistiques sont ensuite partagées à tous les petitpois lors de la journée suivante.

- **Attaque** : Un niveau augmente de 1 les dégats infligés en combat.
- **Défense** : la Défense augmente de 1 tous les 2 niveaux. Chaque niveau de défense permet d'éviter au petitpois de prendre un dégat.
- **Sagesse** : La sagesse permet d'augmenter le gain d'expérience des petitpois. Un niveau de sagesse augmente de 0.25 le coefficient multiplicateur de gain d'XP.
- **Endurance** : Un niveau d'endurance augmente de 0.25 le coefficient multiplicateur de la vie maximale d'un petitpois.
- **Perception** : Chaque niveau de perception augmente la distance de vision d'un petitpois d'une demi case.
- **Vitesse** : Chaque niveau de vitesse réduit de 1 le nombre de tours nécessaires pour se déplacer d'une case.
- **Collecte** : Chaque niveau de collecte permet de collecter 1 ressource suplémentaire par tour.
- **Chance** : Permet de perdre moins de vie ou d'objet lorsqu'un mauvais évènement arrive.

### Stratégie de la caravane

La caravane possède un plan d'action déterminé par une pondération répartie entre 4 stratégies. Cela permet ensuite à la caravane de répartir l'expérience reçu au cours de la journée, d'assigner les ressources aux différentes tâches de la nuit, puis de créer et assigner les rôles correspondants pour les petitpois. Les 4 stratégies sont les suivantes :

- **Agressive**
- **Défensive**
- **Collecte**
- **Exploration**

Ces stratégies n'évoluent pas au cours des jours, mais nous avons laissé la possibilité de créer un algoritme de récompense pour les faire varier pour chaque caravane en fonction des résultats obtenus les jours précédents.

### Croyances de la tribu 

La tribu possède deux types de croyances :

- **Croyances sur les biomes :** 

C'est la proportion de chaque ressource et de danger. Les biomes sont utilisés dans les stratégies de la caravane pour cibler des ressources en particulier ou éviter des cases dangereuses.

- **Croyances sur les cellules de la carte :** 

Ces croyances sont composées de la date, l'emplacement, le biome, la quantité de ressources, danger et les enemis et caravanes présents sur la case. Cela donne une information plus précise sur la quantité de ressource restante à l’emplacement donné.

## Les petitpois
 
Les petitpois sont les agents qui vivent dans le monde. Ils appartiennent à une tribu (donc à une caravane). Les membres d’une tribu de petitpois ne vivent que pour le bien de leur tribu. Ils écoutent aveuglément les ordres donnés par la caravane et font de leur mieux pour réaliser les objectifs journaliers qui leurs sont donnés.

Ils possèdent un carnet de notes qui leur permet de noter l'ensemble des informations qu'ils reçoivent au cours d'une journée. Ces informations sont ensuite partagées avec la caravane à la fin de la journée. Elles peuvent aussi être utilisées pour leur retour à la caravane, afin d'éviter des emplacements qui leur ont déjà été défavorables durant le jour. De plus, ils possèdent les connaissances de l'ensemble de la tribu qui leur sont transmises au début de la journée.

Les petitpois cherchent toujours à rentrer à leur caravane avant la fin de la journée. En effet, un petitpois ne peut passer la nuit en dehors de celle-ci sans mourir.

Les petitpois ont différents rôles qui leur sont assignés par la caravane. Ces rôles sont :
- *Explorateur* : ils sont missionnés par la caravane pour découvrir l'environnement. Ils voyagent donc plus vite que les autres, en empruntant le chemin le plus court, et sans s’arrêter avant d'atteindre l'objectif. Une fois cet objectif atteint, ils continuent à explorer sur des cases aléatoires tant qu'ils en ont le temps. L’explorateur est missionné pour ramener le plus de connaissances possibles. S’il aperçoit un ennemi, il cherche à l'éviter.
- *Collecteur* : ils ont pour objectif de ramener une certaine quantité de ressources à la tribu. La caravane les envoie récolter vers un lieu à haut potentiel pour la/les ressources demandées. Pour se déplacer, le collecteur va vers la position exacte que lui indique la caravane en passant par le chemin le plus court. Sur le trajet, il peut être amené à percevoir des éléments intéressants pour son objectif. Il va donc s’empresser de s’y rendre et de ramasser la quantité demandée par la caravane de ressource correspondante. Une fois cela fait, il se dirige à nouveau vers son objectif s’il n’a pas encore rempli son inventaire. Lorsqu'un collecteur meurt, son inventaire est déposé sur le sol, récupérable par d'autres collecteurs.
- *Défenseur* : ils se doivent de défendre la caravane, en restant posté dans celle-ci en attente d'une attaque.
- *Attaquant* : ils ont pour objectif de tuer des petitpois ennemis pour leur voler leurs ressources. Si une caravane est présente sans aucun ennemi autour, ils vont la prendre d'assaut pour la détruire. Si des ennemis sont postés dans la caravane en question, ils devront alors tout d'abord les éliminer.

Au début de chaque jour, les petitpois reçoivent une mission de la part de la caravane, qui leur permet de récupérer leur rôle, leurs nouvelles statistiques (dues au changement de rôle et à la montée en niveau de la caravane), la position où ils doivent se rendre... Ils restaurent aussi leur santé durant la nuit.

## Le serveur

- **Fonctionnement général :**
Le Serveur central coordonne les Caravanes, les Petitpois et l’UI sur l’enchaînement des tours et des jours. Il est le seul à avoir accès aux informations de la Map. Il fournit les résultats des actions des Petitpois.

**Un tour se décompose en 3 phases :**
- **Act Phase** : les petitpois exécutent leurs actions (se déplacer, collecter, attaquer...).
- **See Phase** : les petitpois observent les cases autour d’eux. Ils doivent interroger le serveur pour avoir une confirmation et un résultat de leur action. L'UI est mise à jour durant cette phase.
- **Think Phase** : les petitpois sélectionnent leur prochaine action selon leurs objectifs et les informations dont ils disposent.

Durant la phase d'action, le serveur attend de recevoir les requêtes Act de l'ensemble des petitpois vivants. Une fois qu'il a reçu toutes les requêtes, il les traite et renvoie les résultats aux petitpois, dans un ordre précis : 
`Attack -> Raid -> Move -> Collect -> Idle`

A tout moment, un petitpois peut mourir, il envoie donc une requête `Death` qui peut être traitée à tout moment par le serveur. Elle lui permet d'ajouter l'inventaire du petitpois mort sur la case, mais aussi de savoir qu'il n'a plus à attendre de prochaines requêtes de la part du petitpois.

A la fin d’une journée, c'est la fin des tours: on entre dans la phase de Nuit. Les caravanes sont activées par le serveur.

- **Choix techniques :**
Les petitpois peuvent s'adresser au serveur selon les phases de la journée aux endpoints "{serveur_port}/act" et "{serveur_port}/see". La requête Act est la plus compliquée à traiter car elle est multi-forme . Chaque nouvelle requête démarre une goroutine sur le serveur. Cette goroutine est forcément celle qui va envoyer la réponse au petitpois. De fait, en attendant qu'elle soit traitée au moment où toutes les requêtes seront reçues, la goroutine est mise en attente par un channel. 
Quand tous les petitpois attendus ont envoyé leur requête, le serveur traite le See ou l'Act en créant de nouvelles goroutines de traitement pour chaque requête (ou par groupe de requêtes pour les combats et les raids). Il y a un casting des requêtes Act en requêtes plus précises (Attack/Move/etc.) pour accéder aux informations nécessaires au traitement. Chaque fonction de traitement a un comportement propre et crée un objet de réponse pour les requêtes.
Quand toutes les réponses sont prêtes, un signal est transmis par channel aux goroutines des requêtes. Ces goroutines récupèrent la réponse et l'envoient aux petitpois.
On utilise des logiques similaires lorsqu'il s'agit de communiquer avec les caravanes. Par le biais des échanges avec les caravanes à la fin de la nuit, le serveur a toujours une connaissance actualisée du nombre de petitpois et des statistiques de la caravane.
On emploie des mutex à chaque fois qu'on essaie d'accéder aux ressources partagées, les attributs du serveur.

## L'UI
L'interface graphique a été réalisée en React. 

C'est depuis l'UI que l'on lance la simulation sur le bouton central.
Elle permet de visualiser la carte de la simulation en temps réel avec les caravanes et les petits pois.
On peut aussi y afficher le contenu plus détaillé de chaque tile de la carte et les statistiques de chaque caravane.

Le _frontend_ est découpé en plusieurs composants imbriqués qui échangent des informations via des propriétés partagées qui se mettent à jour uniquement si besoin et des événements.

## La communication entre modules
![img.png](img.png)

Les petitpois et leur caravanes communiquent exclusivement grâce à des channels. Ils permettent la transmission des missions et des statistiques au début de la journée, mais aussi le dépôt des inventaires et des connaissances acquises durant la journée.

Les petitois envoient des requêtes POST HTTP au serveur, qui les traite et renvoie les résultats. Les requêtes sont envoyées en JSON. 
Il n'a pas été prévu que le serveur envoie des requêtes de lui-même vers les petits pois : ils sont uniquement clients de ce serveur REST (mais pas RESTful car il stocke dans un buffer les requêtes reçues à chaque phase).

La communication entre le serveur et les caravanes est bi-directionnelle et se fait par le biais de requêtes POST également.
Chaque caravane possède sa propre URL sur laquelle le serveur envoie les requêtes à la tombée de la nuit.

Enfin, une websocket TCP relie l'UI au serveur. Le serveur et l'UI restant allumés pendant toute la simulation, la connexion reste valide.
Via cette websocket, l'interface visuel envoie un "ping" au serveur toutes les 10 ms qui ne lui retourne les informations résumées de la simulation que si le tour a changé. 
Les différents composants React se rafraichissent dans ce cas uniquement.

## Difficultés rencontrées et améliorations possibles

- **Difficultés rencontrées :**
- La difficulté majeure de ce projet a été d'ordonnancer les phases de la journée en s'adaptant au fonctionnement de go. Il était nécessaire de faire patienter l'envoi des réponses aux requêtes des petitpois. Sans ça, il était impossible de synchroniser les combats et de cadencer chaque phase de la journée. Nous avions d'abord opté pour une association de booléens et de boucles infinis qui bloquaient chaque goroutine de requête. Nous avons mis un certain temps à corriger notre système grâce à l'emploi de channels au sein du serveur.
- Des comportements inattendus sur les mutex nous ont aussi bloqué un moment.
- Comme souvent pour ce type de projet, il y a eu beaucoup de temps passé à accorder les modules de code de chacun et à communiquer sur des standards afin que tout s'assemble parfaitement.

- **Améliorations possibles :** 
- A la fin du développement, nous sommes encore conscients que des bugs existent sous certaines conditions. Quand trop de petitpois sont actifs sur la simulation, il est probable que des petitpois bloquent et n'envoient jamais de requêtes au serveur en attente. Cela bloque l'avancée des tours pour tous les agents. Nous devrions trouver l'origine de ce bug mais aussi être plus flexible à l'erreur en instaurant un délai maximal d'attente. Au delà de ce délai, les petitpois attendus seraient considérés morts.
- Nous souhaiterions trouver des optimisations pour gérer une charge plus grande d'agents sur des plus grandes cartes. Cela passerait sûrement par une révision de l'architecture du serveur central. On pense par exemple à changer les échanges serveur-petitpois en passant par des websockets.
- Les caravanes ont encore un comportement très simple. Tout au long d'une simulation, nous souhaiterions qu'elles puissent s'adapter aux informations qu'elles reçoivent et changer de stratégie en conséquence. Pour affiner les stratégies entreprises par les caravanes, nous avons pensé à un algorithme de renforcement.
- Les petitpois pourraient aussi gagner à avoir des fonctions Think plus élaborées sachant qu'ils ont beaucoup d'informations de leur environnement. Par exemple avoir des estimations plus fines des risques que chaque action implique.