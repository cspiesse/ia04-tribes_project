package tribes

// --------- REQUESTS -------- //

type DieRequest struct {
	PetitpoisID PetitpoisID `json:"petitpois-id"`
	Inventory   Inventory   `json:"inventory"`
	// NotePad     MapBelief   `json:"notepad"`
}

type ActRequest struct {
	PetitpoisID PetitpoisID `json:"petitpois-id"`
	Type        string      `json:"type"`
	Request     interface{} `json:"request"`
}

type AttackRequest struct {
	PetitpoisID PetitpoisID `json:"petitpois-id"`
	Attack      int         `json:"attack"`
	Defense     int         `json:"defense"`
	Pv          int         `json:"pv"`
}

type DefendRequest struct{}

// RaidRequest is a type representing a raid request
type RaidPPRequest struct {
	PetitpoisID PetitpoisID `json:"petitpois-id"`
	Attack      int         `json:"attack"`
}

type RaidCaravanRequest struct {
	DamageReceived int `json:"damages"`
}

type MoveRequest struct {
	PetitpoisID PetitpoisID `json:"petitpois-id"`
	Luck        float64     `json:"luck"`
	To          Position    `json:"desired-position"`
	Pv          int         `json:"current-pv"`
}

type CollectRequest struct {
	PetitpoisID PetitpoisID `json:"petitpois-id"`
	ItemType    ItemType    `json:"item-type"`
	Quantity    int         `json:"quantity"`
}

type SeeRequest struct {
	PetitpoisID PetitpoisID `json:"petitpois-id"`
	Position    Position    `json:"position"`
	Range       int         `json:"perception-stat"`
}

type IdleRequest struct {
	PetitpoisID PetitpoisID `json:"petitpois-id"`
}

// NightfallRequest is a type representing a nightfall request
type NightfallRequest struct {
	CurrentDay Day `json:"currentDay"`
}

// func ParseRequest(request ActRequest) (interface{}, error) {
// 	marshalledReq, _ := json.Marshal(request)

// 	var unmarshalledActRequest ActRequest
// 	_ = json.Unmarshal(marshalledReq, &unmarshalledActRequest)

// 	marshalledTypedRequest, _ := json.Marshal(unmarshalledActRequest.Request)

// 	switch unmarshalledActRequest.Type {
// 	case "MoveRequest":
// 		var unmarshalledTypedRequest MoveRequest
// 		_ = json.Unmarshal(marshalledTypedRequest, &unmarshalledTypedRequest)
// 		return unmarshalledTypedRequest, nil
// 	case "CollectRequest":
// 		var unmarshalledTypedRequest CollectRequest
// 		_ = json.Unmarshal(marshalledTypedRequest, &unmarshalledTypedRequest)
// 		return unmarshalledTypedRequest, nil
// 		// TODO : mettre les autres types de requêtes
// 	}
// 	return nil, nil
// }

// --------- RESPONSES -------- //

type CaravanResponse struct { // TODO : replace by the good type asap

}

type AttackResponse struct {
	DamageReceived int `json:"damages"`
}

type IdleResponse struct{}

type RaidCaravanResponse struct {
	RemainingHP        int `json:"HP"`
	RemainingFood      int `json:"Food"`
	RemainingMaterials int `json:"Materials"`
}

type RaidPPResponse struct{}

type SeeResponse struct {
	RemainingTurns int              `json:"remaining-turns"`
	NewBeliefs     []SeenTileBelief `json:"beliefs"`
}

type CollectResponse struct { // TODO : revérifier ça avec les zouaves
	ItemType ItemType `json:"item-type"`
	Quantity int      `json:"quantity"`
}

type MoveResponse struct {
	AffectedStatByEvent string `json:"stat"`
	Quantity            int    `json:"quantity"`
}

type DieResponse struct{}

type NightfallResponse struct {
	CaravanID    CaravanID              `json:"caID"`
	CaravanPos   Position               `json:"caPos"`
	CaravanStats map[string]int         `json:"caravanStats"`
	Petitpois    map[PetitpoisID]string `json:"petitpois"`
}
