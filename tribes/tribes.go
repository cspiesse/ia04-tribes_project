package main

import (
	"fmt"
	"tribes"
	"tribes/agents/caravan"
	"tribes/debug"
	"tribes/main_server/mapagent"
	"tribes/main_server/server"
)

func main() {

	debugEnable := false

	if debugEnable {
		debug.Debug()
		return
	}

	serverPort := "8080"
	serverAddress := "http://localhost:" + serverPort + "/"

	caravansAddr := make(map[tribes.CaravanID]string)
	caravansInfo := make(map[tribes.CaravanID]server.SimplifiedCaravanInfo)

	if tribes.WhichSimulation == 1 {

		// ------ 2 Caravan simulation ------ //
		strats, positions := MakeCaravans()
		caravansAddr, caravansInfo = caravan.MakeCaravanes(tribes.NbCaravans, serverAddress, strats, positions)

	} else if tribes.WhichSimulation == 2 {

		// ------ Random caravans simu ------ //
		// 8 Caravanes maximum (à cause du front)
		caravansAddr, caravansInfo = caravan.MakeCaravanes(tribes.NbCaravans, serverAddress, []caravan.CaravanStrategy{}, []tribes.Position{})
	}

	m := mapagent.RandomMapGeneration(tribes.Height, tribes.Width, tribes.VanillaBiomes, tribes.EventBiomes, tribes.DefaultBiomeRatio)

	grid := mapagent.Map{
		Matrix:      m.Matrix,
		Biomes:      m.Biomes,
		EventBiomes: m.EventBiomes}

	rsa := server.NewRestWebService("toto", serverPort, caravansAddr, grid, caravansInfo)

	go rsa.Start()

	fmt.Scanln()
}

func MakeCaravans() (strats []caravan.CaravanStrategy, positions []tribes.Position) {
	// -- Caravan 1 --
	caravane1Strat := caravan.CaravanStrategy{
		"Agressive": 100,
		"Defensive": 0,
		"Gathering": 0,
		"Knowledge": 0}
	pos1 := tribes.Position{X: 7, Y: 7}

	positions = append(positions, pos1)
	strats = append(strats, caravane1Strat)

	// -- Caravan 2 --
	caravane2Strat := caravan.CaravanStrategy{
		"Agressive": 0,
		"Defensive": 100,
		"Gathering": 0,
		"Knowledge": 0}
	pos2 := tribes.Position{X: 8, Y: 3}

	positions = append(positions, pos2)
	strats = append(strats, caravane2Strat)

	// -- Caravan 3 --
	caravane3Strat := caravan.CaravanStrategy{
		"Agressive": 0,
		"Defensive": 0,
		"Gathering": 100,
		"Knowledge": 0}
	pos3 := tribes.Position{X: 3, Y: 8}

	positions = append(positions, pos3)
	strats = append(strats, caravane3Strat)

	// -- Caravan 4 --
	caravane4Strat := caravan.CaravanStrategy{
		"Agressive": 0,
		"Defensive": 0,
		"Gathering": 0,
		"Knowledge": 100}
	pos4 := tribes.Position{X: 8, Y: 8}

	positions = append(positions, pos4)
	strats = append(strats, caravane4Strat)

	return
}
