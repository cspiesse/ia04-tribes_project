package tribes

import (
	"fmt"
	"math"
	"math/rand"
	"sort"
	"strconv"
	"time"
)

func TorusPos(x, y int, matrixHeight, matrixWidth int) Position {
	if x < 0 {
		x = matrixWidth + x%matrixWidth
	} else if x >= matrixWidth {
		x = x % matrixWidth
	}
	if y < 0 {
		y = matrixHeight + y%matrixHeight
	} else if y >= matrixHeight {
		y = y % matrixHeight
	}
	return Position{x, y}
}

// Gives the 4 adjacent positions of a given position in a crux shape inside a toric matrix
func (p *Position) Adjacent(matrixHeight, matrixWidth int) []Position {
	sum := []int{-1, 1}
	adjNeighbors := make([]Position, 0, 4)

	for _, v := range sum {
		adjNeighbors = append(adjNeighbors, TorusPos(p.X+v, p.Y, matrixHeight, matrixWidth))
		adjNeighbors = append(adjNeighbors, TorusPos(p.X, p.Y+v, matrixHeight, matrixWidth))
	}

	return adjNeighbors
}

// Gives the 8 neighbor's positions of a given position in a square shape inside a toric matrix
func (p *Position) Neighborhood(matrixHeight, matrixWidth int) []Position {
	neighbors := make([]Position, 0, 8)
	for i := -1; i <= 1; i++ {
		for j := -1; j <= 1; j++ {
			if i != j {
				neighbors = append(neighbors, TorusPos(p.X+j, p.Y+i, matrixHeight, matrixWidth))
			}
		}
	}
	return neighbors
}

// Gives the reachable neighbor's positions of a given position according to a given range
func (p *Position) RangeNeighborhood(matrixHeight, matrixWidth, r int) (neighbors []Position) {
	for i := -r; i <= r; i++ {
		for j := -r; j <= r; j++ {
			if r >= TaxiCabDistance(*p, TorusPos(p.X+i, p.Y+j, matrixHeight, matrixWidth), matrixHeight, matrixWidth) {
				if i != 0 || j != 0 {
					neighbors = append(neighbors, TorusPos(p.X+j, p.Y+i, matrixHeight, matrixWidth))
				}
			}
		}
		// Returns the list in a random order for situations with few tileBeliefs
		rand.Seed(time.Now().UnixNano())
		rand.Shuffle(len(neighbors), func(i, j int) {
			neighbors[i], neighbors[j] = neighbors[j], neighbors[i]
		})
	}

	return
}

// Computes Taxi-Cab distance between pos 1 and pos2
func TaxiCabDistance(pos1, pos2 Position, matrixHeight, matrixWidth int) (d int) {
	// Tries the 4 distances of the 2 points in the torus and selects the smaller one
	d = int(math.Abs(float64(pos1.X)-float64(pos2.X))) + int(math.Abs(float64(pos1.Y)-float64(pos2.Y)))

	d2 := matrixWidth - int(math.Abs(float64(pos1.X)-float64(pos2.X))) + int(math.Abs(float64(pos1.Y)-float64(pos2.Y)))
	if d2 < d {
		d = d2
	}

	d3 := int(math.Abs(float64(pos1.X)-float64(pos2.X))) + matrixHeight - int(math.Abs(float64(pos1.Y)-float64(pos2.Y)))
	if d3 < d {
		d = d3
	}

	d4 := matrixWidth - int(math.Abs(float64(pos1.X)-float64(pos2.X))) + matrixHeight - int(math.Abs(float64(pos1.Y)-float64(pos2.Y)))
	if d4 < d {
		d = d4
	}

	return
}

func (p *Position) Pos2String() string {
	return "(" + strconv.FormatInt(int64(p.X), 10) + ", " + strconv.FormatInt(int64(p.Y), 10) + ")"
}

// Remove a choice at the specified index from a given slice of choices.
func RemovePosition(s []Position, i int) []Position {
	s[i] = s[len(s)-1]
	return s[:len(s)-1]
}

// Remove a string at the specified index from a given slice of strings.
func RemoveString(s []string, i int) []string {
	s[i] = s[len(s)-1]
	return s[:len(s)-1]
}

// Sums a slice of floats
func SumFloatSlice(slice []float64) (result float64) {
	for _, v := range slice {
		result += v
	}
	return
}

// Sort a strategy map
func SortStrategyDict(dict StratType) (sortedKeys []string) {
	sortedKeys = make([]string, 0, len(dict))

	for key := range dict {
		sortedKeys = append(sortedKeys, key)
	}

	sort.SliceStable(sortedKeys, func(i, j int) bool {
		return dict[sortedKeys[i]] < dict[sortedKeys[j]]
	})
	return
}

// Removes an item from a slice
func RemoveFromSlice[T comparable](l []T, item T) []T {
	for i, other := range l {
		if other == item {
			return append(l[:i], l[i+1:]...)
		}
	}
	return l
}

func Contains(elems []PetitpoisID, v PetitpoisID) bool {
	for _, s := range elems {
		if v == s {
			return true
		}
	}
	return false
}

func CompareIntSlice(s1, s2 []int) bool {

	if len(s1) != len(s2) {
		return false
	}

	for i := range s1 {
		if s1[i] != s2[i] {
			return false
		}
	}

	return true
}

// A method to get the last element of a slice
func Last[T any](s []T) T {
	return s[len(s)-1]
}

// A method to pop the last element of a slice
func Pop[T any](s []T) (T, []T) {
	return s[len(s)-1], s[:len(s)-1]
}

// Modulo like in python
func Modulo(d, m int) int {
	var res int = d % m
	if (res < 0 && m > 0) || (res > 0 && m < 0) {
		return res + m
	}
	return res
}

// Get the type of a variable
func TypeOf(v interface{}) string {
	return fmt.Sprintf("%T", v)
}

func GetOppositeEnemiDirection(currentPosition Position, matrixHeight int, matrixWidth int, distanceCapacity int, enemis map[Position]int) Position {
	possiblePositions := currentPosition.RangeNeighborhood(matrixHeight, matrixWidth, distanceCapacity)

	bestScore := 0
	position := currentPosition
	for _, pos := range possiblePositions {
		score := 0
		for enemiPos, nbEnemi := range enemis {
			// The more enemies are distant, the more the score will be high
			score += nbEnemi * TaxiCabDistance(currentPosition, enemiPos, matrixHeight, matrixWidth)
		}
		if score > bestScore {
			bestScore = score
			position = pos
		}
	}
	return position
}

func CompareTileBeliefs(b1, b2 TileBelief) bool {
	if b1.Position != b2.Position {
		return false
	}
	if b1.Biome != b2.Biome {
		return false
	}
	if b1.Elements.Caravan != b2.Elements.Caravan {
		return false
	}
	if !CompareElements(b1.Elements, b2.Elements) {
		return false
	}
	return true
}
