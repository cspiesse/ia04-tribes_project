import React, { Component } from "react";
import './bootstrap.min.css';
import './App.css';
import Map from './components/Map/Map';
import Header from "./components/Header/Header";
import Sider from "./components/Sider/Sider";
import {connect, sendMsg} from "./api";
import PopUpCaravan from "./components/PopUpCaravan/PopUpCaravan";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentCell: {
                i: null,
                j: null,
                elements: {
                    pois: [],
                    caravan: null
                },
                bg: null,
            },
            mapInfo: null,
            visiblePopUpCaravan: false
        }
    }

    send(msg) {
        //console.log("Message sent", msg);
        sendMsg(msg);
    }

    showPopUpCaravan = (caravanId) => {
        console.log("visiblePopUpCaravan for ", caravanId);
        if (caravanId) {
            this.setState(prevState => ({
                ...prevState,
                visiblePopUpCaravan: !prevState.visiblePopUpCaravan,
            }));
        }
}

    setCurrentCell = (cell) => {
        this.setState(prevState => ({
            ...prevState,
            currentCell: cell,
        }));
    }

    componentDidMount() {
        connect((msg) => {
            const data = JSON.parse(msg.data);
            //console.log(data);
            if (data && data.CurrentTurn !== undefined) {
                this.setState(prevState => ({
                    ...prevState,
                    mapInfo: data,
                }))
            }
        });
    }

    render() {
        let currentDay = 0;
        let currentTurn = 0;
        if (this.state.mapInfo) {
            currentDay = this.state.mapInfo.CurrentDay;
            currentTurn = this.state.mapInfo.CurrentTurn;
        }

        return (
            <div className="App">
                <Header day={currentDay} turn={currentTurn}/>
                <div className="main-body">
                    {this.state.visiblePopUpCaravan ?
                        <PopUpCaravan caravanId={this.state.currentCell.elements.caravan} popUpCaravan={this.showPopUpCaravan} caravansInfo={this.state.mapInfo.CaravansInfo}/>
                        : null
                    }
                    <section className="main-banner" id="top">
                        <div className="caption">
                            <Map className="Map" mapInfo={this.state.mapInfo} send={this.send} click={this.setCurrentCell}/>
                        </div>
                    </section>
                    <Sider currentCell={this.state.currentCell} popUpCaravan={this.showPopUpCaravan}/>
                </div>
                <footer>
                    <div className="pre-header">
                        <div className="row">
                            <div className="left-info">
                                <ul>
                                    <li><a href="mailto:anselme.canivet@etu.utc.fr">Canivet Anselme</a></li>
                                    <li><a href="mailto:oceane.durand@etu.utc.fr">Durand Océane</a></li>
                                    <li><a href="mailto:robin.monje@etu.utc.fr">Monje Robin</a></li>
                                    <li><a href="mailto:charles.spiesser@etu.utc.fr">Spiesser Charles</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        );
    }
}

export default App;
