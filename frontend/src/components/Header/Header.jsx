import React from "react";
import "./Header.css";

const Header = (args) => (
    <header className="header-area header-sticky">
        <div className="row">
            <div className="col-12">
                <nav className="main-nav">
                    <h2>Tribes</h2>
                    <div className="clock-menu">
                        <div className="main-button scroll-to-section">
                            <a href="#">Current Day: {args.day}</a>
                        </div>
                        <div className="second-button">
                            <a href="#">Current Turn: {args.turn}</a>
                        </div>
                    </div>
                    <a className='menu-trigger' href="#menu">
                        <span>Menu</span>
                    </a>
                </nav>
            </div>
        </div>
    </header>
);

export default Header;
