import React, {Component} from "react";
import "./Card.css";
import PetitPois from "../Map/Pois";
import {caravan} from "../Map/textures";
import {colors} from "../Map/Map";

class Card extends Component {
    render() {
        const cell = (
            <div>
                <h6>Tile infos</h6>
                <div className="big-cell" style={{ backgroundImage: `url(${this.props.currentCell.bg})`}}>
                    {
                        this.props.currentCell.elements.caravan ?
                            (<div>
                                <div className="cell" style={{
                                    width: `200px`,
                                    height: `200px`,
                                    position:"absolute",
                                    backgroundColor: `${colors[this.props.currentCell.elements.caravan]}80`,
                                    backgroundSize: `200px`
                                }}/>
                                <div className="cell" style={{
                                    width: `200px`,
                                    height: `200px`,
                                    position:"absolute",
                                    backgroundImage: `url(${caravan})`,
                                    backgroundSize: `200px`
                                }}/>
                            </div>) : null
                    }
                    <PetitPois infos={this.props.currentCell.elements.pois} sizemax={200}/>
                </div>
                <div className="infos">
                    <p>Position: (X={this.props.currentCell.i}, Y={this.props.currentCell.j})</p>
                    <p>Caravan: {JSON.stringify(this.props.currentCell.elements.caravan)}</p>
                    <p>Pois: {
                        this.props.currentCell.elements.pois ?
                            this.props.currentCell.elements.pois.map(pois => `
                            ${pois.petitpoisId}: ${pois.role}\n`) : null} </p>
                </div>
            </div>
        )
        return (
            <div className="caption">
                <div className="zoom-cell">
                    {cell}
                </div>
            </div>
        )
    }
}

export default Card;