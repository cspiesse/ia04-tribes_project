import React, {Component} from "react";
import "./Sider.css";
import "../../App.css";
import Card from "../Card/Card";

class Sider extends Component {

    render() {
        return (
            <aside>
                <div className="caption">
                    <div className="sider-menu">
                        <div className="main-button scroll-to-section">
                            <a href="#" onClick={() => this.props.popUpCaravan(this.props.currentCell.elements.caravan)}>Caravan Infos</a>
                        </div>
                        <div className="second-button">
                            <a href="#">Stats</a>
                        </div>
                    </div>
                    <Card currentCell={this.props.currentCell}/>
                </div>
            </aside>
        )
    }
}

export default Sider;