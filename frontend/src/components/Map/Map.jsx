import {Component} from "react";
import "./Map.css";
import {grass, sand, basalt, water, dirt, caravan} from "./textures";
import PetitPois from "./Pois";

export const colors = {
    "Caravane1": "#f66857",
    "Caravane2": "#38afe7",
    "Caravane3": "#4fce80",
    "Caravane4": "#fde200",
    "Caravane5": "#fc8d36",
    "Caravane6": "#ba4eea",
    "Caravane7": "#ff79ea",
    "Caravane8": "#008506"
};

const biomes = {
    "desert"  : sand,
    "oasis"   : water,
    "mountain": basalt,
    "jungle"  : grass,
    "savanna" : dirt,
}

class Map extends Component {
    map;
    caravans;
    petitpois;
    cols;
    rows;
    currentTurn;
    launchSimulation;

    componentDidMount() {
        setInterval(() => {
            if (this.launchSimulation) {
                this.props.send("launchSimulation");
                this.launchSimulation = false;
            }
            else {
                this.props.send("Ping from UI");
            }
        }, 500);
        clearInterval();
    }

    render() {
        if (this.props.mapInfo !== null) {
            this.cols = this.props.mapInfo.Width;
            this.rows = this.props.mapInfo.Height;
            let cellSize = Math.min(30,Math.floor(700/Math.max(this.cols, this.rows)));

            if (this.props.mapInfo.CurrentTurn != this.currentTurn) {
                // Update Turn
                this.currentTurn = this.props.mapInfo.CurrentTurn;

                // Update Map
                this.map = this.props.mapInfo.SimplifiedGrid.Matrix;

                // Update caravans on map
                this.caravans = this.props.mapInfo.CaravansInfo;
                Object.keys(this.caravans).forEach(caravanId => {
                    const c = this.caravans[caravanId];
                    this.map[c.position.Y][c.position.X].Elements.Caravan = caravanId;
                })

                // Update petitpois on map
                console.log(">>>>>> udpate pp for turn ", this.props.mapInfo.CurrentTurn);
                this.petitpois = this.props.mapInfo.PetitpoisInfo;
                Object.keys(this.petitpois).forEach(petitpoisId => {
                    const p = this.petitpois[petitpoisId];
                    let enemies = this.map[p.position.Y][p.position.X].Elements.Enemies;
                    if (!enemies) {
                        this.map[p.position.Y][p.position.X].Elements.Enemies = [{petitpoisId:petitpoisId, ...p}];
                    }
                    else {
                        this.map[p.position.Y][p.position.X].Elements.Enemies.push(
                            {petitpoisId:petitpoisId, ...p});
                    }
                })
            }

            let grid = this.map.map((row, i) =>
                row.map((cell, j) => {
                    let bg = biomes[cell.BiomeName]
                    let content = {
                        i:cell.X,
                        j:cell.Y,
                        elements: {
                            pois: cell.Elements.Enemies,
                            caravan: cell.Elements.Caravan
                        },
                        bg: bg,
                    }
                    return (
                        <div className="cell"
                             key={`${i}-${j}`}
                             style={{
                                 width: `${cellSize}px`,
                                 height: `${cellSize}px`,
                                 backgroundImage: `url(${bg})`,
                                 backgroundSize: `${cellSize}px`,
                                 border: cellSize <20 ? '.5px solid white':'1px solid white'}}
                             onClick={() => this.props.click(content)}>
                            {
                                content.elements.caravan ?
                                    (<div>
                                        <div className="cell" style={{
                                            width: `${cellSize}px`,
                                            height: `${cellSize}px`,
                                            position:"absolute",
                                            backgroundColor: `${colors[content.elements.caravan]}A0`,
                                            backgroundSize: `${cellSize}px`
                                        }}/>
                                        <div className="cell" style={{
                                            width: `${cellSize}px`,
                                            height: `${cellSize}px`,
                                            position:"absolute",
                                            backgroundImage: `url(${caravan})`,
                                            backgroundSize: `${cellSize}px`
                                        }}/>
                                    </div>) : null
                            }

                            <PetitPois infos={content.elements.pois} sizemax={cellSize}/>
                        </div>
                    )
                }))
            return (
                <div style={{
                    display: "grid",
                    gridTemplateColumns:`repeat(${this.cols}, ${cellSize}px)`}}>
                    {grid}
                </div>
            );
        }
        else {
            return (
                <div>
                    <h6>Map loading...</h6>
                    <div className="loader"/>
                    <div className="second-button">
                        <a href="#" onClick={() => this.launchSimulation=true}>Launch Simulation</a>
                    </div>
                </div>
            )
        }
    }
}

export default Map;