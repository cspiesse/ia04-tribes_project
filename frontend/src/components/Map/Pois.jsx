import React from 'react';
import {colors} from "./Map";

const types = {
    "Attacker" : 3,
    "Defender" : 2,
    "Collector": 1,
    "Explorer" : 0
}

const PetitPois = (args) => {
    let poisList = args.infos ? args.infos : [];

    let sizemax = args.sizemax;
    let shapesArray = [];
    poisList.forEach(pois => {
        shapesArray.push(generateShapePois(pois, sizemax));
    })

    return (
        <div>
            {shapesArray.map((shape, index) => (
                <div className="animated" key={index} style={{zIndex:`${index}`}}>
                    <div className="particle" style={{...shape.style}}/>
                </div>
            ))}
        </div>
    );
};

const generateShapePois = (pois, sizemax) => {
    // TODO: shape depending of type SimplifiedPetitpoisInfo.role
    const shapeType = types[pois.role];
    // TODO: color depending on SimplifiedPetitpoisInfo.belongingCaravanID
    const color = colors[pois.belongingCaravanID];
    let style;
    const size = Math.floor(sizemax > 100 ? sizemax/6 : sizemax/4); // Math.floor(Math.random() * 50 + 50);
    const x = Math.min(Math.max(sizemax/20,Math.floor(Math.random() * sizemax*9/10)),sizemax*7/10);
    const y = Math.min(Math.max(sizemax/20,Math.floor(Math.random() * sizemax*9/10)),sizemax*7/10);
    switch (shapeType) {
        case 0:
            // Generate a circle
            style = {
                width: size,
                height: size,
                borderRadius: '50%',
                backgroundColor: color,
                transform: `translate(${x}px, ${y}px)`
            };
            break;
        case 1:
            // Generate a square
            style = {
                width: size,
                height: size,
                backgroundColor: color,
                transform: `translate(${x}px, ${y}px)`
            };
            break;
        case 2:
            // Generate a bottom triangle
            style = {
                width: 0,
                height: 0,
                borderLeft: `${Math.ceil(size/2)}px solid transparent`,
                borderRight: `${Math.ceil(size/2)}px solid transparent`,
                borderBottom: `${size}px solid ${color}`,
                transform: `translate(${x}px, ${y}px)`
            };
            break;
        default:
            // Generate a top triangle
            style = {
                width: 0,
                height: 0,
                borderLeft: `${Math.ceil(size/2)}px solid transparent`,
                borderRight: `${Math.ceil(size/2)}px solid transparent`,
                borderTop: `${size}px solid ${color}`,
                transform: `translate(${x}px, ${y}px)`
            };
            break;
    }
    return { style };
};

function RandKey(NewDictionary){
    const keys = Object.keys(NewDictionary);
    let i = keys.length - 1;
    const j = Math.floor(Math.random() * i);
    return keys[j];
}

/*

const getRandomColor = () => {
    const letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
};

const moveShape = (shape, sizemax) => {
    const x = Math.floor(Math.random() * sizemax);
    const y = Math.floor(Math.random() * sizemax);
    let style = {...shape.style };
    style.transform = `translate(${x}px, ${y}px)`;
    return { style };
};
*/

export default PetitPois;
