import grass from './grass.png';
import sand from './sand.bmp';
import basalt from './basalt.png';
import dirt from './dirt.webp';
import water from './water.png';
import caravan from './caravan.png';

export {grass, sand, basalt, dirt, water, caravan};
