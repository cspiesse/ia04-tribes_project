import React, {Component} from "react";
import "./PopUpCaravan.css";
import {caravan} from "../Map/textures";
import {colors} from "../Map/Map";

function getStats(caravanStats) {
    return (
        <ul>
            {Object.keys(caravanStats).map(stat => (
                <li key={stat}>
                    <b>{stat}:</b>{`\t${caravanStats[stat]}`}
                </li>))}
        </ul>
    )
}

class PopUpCaravan extends Component {
    render() {
        const caravanId = this.props.caravanId;
        const caravanInfos = this.props.caravansInfo[caravanId];

        return (
            <div className="popup caption">
                <div className="main-button scroll-to-section" id="close">
                    <a href="#" onClick={() => this.props.popUpCaravan(caravanId)}>Close</a>
                </div>
                <h6 id="head">Caravan infos</h6>
                <p style={{textAlign:'center'}}><b>{caravanId}</b></p>
                <div className="big-cell">
                    <div className="cell" style={{
                        width: `200px`,
                        height: `200px`,
                        position:"absolute",
                        backgroundColor: `${colors[caravanId]}A0`,
                        backgroundSize: `200px`
                    }}/>
                    <div className="cell" style={{
                        width: `200px`,
                        height: `200px`,
                        position:"absolute",
                        backgroundImage: `url(${caravan})`,
                        backgroundSize: `200px`
                    }}/>
                </div>
                <div className="infos">
                    <p><b>Position:</b> (X={caravanInfos.position.X}, Y={caravanInfos.position.Y})</p>
                    <p><b>Stats:</b></p>
                    {getStats(caravanInfos.caravanStats)}
                </div>
            </div>
        )
    }
}

export default PopUpCaravan;