package tribes

type Inventory struct {
	PetitpoisID PetitpoisID
	Materials   int
	Food        int
	XP          int
	Size        int
}

func NewInventory(petitPoisID PetitpoisID, size int) *Inventory {
	return &Inventory{
		PetitpoisID: petitPoisID,
		Size:        size,
	}
}

// Method to add or remove food to the inventory
func (i *Inventory) AddOrRemoveFood(n int) {
	if i.SizeLeft() < n {
		i.Food += i.SizeLeft()
		return
	}
	if i.Food+n < 0 {
		i.Food = 0
		return
	}
	i.Food += n
}

// Method to add or remove food to the inventory
func (i *Inventory) AddOrRemoveMaterials(n int) {
	if i.SizeLeft() < n {
		i.Materials += i.SizeLeft()
		return
	}
	if i.Materials+n < 0 {
		i.Materials = 0
		return
	}
	i.Materials += n
}

func (i *Inventory) AddOrRemoveXp(n int) {
	if i.XP+n < 0 {
		i.XP = 0
		return
	}
	i.XP += n
}

// Method to get the size left in the inventory
func (i *Inventory) SizeLeft() int {
	return i.Size - i.Food - i.Materials
}

// Method to check if the inventory is full
func (i *Inventory) IsFull() bool {
	return i.SizeLeft() == 0
}

// Method to update the size of the inventory
func (i *Inventory) UpdateSize(size int) {
	i.Size = size
}

// Method to get the current proportions of food and materials in the inventory
func (i *Inventory) CurrentProportions() map[ItemType]float64 {
	total := float64(i.Food + i.Materials)
	return map[ItemType]float64{
		"Food":      float64(i.Food) / total,
		"Materials": float64(i.Materials) / total,
	}
}
