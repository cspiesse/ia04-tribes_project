package tribes

import (
	"math"
)

// ------ Parameters to play with ------//
// Map parameters
var DayDuration = Turn(40)
var Height = 15
var Width = 15
var NbCaravans = 8 // max 8 caravans (limited by the frontend)

// Caravan
var WhichSimulation = 2

var NbstartMembers = 5
var NbStartFood = 30
var NbStartMaterials = 10
var BaseMaxCaravanHP = 30

// Petitpois
var BaseMaxPetitpoisHP = 10
var InventoryBaseSize = 6

// ------ Map Parameters ------//
var WaitTime = 250

var DefaultTileBiome = "desert"
var DefaultBiomeRatio float32 = 0.3

var Desert = Biome{
	Name:             "desert",
	NbTilesMin:       0,
	NbTilesMax:       0,
	RegenerationRate: 0,
	FoodLimit:        0,
	MaterialsLimit:   0,
}
var Oasis = Biome{
	Name:             "oasis",
	NbTilesMin:       4,
	NbTilesMax:       10,
	RegenerationRate: 6,
	FoodLimit:        15,
	MaterialsLimit:   3,
}
var Mountain = Biome{
	Name:             "mountain",
	NbTilesMin:       10,
	NbTilesMax:       18,
	RegenerationRate: 4,
	FoodLimit:        0,
	MaterialsLimit:   20,
}
var Jungle = Biome{
	Name:             "jungle",
	NbTilesMin:       7,
	NbTilesMax:       15,
	RegenerationRate: 4,
	FoodLimit:        12,
	MaterialsLimit:   4,
}
var Savanna = Biome{
	Name:             "savanna",
	NbTilesMin:       6,
	NbTilesMax:       18,
	RegenerationRate: 6,
	FoodLimit:        10,
	MaterialsLimit:   10,
}

var VanillaBiomes = map[string]Biome{
	"desert":   Desert,
	"oasis":    Oasis,
	"mountain": Mountain,
	"jungle":   Jungle,
	"savanna":  Savanna,
}

//------- Events parameters -------//

// Events Bonuses

var VeryGoodHPEventBonus = 5.
var VeryGoodFoodEventBonus = 5.
var VeryGoodMaterialEventBonus = 5.
var VeryGoodXPEventBonus = 10.

var GoodHPEventBonus = 2.
var GoodFoodEventBonus = 2.
var GoodMaterialEventBonus = 2.
var GoodXPEventBonus = 3.

var NeutralEventBonus = 0.

var BadHPEventBonus = -2.
var BadFoodEventBonus = -2.
var BadMaterialEventBonus = -2.
var BadXPEventBonus = -3.

var VeryBadHPEventBonus = -5.
var VeryBadFoodEventBonus = -5.
var VeryBadMaterialEventBonus = -5.
var VeryBadXPEventBonus = -10.

// Event Distribution
type EventProbability map[string]float64

func EventCalculator(luckCoef float64, eventName string) (int, string) {
	switch eventName {
	case "VeryGoodHPEvent":
		return int(math.Floor(5 * luckCoef)), "HP"
	case "VeryGoodFoodEvent":
		return int(math.Floor(5 * luckCoef)), "Food"
	case "VeryGoodMaterialEvent":
		return int(math.Floor(5 * luckCoef)), "Materials"
	case "VeryGoodXPEvent":
		return int(math.Floor(10 * luckCoef)), "XP"
	case "GoodHPEvent":
		return int(math.Floor(2 * luckCoef)), "HP"
	case "GoodFoodEvent":
		return int(math.Floor(2 * luckCoef)), "Food"
	case "GoodMaterialEvent":
		return int(math.Floor(2 * luckCoef)), "Materials"
	case "GoodXPEvent":
		return int(math.Floor(3 * luckCoef)), "XP"
	case "NeutralEvent":
		return 0, "XP"
	case "BadHPEvent":
		return int(math.Ceil(-2 * (2 - luckCoef))), "HP"
	case "BadFoodEvent":
		return int(math.Ceil(-2 * (2 - luckCoef))), "Food"
	case "BadMaterialEvent":
		return int(math.Ceil(-2 * (2 - luckCoef))), "Materials"
	case "BadXPEvent":
		return int(math.Ceil(-3 * (2 - luckCoef))), "XP"
	case "VeryBadHPEvent":
		return int(math.Ceil(-5 * (2 - luckCoef))), "HP"
	case "VeryBadFoodEvent":
		return int(math.Ceil(-5 * (2 - luckCoef))), "Food"
	case "VeryBadMaterialEvent":
		return int(math.Ceil(-5 * (2 - luckCoef))), "Materials"
	case "VeryBadXPEvent":
		return int(math.Ceil(-10 * (2 - luckCoef))), "XP"
	default:
		return -1, "err"
	}
}

var DesertEvents = EventProbability{
	"VeryGoodHPEvent":       0,
	"VeryGoodFoodEvent":     0,
	"VeryGoodMaterialEvent": 0,
	"VeryGoodXPEvent":       0,
	"GoodHPEvent":           0,
	"GoodFoodEvent":         0,
	"GoodMaterialEvent":     0,
	"GoodXPEvent":           0,
	"NeutralEvent":          1,
	"BadHPEvent":            0,
	"BadFoodEvent":          0,
	"BadMaterialEvent":      0,
	"BadXPEvent":            0,
	"VeryBadHPEvent":        0,
}

var OasisEvents = EventProbability{
	"VeryGoodHPEvent":       0.01,
	"VeryGoodFoodEvent":     0.01,
	"VeryGoodMaterialEvent": 0,
	"VeryGoodXPEvent":       0.01,
	"GoodHPEvent":           0.04,
	"GoodFoodEvent":         0.04,
	"GoodMaterialEvent":     0,
	"GoodXPEvent":           0.04,
	"NeutralEvent":          0.82,
	"BadHPEvent":            0.01,
	"BadFoodEvent":          0,
	"BadMaterialEvent":      0.01,
	"BadXPEvent":            0.01,
	"VeryBadHPEvent":        0,
	"VeryBadFoodEvent":      0,
	"VeryBadMaterialEvent":  0,
	"VeryBadXPEvent":        0,
}

var MountainEvents = EventProbability{
	"VeryGoodHPEvent":       0,
	"VeryGoodFoodEvent":     0,
	"VeryGoodMaterialEvent": 0.01,
	"VeryGoodXPEvent":       0.01,
	"GoodHPEvent":           0,
	"GoodFoodEvent":         0,
	"GoodMaterialEvent":     0.04,
	"GoodXPEvent":           0.04,
	"NeutralEvent":          0.77,
	"BadHPEvent":            0.04,
	"BadFoodEvent":          0.04,
	"BadMaterialEvent":      0.01,
	"BadXPEvent":            0.01,
	"VeryBadHPEvent":        0.01,
	"VeryBadFoodEvent":      0.01,
	"VeryBadMaterialEvent":  0.00,
	"VeryBadXPEvent":        0.01,
}

var JungleEvents = EventProbability{
	"VeryGoodHPEvent":       0.01,
	"VeryGoodFoodEvent":     0.01,
	"VeryGoodMaterialEvent": 0,
	"VeryGoodXPEvent":       0,
	"GoodHPEvent":           0.04,
	"GoodFoodEvent":         0.04,
	"GoodMaterialEvent":     0,
	"GoodXPEvent":           0,
	"NeutralEvent":          0.77,
	"BadHPEvent":            0.01,
	"BadFoodEvent":          0.01,
	"BadMaterialEvent":      0.04,
	"BadXPEvent":            0.04,
	"VeryBadHPEvent":        0.01,
	"VeryBadFoodEvent":      0.00,
	"VeryBadMaterialEvent":  0.01,
	"VeryBadXPEvent":        0.01,
}

var SavannaEvents = EventProbability{
	"VeryGoodHPEvent":       0.01,
	"VeryGoodFoodEvent":     0,
	"VeryGoodMaterialEvent": 0.01,
	"VeryGoodXPEvent":       0.01,
	"GoodHPEvent":           0.04,
	"GoodFoodEvent":         0,
	"GoodMaterialEvent":     0.04,
	"GoodXPEvent":           0.04,
	"NeutralEvent":          0.82,
	"BadHPEvent":            0.01,
	"BadFoodEvent":          0.01,
	"BadMaterialEvent":      0,
	"BadXPEvent":            0.01,
	"VeryBadHPEvent":        0,
	"VeryBadFoodEvent":      0,
	"VeryBadMaterialEvent":  0,
	"VeryBadXPEvent":        0,
}

var EventBiomes = map[string]EventProbability{
	"desert":   DesertEvents,
	"oasis":    OasisEvents,
	"mountain": MountainEvents,
	"jungle":   JungleEvents,
	"savanna":  SavannaEvents,
}

//------- Formulas parameters -------//

var AttackBaseBonus = 2
var WisdomDivider = 4.
var MaxHPDivider = 4.
var SeeRangeBaseBonus = 3
var SeeRangeDivider = 2.
var MoveSpeedBaseBonus = 6
var CollectSpeedBaseBonus = 1
var LuckDivider = 8.

//------- Petitpois parameters -------//

// Thresholds
var PtgRemainingHPBeforeRetreat = 0.2
var SafeReturnMargin = 0.05

// --- Stats bonuses ---

// Gatherer
var GathererAttackBonus = -2
var GathererDefenseBonus = 0
var GathererPerceptionBonus = 0
var GathererSpeedBonus = 0
var GathererCollectBonus = 2
var GathererInventorySizeBonus = 4

// Attacker
var AttackerAttackBonus = 1
var AttackerDefenseBonus = 2
var AttackerPerceptionBonus = 0
var AttackerSpeedBonus = 0
var AttackerCollectBonus = 0
var AttackerInventorySizeBonus = 0

// Explorer
var ExplorerAttackBonus = 0
var ExplorerDefenseBonus = 0
var ExplorerPerceptionBonus = 2
var ExplorerSpeedBonus = 2
var ExplorerCollectBonus = 0
var ExplorerInventorySizeBonus = 0

// Defender
var DefenderAttackBonus = 1
var DefenderDefenseBonus = 2
var DefenderPerceptionBonus = 0
var DefenderSpeedBonus = 0
var DefenderCollectBonus = 0
var DefenderInventorySizeBonus = 0

//------- Caravan parameters -------//

// XP
// var LevelThresholds = []int{50, 100, 200, 400, 800, 1600, 3200, 6400}
var LevelThresholds = []int{20, 40, 80, 160, 320, 640, 1280, 2560}

// Ranges
var CaravanPerceptionRange = 3
var CaravanMoveRange = 1

// Caravan inner thersholds
var MaxPetitpois = 10000
var ExplorersPerEmptyTiles = 10
var AttackersInGroups = 2
var NbDaysOfMemory = 5
var MaxResourceLevel = 3

// Needs
var FoodNeededForBirth = 1
var MaterialNeedsForHPGain = 1
var ResourceNeeds = 2.

// Strategies
var AgressiveStrat = Strategy{
	XPStrat: XPStrategy{
		"Attack":     55,
		"Defense":    5,
		"Wisdom":     5,
		"Stamina":    15,
		"Perception": 5,
		"Speed":      15,
		"Gathering":  0,
		"Luck":       0,
	},
	FoodStrat: FoodStrategy{
		"Population": 40,
		"Stamina":    40,
		"Save":       20,
	},
	ResourcesStrat: ResourcesStrategy{
		"Attack":    60,
		"Defense":   20,
		"Speed":     20,
		"Gathering": 0,
	},
	RolesStrat: "Fighter",
}

var DefensiveStrat = Strategy{
	XPStrat: XPStrategy{
		"Attack":     10,
		"Defense":    60,
		"Wisdom":     0,
		"Stamina":    30,
		"Perception": 0,
		"Speed":      0,
		"Gathering":  0,
		"Luck":       0,
	},
	FoodStrat: FoodStrategy{
		"Population": 20,
		"Stamina":    40,
		"Save":       40,
	},
	ResourcesStrat: ResourcesStrategy{
		"Attack":    25,
		"Defense":   75,
		"Speed":     0,
		"Gathering": 0,
	},
	RolesStrat: "Defender",
}

var GatheringStrat = Strategy{
	XPStrat: XPStrategy{
		"Attack":     0,
		"Defense":    0,
		"Wisdom":     10,
		"Stamina":    0,
		"Perception": 20,
		"Speed":      0,
		"Gathering":  50,
		"Luck":       20,
	},
	FoodStrat: FoodStrategy{
		"Population": 100,
		"Stamina":    0,
		"Save":       0,
	},
	ResourcesStrat: ResourcesStrategy{
		"Attack":    0,
		"Defense":   0,
		"Speed":     0,
		"Gathering": 100,
	},
	RolesStrat: "Gatherer",
}

var KnowledgeStrat = Strategy{
	XPStrat: XPStrategy{
		"Attack":     0,
		"Defense":    0,
		"Wisdom":     35,
		"Stamina":    0,
		"Perception": 20,
		"Speed":      35,
		"Gathering":  0,
		"Luck":       10,
	},
	FoodStrat: FoodStrategy{
		"Population": 0,
		"Stamina":    25,
		"Save":       75,
	},
	ResourcesStrat: ResourcesStrategy{
		"Attack":    0,
		"Defense":   0,
		"Speed":     100,
		"Gathering": 0,
	},
	RolesStrat: "Explorer",
}

var Strategies = map[string]Strategy{
	"Agressive": AgressiveStrat,
	"Defensive": DefensiveStrat,
	"Gathering": GatheringStrat,
	"Knowledge": KnowledgeStrat}
