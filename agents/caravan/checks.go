package caravan

import (
	"errors"
	"tribes"
)

// Check if the sum of strategies weights equals 100
func CheckStrategy(s tribes.StratType) (err error) {

	sum := 0
	for _, value := range s {
		sum += int(value)
	}

	if sum != 100.0 {
		err = errors.New("wrong weighting strategie")
	}

	return
}
