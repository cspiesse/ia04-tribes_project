package caravan

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"
	"tribes"
	agents "tribes/agents"
	"tribes/agents/petitpois"
)

// The function starts the  Caravan's server
func (ca *CaravanAgent) Start(addr string) {
	// multiplexer creation
	mux := http.NewServeMux()
	mux.HandleFunc(string("/nightfall"), ca.NightActions)
	mux.HandleFunc(string("/raid"), ca.CaravanIsRaided)

	// Création du serveur http
	s := &http.Server{
		Addr:           ":" + addr,
		Handler:        mux,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20}
	ca.Serv = s

	// Lancement du serveur
	log.Println(string(ca.ID) + " Listening on " + s.Addr)
	go log.Fatal(s.ListenAndServe())
}

// checkMethod checks the type of method of a request
func (ca *CaravanAgent) checkMethod(method string, w http.ResponseWriter, r *http.Request) bool {
	if r.Method != method {
		SendNightfallStatus(w, 501, fmt.Sprintf("method %q not allowed", r.Method), agents.TribeStats{}, tribes.Position{}, []petitpois.MissionRegistration{})
		return false
	}
	return true
}

// Helps for sending status
func SendNightfallStatus(w http.ResponseWriter, statusError int, caID string, caravanStats agents.TribeStats, caravanPos tribes.Position, registrations []petitpois.MissionRegistration) {
	var resp tribes.NightfallResponse
	resp.CaravanID = tribes.CaravanID(caID)
	resp.CaravanPos = caravanPos
	resp.CaravanStats = caravanStats
	resp.Petitpois = make(map[tribes.PetitpoisID]string)
	for _, registration := range registrations {
		resp.Petitpois[registration.ID] = registration.Role
	}

	w.WriteHeader(statusError)
	serial, _ := json.Marshal(resp)
	w.Write(serial)
	return
}

func SendRaidStatus(w http.ResponseWriter, statusError int, hp int, resources CaravanResources) {
	var resp tribes.RaidCaravanResponse
	resp.RemainingHP = hp
	resp.RemainingFood = resources.Food
	resp.RemainingMaterials = resources.Materials
	w.WriteHeader(statusError)
	serial, _ := json.Marshal(resp)
	w.Write(serial)
	return
}

func (ca *CaravanAgent) decodeNightfallRequest(r *http.Request) (req tribes.NightfallRequest, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req)
	return
}

func (ca *CaravanAgent) decodeRaidRequest(r *http.Request) (req tribes.RaidCaravanRequest, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req)
	return
}
