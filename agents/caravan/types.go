package caravan

import (
	"tribes"
	"tribes/agents"
)

// Beliefs
type CaravanBeliefs struct {
	MapBeliefs            [][]tribes.TileBelief
	BiomBeliefs           map[string]*agents.BiomeBelief
	EnemiesMapBeliefs     map[tribes.Position][]int
	EnemiesCaravanBeliefs map[tribes.CaravanID]tribes.Position
	CaravanNbRaids        []int // Number of raiders
}

// Resources
type CaravanResources struct {
	Food      int
	Materials int
	XP        int
}

// Strategies ("Agressive", "Defensive", "Gathering", "Knowledge")
type CaravanStrategy tribes.StratType

var CaravanStrategyNames = []string{"Agressive", "Defensive", "Gathering", "Knowledge"}
