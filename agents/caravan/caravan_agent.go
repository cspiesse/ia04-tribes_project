package caravan

import (
	"fmt"
	"math"
	"math/rand"
	"net/http"
	"strconv"
	"sync"
	"time"
	"tribes"
	agents "tribes/agents"
	"tribes/agents/petitpois"
	"tribes/main_server/server"
)

type CaravanAgent struct {
	sync.Mutex
	Serv                   *http.Server
	ID                     tribes.CaravanID
	HP                     int
	MaxHP                  int
	StatsXP                agents.TribeStats
	StatsResourcesLevel    agents.TribeStats
	StatsValue             agents.TribeStats
	Members                []*petitpois.Petitpois
	Resources              CaravanResources
	Position               tribes.Position
	Beliefs                CaravanBeliefs
	Strategy               CaravanStrategy
	GiveOrdersChannel      chan (petitpois.Mission)
	GetMissionRegistration chan (petitpois.MissionRegistration)
	MembersRegistrations   []petitpois.MissionRegistration
	GetBeliefsChannel      chan ([]tribes.TileBelief)
	GetResourcesChannel    chan (tribes.Inventory)
	CurrentDay             int
	ServerUrl              string
	MembersCount           int
}

func MakeCaravanes(nbCar int, serverAdress string, strategies []CaravanStrategy, startPos []tribes.Position) (caAddrs map[tribes.CaravanID]string, caInfos map[tribes.CaravanID]server.SimplifiedCaravanInfo) {

	caAddrs = make(map[tribes.CaravanID]string)
	caInfos = make(map[tribes.CaravanID]server.SimplifiedCaravanInfo)

	statsKeys := []string{"Agressive", "Defensive", "Gathering", "Knowledge"}
	rand.Shuffle(4, func(i, j int) {
		statsKeys[i], statsKeys[j] = statsKeys[j], statsKeys[i]
	})

	rand.Seed(time.Now().UnixNano())
	if len(strategies) < nbCar {
		for i := 0; i < nbCar; i++ {
			if len(strategies) <= i {
				strat := CaravanStrategy{}
				total := 100
				strat[statsKeys[0]] = float64(rand.Intn(total)) - 1
				total -= int(strat[statsKeys[0]])
				strat[statsKeys[1]] = float64(rand.Intn(total)) - 1
				total -= int(strat[statsKeys[1]])
				strat[statsKeys[2]] = float64(rand.Intn(total)) - 1
				total -= int(strat[statsKeys[2]])
				strat[statsKeys[3]] = 100 - strat[statsKeys[0]] - strat[statsKeys[1]] - strat[statsKeys[2]]
				strategies = append(strategies, strat)
			}
		}
	}
	fmt.Println(strategies)
	if len(startPos) < nbCar {
		for i := 0; i < nbCar; i++ {
			if len(startPos) <= i {
				xPos := rand.Intn(tribes.Width)
				yPos := rand.Intn(tribes.Height)
				startPos = append(startPos, tribes.Position{X: xPos, Y: yPos})
			}
		}
	}

	for i := 0; i < nbCar; i++ {
		caName := tribes.CaravanID("Caravane" + strconv.Itoa(i+1))
		ca := NewCaravanAgent(caName, serverAdress)
		ca.Init(startPos[i], strategies[i])

		caPort := "300" + strconv.Itoa(i+1)
		go ca.Start(caPort)
		caAddr := "http://localhost:" + caPort + "/"

		// -- Info for server --
		simplifiedCaravanInfo := server.SimplifiedCaravanInfo{
			Position:     ca.Position,
			CaravanStats: ca.StatsValue,
		}
		caAddrs[ca.ID] = caAddr // Comment Get l'adresse du serveur caravane ??? // TODO recreate the complete address
		caInfos[ca.ID] = simplifiedCaravanInfo
	}
	return
}

// Caravan constructor
func NewCaravanAgent(id tribes.CaravanID, url string) *CaravanAgent {
	return &CaravanAgent{
		ID:        id,
		ServerUrl: url,
	}
}

func (ca *CaravanAgent) DestroyCaravan() {
	// TODO : Que faire quand la caravane est détruite ??
	//ca.Serv.Shutdown(context.Background())
	*ca = CaravanAgent{}
}

// Caravan initialization
func (ca *CaravanAgent) Init(pos tribes.Position, strat CaravanStrategy) {

	err := CheckStrategy(tribes.StratType(strat))

	if err != nil {
		fmt.Println(err.Error())
		return
	}

	ca.GiveOrdersChannel = make(chan petitpois.Mission, tribes.MaxPetitpois)
	ca.GetBeliefsChannel = make(chan []tribes.TileBelief, tribes.MaxPetitpois)
	ca.GetResourcesChannel = make(chan tribes.Inventory, tribes.MaxPetitpois)
	ca.GetMissionRegistration = make(chan petitpois.MissionRegistration, tribes.MaxPetitpois)

	ca.InitCaravanBeliefs(tribes.Width, tribes.Height)

	ca.InitCaravanStats()
	ca.Position = pos
	ca.Strategy = strat
	ca.Resources.Food = tribes.NbStartFood
	ca.Resources.Materials = tribes.NbStartMaterials
	ca.MakeNewPetitpois(tribes.NbstartMembers)
}

// All caravan actions during the night
func (ca *CaravanAgent) NightActions(w http.ResponseWriter, r *http.Request) {

	fmt.Println(ca.ID, "recieved nightfall request !")

	fmt.Println(ca.ID, ": Here are my members :")
	for _, m := range ca.Members {
		fmt.Println(m.Id)
	}

	// Request method verification
	ca.checkMethod("POST", w, r)

	// Request method decoding
	req, err := ca.decodeNightfallRequest(r)
	if err != nil {
		SendNightfallStatus(w, 400, err.Error(), agents.TribeStats{}, tribes.Position{}, []petitpois.MissionRegistration{})
		return
	}
	ca.CurrentDay = int(req.CurrentDay)

	if ca.CurrentDay != -1 {
		// Collection phase
		backHomePetitpois := ca.CollectResources()
		dayBeliefs := ca.CollectBeliefs()

		// Thinking phase
		ca.UpdateMembers(backHomePetitpois)

		ca.UpdateBeliefs(dayBeliefs)
		ca.UpdateStrategy()
		ca.AssignXP()
		ca.AssignFood()
		ca.AssignMaterials()

		if len(ca.Members) != 0 {
			ca.MoveCaravan()
		}
	}

	for _, pp := range ca.Members {
		if !pp.Isliving {
			go pp.Live()
		}
	}

	// Day preparation
	ca.ComputePurposes()
	fmt.Println(ca.ID, ": waiting for registration : ", len(ca.GiveOrdersChannel))
	ca.GetPetitpoisRegistrations()
	fmt.Println(ca.ID, ": got all registrations !")

	if ca.CurrentDay != -1 {
		// Reset next day enemies and raid data
		ca.ResetNextDayData()
	}

	// End of nights actions, the caravan communicates that it is ready for next day
	SendNightfallStatus(w, 200, string(ca.ID), ca.StatsValue, ca.Position, ca.MembersRegistrations)
	fmt.Println(ca.ID, "sent nightfall response !")
}

// Called when the caravan is raided by another tribe
func (ca *CaravanAgent) CaravanIsRaided(w http.ResponseWriter, r *http.Request) {

	// Request method verification
	ca.checkMethod("POST", w, r)

	// Request method decoding
	req, _ := ca.decodeRaidRequest(r)

	ca.HP -= req.DamageReceived

	if ca.HP <= 0 {
		defer ca.DestroyCaravan()
	}

	// End of the raid action, the caravan communicates its remaining HP and inventory
	SendRaidStatus(w, 200, ca.HP, ca.Resources)
	fmt.Println(ca.ID, "sent raid response !")
}

// ------- Caravan Communications -------- //

func (ca *CaravanAgent) CollectResources() (backHomePetitpois []tribes.PetitpoisID) {
	for len(ca.GetResourcesChannel) > 0 {
		resources := <-ca.GetResourcesChannel
		ca.Resources.Food += resources.Food
		ca.Resources.Materials += resources.Materials
		ca.Resources.XP += resources.XP
		backHomePetitpois = append(backHomePetitpois, resources.PetitpoisID)
	}
	return
}

// Collects all the petitpois' beliefs of the day
func (ca *CaravanAgent) CollectBeliefs() (beliefs []tribes.TileBelief) {

	for len(ca.GetBeliefsChannel) > 0 {
		beliefs = append(beliefs, <-ca.GetBeliefsChannel...)
	}
	return
}

// Communicates a purpose to each petitpois
func (ca *CaravanAgent) CommunicatePurposes(purposes []petitpois.Mission) {
	for _, purpose := range purposes {
		ca.GiveOrdersChannel <- purpose
	}
}

func (ca *CaravanAgent) GetPetitpoisRegistrations() {
	ca.MembersRegistrations = []petitpois.MissionRegistration{}
	for i := 0; i < len(ca.Members); i++ {
		ca.MembersRegistrations = append(ca.MembersRegistrations, <-ca.GetMissionRegistration)
		fmt.Println(ca.ID, ": got one registration !")
	}
}

// ------- Caravan Actions ------- //

func (ca *CaravanAgent) UpdateMembers(backHomePetitpois []tribes.PetitpoisID) {
	updatedMembers := []*petitpois.Petitpois{}
	for _, member := range ca.Members {
		id := member.Id
		if tribes.Contains(backHomePetitpois, id) {
			// The petitpois made it through the day, he stays within the Caravan members
			updatedMembers = append(updatedMembers, member)
		}
	}
	ca.Members = updatedMembers
}

func (ca *CaravanAgent) UpdateBeliefs(beliefs []tribes.TileBelief) {

	// Set belief Elements to nil if no news for x days
	for i, row := range ca.Beliefs.MapBeliefs {
		for j, cell := range row {
			deltaDays := float64(tribes.Turn(math.Abs(float64(int(tribes.Turn(int(ca.CurrentDay)*int(tribes.DayDuration)))-int(cell.Turn)))) / tribes.DayDuration)
			if deltaDays >= float64(tribes.NbDaysOfMemory) {
				ca.Beliefs.MapBeliefs[i][j].Elements = tribes.Element{}
			}
		}
	}

	// Iterate over each TileBelief
	enemiesBuffer := make(map[tribes.Position][]tribes.PetitpoisID)
	for _, atom := range beliefs {
		// if the belief atom is newer than the current belief and we have knowledge of remaining resources on the tile, we replace it
		tilePos := atom.Position
		currentBelief := ca.Beliefs.MapBeliefs[tilePos.Y][tilePos.X]
		if (tribes.CompareElements(currentBelief.Elements, tribes.Element{})) || ((atom.Day >= currentBelief.Day) && (atom.Turn > currentBelief.Turn) && atom.Elements.Items["Food"] != -1 && atom.Elements.Items["Materials"] != -1) {
			ca.Beliefs.MapBeliefs[tilePos.Y][tilePos.X] = atom
		}

		// Managment of enemies positions
		if len(enemiesBuffer[tilePos]) == 0 {
			enemiesBuffer[tilePos] = atom.Elements.Enemies
		} else {
			for _, enemi := range atom.Elements.Enemies {
				if !tribes.Contains(enemiesBuffer[tilePos], enemi) {
					enemiesBuffer[tilePos] = append(enemiesBuffer[tilePos], enemi)
				}
			}
		}

		// Managment of caravans position
		if atom.Elements.Caravan != "" {
			ca.Beliefs.EnemiesCaravanBeliefs[atom.Elements.Caravan] = atom.Position
		}

		// Update of beliefs on biomes
		currentBiome := ca.Beliefs.BiomBeliefs[atom.Biome]

		// If a petitpois has been on the tile, he knows theses following details
		if atom.Elements.Items["Food"] != -1 && atom.Elements.Items["Materials"] != -1 {

			// RegenerationRate
			y := currentBelief.Position.Y
			x := currentBelief.Position.X
			if float64(currentBelief.Day)-float64(ca.Beliefs.MapBeliefs[y][x].Day) == 1 {
				// If there is only one day between the two data
				// Food
				foodAtArrival := atom.Elements.ItemsAtArrival["Food"]
				foodDelta := foodAtArrival - ca.Beliefs.MapBeliefs[y][x].Elements.Items["Food"]

				// Materials
				materialsAtArrival := atom.Elements.ItemsAtArrival["Materials"]
				materialsDelta := materialsAtArrival - ca.Beliefs.MapBeliefs[y][x].Elements.Items["Materials"]

				if materialsDelta+foodDelta > int(currentBiome.RegenerationRate) {
					currentBiome.RegenerationRate = float64(materialsDelta + foodDelta)
				}

			}
			// FoodLimit
			if currentBiome.FoodLimit < atom.Elements.Items["Food"] {
				currentBiome.FoodLimit = atom.Elements.Items["Food"]
			}
			// MaterialLimit
			if currentBiome.MaterialsLimit < atom.Elements.Items["Materials"] {
				currentBiome.MaterialsLimit = atom.Elements.Items["Materials"]
			}
			// DangerRate
			danger := 0.
			if atom.Danger {
				danger = 1.
			}
			currentBiome.DangerRate = (float64(currentBiome.NbTimesExplored)*currentBiome.DangerRate + danger) / (float64(currentBiome.NbTimesExplored) + 1)
			currentBiome.NbTimesExplored += 1
		}

	}

	// enemies map update
	enemiesMapIdx := ca.CurrentDay % tribes.NbDaysOfMemory
	for pos, enemies := range enemiesBuffer {
		// Init position if not already exists
		if len(ca.Beliefs.EnemiesMapBeliefs[pos]) == 0 {
			ca.Beliefs.EnemiesMapBeliefs[pos] = make([]int, tribes.NbDaysOfMemory)
		}
		ca.Beliefs.EnemiesMapBeliefs[pos][enemiesMapIdx] = len(enemies)
	}
	// If no knowledge anymore, delete this position
	for pos, enemies := range ca.Beliefs.EnemiesMapBeliefs {
		if tribes.CompareIntSlice(enemies, make([]int, tribes.NbDaysOfMemory)) {
			delete(ca.Beliefs.EnemiesMapBeliefs, pos)
		}
	}

}

func (ca *CaravanAgent) UpdateStrategy() {
	// TODO : En stand by (si on a le temps) : Algo génétique
	err := CheckStrategy(tribes.StratType(ca.Strategy))

	if err != nil {
		fmt.Println(err.Error())
		return
	}
}

func (ca *CaravanAgent) AssignXP() {
	xp := ca.Resources.XP

	// Allocation of XPs for each strategy
	StratXPDict := AllocateQuantityForStrategy(xp, tribes.StratType(ca.Strategy))

	// XP assignment for each strategy
	// TODO : Ne pas distribuer d'XP si le niveau max est déjà atteint (en stand by)
	for strat, xps := range StratXPDict {

		StatXPDict := AllocateQuantityForStrategy(int(xps), tribes.StratType(tribes.Strategies[strat].XPStrat))

		ca.EditTribeStats(StatXPDict)
		for _, xp := range StatXPDict {
			ca.Resources.XP -= int(xp)
		}
	}

}

func (ca *CaravanAgent) AssignResources() {

	// Maintain population if possible
	ca.FeedTribe()

	// Assign remaining food for population growth or incrasing stamina level
	ca.AssignFood()

	// Assign materials for incrasing attack, defense, speed or gathering levels
	ca.AssignMaterials()
}

func (ca *CaravanAgent) ComputePurposes() {

	purposes := []petitpois.Mission{}

	// 1. Find food to maintain and grow population (1 gatherer should be able to feed 3 petitpois)
	purposes = append(purposes, ca.ComupteFoodGatherersPurposes()...)

	// 2. If recently attacked or enemies seens, defend the caravan
	purposes = append(purposes, ca.ComputeDefenderPurposes()...)

	// 3. If not enough knowledge about the environement, make explorers
	purposes = append(purposes, ca.ComputeExplorersPurposes()...)

	// 4. Find materials to maintain and upgrade levels
	purposes = append(purposes, ca.ComputeMaterialGathererPurposes()...)

	// 5. Assign the remaining petitpois according to the strategies
	purposes = append(purposes, ca.ComputeRemainingPurposes(purposes)...)

	// 7. Send Petitpois assignments
	ca.CommunicatePurposes(purposes[:len(ca.Members)])
	return
}
