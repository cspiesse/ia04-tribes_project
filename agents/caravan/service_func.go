package caravan

import (
	"fmt"
	"math"
	"sort"
	tribes "tribes"
	agents "tribes/agents"
	petitpois "tribes/agents/petitpois"
	"tribes/randutil"
)

// Init caravan statistics
func (ca *CaravanAgent) InitCaravanStats() {

	ca.StatsXP = make(agents.TribeStats)
	ca.StatsResourcesLevel = make(agents.TribeStats)
	ca.StatsValue = make(agents.TribeStats)
	ca.MaxHP = tribes.BaseMaxCaravanHP
	ca.HP = tribes.BaseMaxCaravanHP

	for _, name := range agents.TribeStatsNames {
		ca.StatsXP[name] = 0
		ca.StatsResourcesLevel[name] = 0
		ca.StatsValue[name] = 0
	}
}

// Init caravan beliefs map
func (ca *CaravanAgent) InitCaravanBeliefs(x, y int) {
	caravanMap := make([][]tribes.TileBelief, y)
	for i := 0; i < y; i++ {
		caravanMap[i] = make([]tribes.TileBelief, x)
		for j := 0; j < x; j++ {
			caravanMap[i][j] = tribes.TileBelief{
				Position: tribes.Position{X: j, Y: i},
				Elements: tribes.Element{},
				Turn:     tribes.Turn(0),
			}
		}
	}

	biomeBeliefs := make(map[string]*agents.BiomeBelief)
	for biome := range tribes.VanillaBiomes {
		biomeBeliefs[biome] = &agents.BiomeBelief{Name: biome}
	}

	enemisMap := make(map[tribes.Position][]int)

	enemisCaravanBeliefs := make(map[tribes.CaravanID]tribes.Position)

	raids := make([]int, tribes.NbDaysOfMemory)

	ca.Beliefs = CaravanBeliefs{caravanMap, biomeBeliefs, enemisMap, enemisCaravanBeliefs, raids}
}

func (ca *CaravanAgent) ResetNextDayData() {
	nextDay := ca.CurrentDay + 1
	idx := nextDay % tribes.NbDaysOfMemory
	// Reset next day enemis positions
	for pos := range ca.Beliefs.EnemiesMapBeliefs {
		ca.Beliefs.EnemiesMapBeliefs[pos][idx] = 0
	}
	// Reset last raid memory
	ca.Beliefs.CaravanNbRaids[idx] = 0
}

// Move Caravan if not enough resources remaining or enemis seen in the caravan perception range
func (ca *CaravanAgent) MoveCaravan() {

	// --- Enemis ---

	// Compute number of enemis seens and their positions
	rangeTiles := ca.Position.RangeNeighborhood(len(ca.Beliefs.MapBeliefs), len(ca.Beliefs.MapBeliefs[0]), tribes.CaravanPerceptionRange)
	enemisAround := make(map[tribes.Position]int)
	for _, pos := range rangeTiles {
		enemisTile := ca.Beliefs.EnemiesMapBeliefs[pos]
		nbEnemi := 0
		if len(enemisTile) > 0 {
			idx := ca.CurrentDay % tribes.NbDaysOfMemory
			nbEnemi = enemisTile[idx]
		}
		if nbEnemi > 0 {
			enemisAround[pos] = nbEnemi
		}
	}

	// At least one enemi has been seen
	if len(enemisAround) > 0 {
		ca.MoveInOppositeEnemiDirection(tribes.CaravanMoveRange, enemisAround)
		return
	}

	// --- Resources ---

	// Compute max tiles range for gatherers
	moveSpeed := ca.ComputeMoveSpeed(tribes.GathererSpeedBonus)
	gathererRange := int(math.Floor((float64(tribes.DayDuration) / 2) / moveSpeed))

	rangeTiles = ca.Position.RangeNeighborhood(len(ca.Beliefs.MapBeliefs), len(ca.Beliefs.MapBeliefs[0]), gathererRange)
	//nbRangeTiles := len(rangeTiles)

	nbEmptyTiles := 0
	nbAvailableFood := 0
	nbAvailableMaterials := 0
	for _, tile := range rangeTiles {
		elements := ca.Beliefs.MapBeliefs[tile.Y][tile.X].Elements

		// Compute the number of reachable tiles to be discovered in the direct Caravan's environement
		if (tribes.CompareElements(elements, tribes.Element{})) {
			nbEmptyTiles += 1
		} else {
			nbAvailableFood += elements.Items["Food"]
			nbAvailableMaterials += elements.Items["Materials"]
		}
	}

	// If more than 90% of tiles has been explored
	//if (float64(nbRangeTiles) / float64(nbEmptyTiles)) < 0.1 {
	if true {
		// If there is not enough food available to feed the tribe
		neededFood := len(ca.Members)
		if neededFood > nbAvailableFood {
			ca.MoveToFindResource(tribes.CaravanMoveRange, "Food")
		}

		// Or if there is not enough materials available
		// TODO (en stand by)
	}

	// Move all petitpois
	for _, petitpois := range ca.Members {
		petitpois.CurrentPosition = ca.Position
	}
}

func (ca *CaravanAgent) MoveInOppositeEnemiDirection(moveRange int, enemis map[tribes.Position]int) {

	matrixHeight := len(ca.Beliefs.MapBeliefs)
	matrixWidth := len(ca.Beliefs.MapBeliefs[0])
	position := tribes.GetOppositeEnemiDirection(ca.Position, matrixHeight, matrixWidth, moveRange, enemis)
	// If strategy si agressive, move to the enemis
	if ca.Strategy["Agressive"] > ca.Strategy["Defensive"] && ca.Strategy["Agressive"] > ca.Strategy["Gathering"] && ca.Strategy["Agressive"] > ca.Strategy["Knowledge"] {
		ca.Position = tribes.TorusPos(position.X-(1+2*tribes.CaravanMoveRange), position.Y-(1+2*tribes.CaravanMoveRange), len(ca.Beliefs.MapBeliefs), len(ca.Beliefs.MapBeliefs[0]))
	} else {
		ca.Position = position
	}

	fmt.Println("Caravan moved (enemi) : ", position)
}

func (ca *CaravanAgent) MoveToFindResource(moveRange int, resourceType string) {
	bestBiomes := ca.SortBiomeByResource(resourceType)

	// Find every tiles that share this biome
	nbTilesOfBiome := 0
	idx := 0
	biomeTiles := []tribes.TileBelief{}
	for nbTilesOfBiome <= 0 && idx < len(bestBiomes) {
		biome := bestBiomes[idx]
		rangeTiles := ca.Position.RangeNeighborhood(len(ca.Beliefs.MapBeliefs), len(ca.Beliefs.MapBeliefs[0]), tribes.CaravanPerceptionRange)
		biomeTiles = []tribes.TileBelief{}
		for _, pos := range rangeTiles {
			biomeTile := ca.Beliefs.MapBeliefs[pos.Y][pos.X]
			if biomeTile.Biome != "" {
				if ca.Beliefs.BiomBeliefs[biomeTile.Biome].Name == biome.Name {
					biomeTiles = append(biomeTiles, biomeTile)
				}
			}
		}
		nbTilesOfBiome = len(biomeTiles)
		idx += 1
	}

	// Move in the direction of the half-farthest tile of this biome (to avoid being stuck in the biome)
	sortedBiomeTiles := ca.SortTilesByDistance(biomeTiles)
	i := int(math.Floor(float64(len(sortedBiomeTiles)) / 2.))
	aimedTile := ca.Beliefs.MapBeliefs[ca.Position.Y][ca.Position.X]
	if len(sortedBiomeTiles) > 0 {
		aimedTile = sortedBiomeTiles[i]
	}

	ca.Position = randutil.ChoicePosition(petitpois.PositionsInDirection(ca.Position, aimedTile.Position, tribes.CaravanMoveRange))
	fmt.Println("Caravan moved (food) : ", ca.Position)
}

// Make a new petitpois ID
func (ca *CaravanAgent) MakePetitpoisID() tribes.PetitpoisID {
	id := fmt.Sprintf("Petitpois%d_%s", ca.MembersCount, ca.ID)
	ca.MembersCount++
	return tribes.PetitpoisID(id)
}

// Make a new Petitpois
func (ca *CaravanAgent) MakeNewPetitpois(nbPetitpois int) {
	for i := 0; i < nbPetitpois; i++ {
		id := ca.MakePetitpoisID()
		newPP := petitpois.NewPetitpois(id, ca.ID, ca.Position, ca.GetBeliefsChannel, ca.GetResourcesChannel, ca.GiveOrdersChannel, ca.GetMissionRegistration, ca.ServerUrl)
		ca.Members = append(ca.Members, newPP)
	}
}

// Allocate a quantity to each statistic of a given strategy
func AllocateQuantityForStrategy(quantity int, strat tribes.StratType) (quantities tribes.StratType) {

	quantities = make(tribes.StratType)
	for name, rate := range strat {
		quantities[name] = math.Floor(rate * float64(quantity) / 100)
	}
	return
}

// Finds the level of a statistic given an amount of XP and levels thresholds
func FindLevel(currentXP int, thresholds []int) (level int) {

	for i, v := range thresholds {
		if (currentXP - v) < 0 {
			return i
		}
	}

	return len(thresholds)
}

// Computes Needs in a resource to maintain current level
func (ca *CaravanAgent) MaintainLevelNeeds(stat string) (maintain, nextLevel int) {
	pop := len(ca.Members)
	currentLevel := ca.StatsResourcesLevel[stat]

	levelDiff := float64(tribes.MaxResourceLevel - currentLevel)
	if levelDiff == 0 {
		// Case of last level
		levelDiff = 0.5
	}

	if currentLevel == 0 {
		// Case of first level
		maintain = 0
	} else {
		maintain = int(math.Ceil(float64(pop) / (levelDiff * 2. * tribes.ResourceNeeds)))
	}

	if levelDiff-1 == 0 {
		// Case of penultimate level
		nextLevel = pop - maintain
	} else if levelDiff-1 < 0 {
		// Case of last level
		nextLevel = 0
	} else {
		nextLevel = int(math.Ceil(float64(pop)/((levelDiff-1)*tribes.ResourceNeeds))) - maintain
	}

	return
}

// Assign an amount of resources for a given statistic
func (ca *CaravanAgent) AssignResourceForStat(resource int, stat string) {

	currentLevel := ca.StatsResourcesLevel[stat]

	maintainLevelNeeds, nextLevelNeeds := ca.MaintainLevelNeeds(stat)

	if resource >= maintainLevelNeeds {
		// Maintain current level and consume food
		ca.Resources.Food -= maintainLevelNeeds

		if (resource >= nextLevelNeeds) && (currentLevel < tribes.MaxResourceLevel) {
			// Level Up
			currentLevel += 1
			ca.Resources.Food -= nextLevelNeeds
		}
	} else if currentLevel > 0 {
		// Level Down
		currentLevel -= 1
	}
	ca.StatsResourcesLevel[stat] = currentLevel
	ca.StatsValue[stat] += ca.StatsResourcesLevel[stat]
}

// ------- Assign XP ------- //

// Edits the amount of XP of each statistic and modifies the current level
func (ca *CaravanAgent) EditTribeStats(xps tribes.StratType) {

	for stat, xp := range xps {
		ca.StatsXP[stat] += int(xp)
		ca.StatsValue[stat] = FindLevel(ca.StatsXP[stat], tribes.LevelThresholds)
	}
}

// ------- Assign Food ------- //

// Assign food for population growth, Stamina or save
func (ca *CaravanAgent) AssignFood() {
	foodAmount := ca.Resources.Food

	// Allocation of food for each statistic
	StratFoodDict := AllocateQuantityForStrategy(foodAmount, tribes.StratType(ca.Strategy))
	assignedFood := map[string]int{"Population": 0, "Stamina": 0, "Save": 0}
	for strat, food := range StratFoodDict {

		StatFoodDict := AllocateQuantityForStrategy(int(food), tribes.StratType(tribes.Strategies[strat].FoodStrat))
		for stat, f := range StatFoodDict {
			assignedFood[stat] += int(f)
		}

	}

	// Food distribution
	ca.GrowPopulation(assignedFood["Population"])
	ca.AssignResourceForStat(assignedFood["Stamina"], "Stamina")
}

// Feeds the Petitpois so that they survive
func (ca *CaravanAgent) FeedTribe() {
	pop := len(ca.Members)

	if ca.Resources.Food < pop {
		// There is not enough food, the tribes sacrifices the older Petitpois (it's horrible)
		starving := ca.Members[0:(pop - ca.Resources.Food)]
		ca.Members = ca.Members[(pop - ca.Resources.Food):]
		for _, individu := range starving {
			individu.Die() // TODO : changed Kill() by Die() check with Anselme/Charles
		}
	}
	ca.Resources.Food -= len(ca.Members)
}

// Feeds Petitpois to increase the population
func (ca *CaravanAgent) GrowPopulation(food int) {

	nbBirths := food / tribes.FoodNeededForBirth
	pop := len(ca.Members)

	if pop-nbBirths < 0 {
		// There is more food than Petitpois
		nbBirths = pop
	}

	ca.MakeNewPetitpois(nbBirths)
	ca.Resources.Food -= nbBirths * tribes.FoodNeededForBirth
}

// ------- Assign Materials ------- //

// Assign materials for Attack, Defense, Speed or gathering stats
func (ca *CaravanAgent) AssignMaterials() {

	materialAmount := ca.Resources.Materials

	// Repair Caravan HPs
	missingHP := ca.MaxHP - ca.HP
	if missingHP*tribes.MaterialNeedsForHPGain <= materialAmount {
		ca.HP += missingHP
		materialAmount -= missingHP * tribes.MaterialNeedsForHPGain
	} else {
		ca.HP += materialAmount / tribes.MaterialNeedsForHPGain
		materialAmount = 0
	}

	// Allocation of materials for each statistic
	StratMaterialDict := AllocateQuantityForStrategy(materialAmount, tribes.StratType(ca.Strategy))
	assignedMaterial := map[string]float64{"Attack": 0, "Defense": 0, "Speed": 0, "Gathering": 0}
	for strat, material := range StratMaterialDict {

		StatMaterialDict := AllocateQuantityForStrategy(int(material), tribes.StratType(tribes.Strategies[strat].ResourcesStrat))
		for stat, f := range StatMaterialDict {
			assignedMaterial[stat] += f
		}

	}

	// Order stats by increasing quantity of materials
	sortedAssignedMaterials := tribes.SortStrategyDict(tribes.StratType(assignedMaterial))

	// Go through stats and assign materials if possible
	for _, stat := range sortedAssignedMaterials {
		ca.AssignResourceForStat(int(assignedMaterial[stat]), stat)
	}
}

// ------- Compute Purposes ------- //
func (ca *CaravanAgent) ComputeDefenderPurposes() (defender []petitpois.Mission) {
	// If caravan recently attacked, create defenders
	raidScores := 0
	for i := 0; i < tribes.NbDaysOfMemory; i++ {
		idx := tribes.Modulo((ca.CurrentDay - i), tribes.NbDaysOfMemory)
		raidScores += ca.Beliefs.CaravanNbRaids[idx] * (tribes.NbDaysOfMemory - i) // The more the attack was recent, the more defenders the caravan creates
	}
	nbDefenders := math.Ceil(float64(raidScores) / 10) // approximately 1 defender for 2 attackers last day
	// Else, if enemis have been seen around, create defensors
	if nbDefenders == 0 {
		rangeTiles := ca.Position.RangeNeighborhood(len(ca.Beliefs.MapBeliefs), len(ca.Beliefs.MapBeliefs[0]), tribes.CaravanPerceptionRange)

		nbEnemies := 0
		for _, pos := range rangeTiles {

			if len(ca.Beliefs.EnemiesMapBeliefs[pos]) > 0 {
				nbEnemies += ca.Beliefs.EnemiesMapBeliefs[pos][ca.CurrentDay%tribes.NbDaysOfMemory]
			}
		}
		// 1 defender for 4 seen enemies, max 25% of pop
		nbDefenders = math.Ceil(float64(nbEnemies) / 4)
		if nbDefenders > math.Floor(0.25*float64(len(ca.Members))) {
			nbDefenders = math.Floor(0.25 * float64(len(ca.Members)))
		}
	}

	// Stats Computing
	stats := petitpois.PetitpoisStat{}
	stats.AttackValue = tribes.AttackBaseBonus + ca.StatsValue["Attack"] + tribes.DefenderAttackBonus
	stats.DefenseValue = ca.StatsValue["Defense"] + tribes.DefenderDefenseBonus
	stats.WisdomCoef = 1 + (float64(ca.StatsValue["Wisdom"]) / tribes.WisdomDivider)
	stats.SeeRange = tribes.SeeRangeBaseBonus + int(math.Ceil(float64(ca.StatsValue["Perception"]+tribes.DefenderPerceptionBonus)/tribes.SeeRangeDivider))
	stats.MoveSpeed = ca.ComputeMoveSpeed(tribes.DefenderSpeedBonus)
	stats.CollectSpeed = tribes.CollectSpeedBaseBonus + ca.StatsValue["Gathering"] + tribes.DefenderCollectBonus
	stats.LuckCoef = 1 + (float64(ca.StatsValue["Luck"]) / tribes.LuckDivider)

	inventorySize := tribes.InventoryBaseSize + tribes.DefenderInventorySizeBonus + ca.StatsValue["Gathering"]

	for i := 0; i < int(nbDefenders); i++ {
		newMission := petitpois.Mission{
			Role:            &petitpois.DefenderRole{},
			Goal:            petitpois.Goal{Position: ca.Position},
			Stats:           stats,
			InventorySize:   inventorySize,
			CaravanPosition: ca.Position,
		}
		defender = append(defender, newMission)
	}

	return
}

// Finds how much food is needed for next night
func (ca *CaravanAgent) ComupteFoodGatherersPurposes() (gatherers []petitpois.Mission) {

	foodNeeds := 3 * len(ca.Members) // 1*len(pop) to feed all petitpois + 1*len(pop) to grow pop + 1*len(pop) to store or upgrade stamina

	// Stats Computing
	stats := petitpois.PetitpoisStat{}
	stats.AttackValue = tribes.AttackBaseBonus + ca.StatsValue["Attack"] + tribes.GathererAttackBonus
	stats.DefenseValue = ca.StatsValue["Defense"] + tribes.GathererDefenseBonus
	stats.WisdomCoef = 1 + (float64(ca.StatsValue["Wisdom"]) / tribes.WisdomDivider)
	stats.SeeRange = tribes.SeeRangeBaseBonus + int(math.Ceil(float64(ca.StatsValue["Perception"]+tribes.GathererPerceptionBonus)/tribes.SeeRangeDivider))
	stats.MoveSpeed = ca.ComputeMoveSpeed(tribes.GathererSpeedBonus)
	stats.CollectSpeed = tribes.CollectSpeedBaseBonus + ca.StatsValue["Gathering"] + tribes.GathererCollectBonus
	stats.LuckCoef = 1 + (float64(ca.StatsValue["Luck"]) / tribes.LuckDivider)

	inventorySize := tribes.InventoryBaseSize + tribes.GathererInventorySizeBonus + ca.StatsValue["Gathering"]
	foodSize := math.Ceil(0.8 * float64(inventorySize))
	nbGatherers := int(math.Ceil(float64(foodNeeds) / foodSize))

	// Compute positions to gather

	// Compute max tiles range for gatherers
	moveSpeed := ca.ComputeMoveSpeed(tribes.GathererSpeedBonus)
	gathererRange := int(math.Floor((float64(tribes.DayDuration) / 2) / moveSpeed))
	rangeTiles := ca.Position.RangeNeighborhood(len(ca.Beliefs.MapBeliefs), len(ca.Beliefs.MapBeliefs[0]), gathererRange)

	positions := ca.ComputeGathererPositions(rangeTiles, "Food", nbGatherers)

	for i := 0; i < nbGatherers; i++ {
		newMission := petitpois.Mission{
			Role: &petitpois.CollectorRole{},
			Goal: petitpois.Goal{
				Position: positions[i],
				// makes gatherers with 80% food collection and 20% materials collection
				Collect: map[tribes.ItemType]float64{
					"Food":      foodSize,
					"Materials": float64(inventorySize - int(foodSize))},
			},
			Stats:           stats,
			InventorySize:   inventorySize,
			CaravanPosition: ca.Position,
		}
		gatherers = append(gatherers, newMission)
	}

	return
}

// Finds how much food is needed for next night
func (ca *CaravanAgent) ComputeMaterialGathererPurposes() (gatherers []petitpois.Mission) {

	stats := []string{"Attack", "Defense", "Speed", "Gathering"}
	materialNeeds := 0
	for _, stat := range stats {
		maintainNeeds, _ := ca.MaintainLevelNeeds(stat)
		materialNeeds += maintainNeeds
	}
	materialNeeds = int(float64(materialNeeds) * 1.5) // x1.5 to collect materials in order to upgrade at least one stat if possible

	// Stats Computing
	petitpoisStats := petitpois.PetitpoisStat{}
	petitpoisStats.AttackValue = tribes.AttackBaseBonus + ca.StatsValue["Attack"] + tribes.GathererAttackBonus
	petitpoisStats.DefenseValue = ca.StatsValue["Defense"] + tribes.GathererDefenseBonus
	petitpoisStats.WisdomCoef = 1 + (float64(ca.StatsValue["Wisdom"]) / tribes.WisdomDivider)
	petitpoisStats.SeeRange = tribes.SeeRangeBaseBonus + int(math.Ceil(float64(ca.StatsValue["Perception"]+tribes.GathererPerceptionBonus)/tribes.SeeRangeDivider))
	petitpoisStats.MoveSpeed = ca.ComputeMoveSpeed(tribes.GathererSpeedBonus)
	petitpoisStats.CollectSpeed = tribes.CollectSpeedBaseBonus + ca.StatsValue["Gathering"] + tribes.GathererCollectBonus
	petitpoisStats.LuckCoef = 1 + (float64(ca.StatsValue["Luck"]) / tribes.LuckDivider)

	inventorySize := tribes.InventoryBaseSize + tribes.GathererInventorySizeBonus + ca.StatsValue["Gathering"]
	materialSize := math.Ceil(0.8 * float64(inventorySize))
	nbGatherers := int(math.Ceil(float64(materialNeeds) / materialSize))

	// Compute max tiles range for gatherers
	moveSpeed := ca.ComputeMoveSpeed(tribes.GathererSpeedBonus)
	gathererRange := int(math.Floor((float64(tribes.DayDuration) / 2) / moveSpeed))

	// Compute positions to gather
	rangeTiles := ca.Position.RangeNeighborhood(len(ca.Beliefs.MapBeliefs), len(ca.Beliefs.MapBeliefs[0]), gathererRange)

	positions := ca.ComputeGathererPositions(rangeTiles, "Material", nbGatherers)

	for i := 0; i < nbGatherers; i++ {
		newMission := petitpois.Mission{
			Role: &petitpois.CollectorRole{},
			Goal: petitpois.Goal{
				Position: positions[i],
				// makes gatherers with 80% materials collection and 20% food collection
				Collect: map[tribes.ItemType]float64{
					"Materials": materialSize,
					"Food":      float64(inventorySize - int(materialSize))},
			},
			Stats:           petitpoisStats,
			InventorySize:   inventorySize,
			CaravanPosition: ca.Position,
		}
		gatherers = append(gatherers, newMission)
	}

	return
}

func (ca *CaravanAgent) ComputeExplorersPurposes() (explorers []petitpois.Mission) {
	// Compute max tiles range for explorer
	moveSpeed := ca.ComputeMoveSpeed(tribes.ExplorerSpeedBonus)
	explorerRange := int(math.Floor((float64(tribes.DayDuration) / 2) / moveSpeed))

	rangeTiles := ca.Position.RangeNeighborhood(len(ca.Beliefs.MapBeliefs), len(ca.Beliefs.MapBeliefs[0]), explorerRange)

	emptyTiles := []tribes.TileBelief{}
	for _, tile := range rangeTiles {
		elements := ca.Beliefs.MapBeliefs[tile.Y][tile.X].Elements

		// Compute the number of reachable tiles to be discovered in the direct Caravan's environement
		if (tribes.CompareElements(elements, tribes.Element{})) {
			emptyTiles = append(emptyTiles, ca.Beliefs.MapBeliefs[tile.Y][tile.X])
		}
	}

	// If not enough tiles are discovered, or there is less available food or materials than needed, make explorers
	nbExplorers := int(math.Floor(float64(len(emptyTiles)) / float64(tribes.ExplorersPerEmptyTiles))) // Make an explorer every x empty tiles

	// Compute positions to explore
	positions := ca.ComputeExplorerPositions(emptyTiles, nbExplorers)

	// Stats Computing
	stats := petitpois.PetitpoisStat{}
	stats.AttackValue = tribes.AttackBaseBonus + ca.StatsValue["Attack"] + tribes.ExplorerAttackBonus
	stats.DefenseValue = ca.StatsValue["Defense"] + tribes.ExplorerDefenseBonus
	stats.WisdomCoef = 1 + (float64(ca.StatsValue["Wisdom"]) / tribes.WisdomDivider)
	stats.SeeRange = tribes.SeeRangeBaseBonus + int(math.Ceil(float64(ca.StatsValue["Perception"]+tribes.ExplorerPerceptionBonus)/tribes.SeeRangeDivider))
	stats.MoveSpeed = ca.ComputeMoveSpeed(tribes.ExplorerSpeedBonus)
	stats.CollectSpeed = tribes.CollectSpeedBaseBonus + ca.StatsValue["Gathering"] + tribes.ExplorerCollectBonus
	stats.LuckCoef = 1 + (float64(ca.StatsValue["Luck"]) / tribes.LuckDivider)

	// Make missions
	inventorySize := tribes.InventoryBaseSize + tribes.ExplorerInventorySizeBonus + ca.StatsValue["Gathering"]
	for i := 0; i < nbExplorers; i++ {
		newMission := petitpois.Mission{
			Role:            &petitpois.ExplorerRole{},
			Goal:            petitpois.Goal{Position: positions[i]},
			Stats:           stats,
			InventorySize:   inventorySize,
			CaravanPosition: ca.Position,
		}
		explorers = append(explorers, newMission)
	}

	return
}

func (ca *CaravanAgent) ComputeRemainingPurposes(purposes []petitpois.Mission) (missions []petitpois.Mission) {
	nbCurrentPurposes := len(purposes)
	nbRemainingPurposes := len(ca.Members) - nbCurrentPurposes
	if nbRemainingPurposes <= 0 {
		return
	}

	StratDict := AllocateQuantityForStrategy(nbRemainingPurposes, tribes.StratType(ca.Strategy))
	for _, n := range StratDict {
		nbRemainingPurposes -= int(n)
	}
	// Make the remaining petitpois defend the caravan
	StratDict["Defensive"] += float64(nbRemainingPurposes)

	// --- Agressive ---
	// Compute max tiles range for attacker
	moveSpeed := ca.ComputeMoveSpeed(tribes.AttackerSpeedBonus)
	attackerRange := int(math.Floor((float64(tribes.DayDuration) / 2) / moveSpeed))

	rangeTiles := ca.Position.RangeNeighborhood(len(ca.Beliefs.MapBeliefs), len(ca.Beliefs.MapBeliefs[0]), attackerRange)

	// Compute positions to attack
	positions := ca.ComputeAttackerPositions(rangeTiles, int(StratDict["Agressive"]))

	// Stats Computing
	stats := petitpois.PetitpoisStat{}
	stats.AttackValue = tribes.AttackBaseBonus + ca.StatsValue["Attack"] + tribes.AttackerAttackBonus
	stats.DefenseValue = ca.StatsValue["Defense"] + tribes.AttackerDefenseBonus
	stats.WisdomCoef = 1 + (float64(ca.StatsValue["Wisdom"]) / tribes.WisdomDivider)
	stats.SeeRange = tribes.SeeRangeBaseBonus + int(math.Ceil(float64(ca.StatsValue["Perception"]+tribes.AttackerPerceptionBonus)/tribes.SeeRangeDivider))
	stats.MoveSpeed = ca.ComputeMoveSpeed(tribes.AttackerSpeedBonus)
	stats.CollectSpeed = tribes.CollectSpeedBaseBonus + ca.StatsValue["Gathering"] + tribes.AttackerCollectBonus
	stats.LuckCoef = 1 + (float64(ca.StatsValue["Luck"]) / tribes.LuckDivider)

	inventorySize := tribes.InventoryBaseSize + tribes.AttackerInventorySizeBonus + ca.StatsValue["Gathering"]
	for i := 0; i < int(StratDict["Agressive"]); i++ {
		newMission := petitpois.Mission{
			Role:            &petitpois.AttackerRole{},
			Goal:            petitpois.Goal{Position: positions[i]},
			Stats:           stats,
			InventorySize:   inventorySize,
			CaravanPosition: ca.Position,
		}
		missions = append(missions, newMission)
	}

	// --- Defensive ---
	// Stats Computing
	stats = petitpois.PetitpoisStat{}
	stats.AttackValue = tribes.AttackBaseBonus + ca.StatsValue["Attack"] + tribes.DefenderAttackBonus
	stats.DefenseValue = ca.StatsValue["Defense"] + tribes.DefenderDefenseBonus
	stats.WisdomCoef = 1 + (float64(ca.StatsValue["Wisdom"]) / tribes.WisdomDivider)
	stats.SeeRange = tribes.SeeRangeBaseBonus + int(math.Ceil(float64(ca.StatsValue["Perception"]+tribes.DefenderPerceptionBonus)/tribes.SeeRangeDivider))
	stats.MoveSpeed = ca.ComputeMoveSpeed(tribes.DefenderSpeedBonus)
	stats.CollectSpeed = tribes.CollectSpeedBaseBonus + ca.StatsValue["Gathering"] + tribes.DefenderCollectBonus
	stats.LuckCoef = 1 + (float64(ca.StatsValue["Luck"]) / tribes.LuckDivider)

	inventorySize = tribes.InventoryBaseSize + tribes.DefenderInventorySizeBonus + ca.StatsValue["Gathering"]
	for i := 0; i < int(StratDict["Defensive"]); i++ {
		newMission := petitpois.Mission{
			Role:            &petitpois.DefenderRole{},
			Goal:            petitpois.Goal{Position: ca.Position},
			Stats:           stats,
			InventorySize:   inventorySize,
			CaravanPosition: ca.Position,
		}
		missions = append(missions, newMission)
	}

	// --- Gathering ---
	// Compute max tiles range for gatherer
	moveSpeed = ca.ComputeMoveSpeed(tribes.GathererSpeedBonus)
	gathererRange := int(math.Floor((float64(tribes.DayDuration) / 2) / moveSpeed))

	rangeTiles = ca.Position.RangeNeighborhood(len(ca.Beliefs.MapBeliefs), len(ca.Beliefs.MapBeliefs[0]), gathererRange)
	rangeTiles = RemoveAssignedTiles(append(purposes, missions...), rangeTiles)

	// Compute positions to gather
	positions = ca.ComputeGathererPositions(rangeTiles, "Both", int(StratDict["Gathering"]))

	// Make missions
	// Stats Computing
	stats = petitpois.PetitpoisStat{}
	stats.AttackValue = tribes.AttackBaseBonus + ca.StatsValue["Attack"] + tribes.GathererAttackBonus
	stats.DefenseValue = ca.StatsValue["Defense"] + tribes.GathererDefenseBonus
	stats.WisdomCoef = 1 + (float64(ca.StatsValue["Wisdom"]) / tribes.WisdomDivider)
	stats.SeeRange = tribes.SeeRangeBaseBonus + int(math.Ceil(float64(ca.StatsValue["Perception"]+tribes.GathererPerceptionBonus)/tribes.SeeRangeDivider))
	stats.MoveSpeed = ca.ComputeMoveSpeed(tribes.GathererSpeedBonus)
	stats.CollectSpeed = tribes.CollectSpeedBaseBonus + ca.StatsValue["Gathering"] + tribes.GathererCollectBonus
	stats.LuckCoef = 1 + (float64(ca.StatsValue["Luck"]) / tribes.LuckDivider)

	inventorySize = tribes.InventoryBaseSize + tribes.GathererInventorySizeBonus + ca.StatsValue["Gathering"]
	materialSize := math.Ceil(0.5 * float64(inventorySize))

	for i := 0; i < int(StratDict["Gathering"]); i++ {
		newMission := petitpois.Mission{
			Role: &petitpois.CollectorRole{},
			Goal: petitpois.Goal{
				Position: positions[i],
				// makes gatherers with 50% materials collection and 50% food collection
				Collect: map[tribes.ItemType]float64{
					"Materials": materialSize,
					"Food":      float64(inventorySize - int(materialSize))},
			},
			Stats:           stats,
			InventorySize:   inventorySize,
			CaravanPosition: ca.Position,
		}
		missions = append(missions, newMission)
	}

	// --- Knowledge ---
	// Compute max tiles range for explorer
	moveSpeed = ca.ComputeMoveSpeed(tribes.ExplorerSpeedBonus)
	explorerRange := int(math.Floor((float64(tribes.DayDuration) / 2) / moveSpeed))

	rangeTiles = ca.Position.RangeNeighborhood(len(ca.Beliefs.MapBeliefs), len(ca.Beliefs.MapBeliefs[0]), explorerRange)
	rangeTiles = RemoveAssignedTiles(append(purposes, missions...), rangeTiles)

	emptyTiles := []tribes.TileBelief{}
	remainingTiles := []tribes.TileBelief{}
	for _, tile := range rangeTiles {
		elements := ca.Beliefs.MapBeliefs[tile.Y][tile.X].Elements

		// Compute the number of reachable tiles to be discovered in the direct Caravan's environement
		if (tribes.CompareElements(elements, tribes.Element{})) {
			emptyTiles = append(emptyTiles, ca.Beliefs.MapBeliefs[tile.Y][tile.X])
		} else {
			remainingTiles = append(remainingTiles, ca.Beliefs.MapBeliefs[tile.Y][tile.X])
		}
	}

	// Compute positions to explore
	if len(emptyTiles) < int(StratDict["Knowledge"]) {
		// There are not enough unexplored tiles to be assigned
		delta := int(StratDict["Knowledge"]) - len(emptyTiles)
		positions = ca.ComputeExplorerPositions(emptyTiles, len(emptyTiles))

		// Assign tiles with older knowledge
		remainingTiles = ca.SortTilesBySeniority(remainingTiles)
		// Select a subset of tiles
		remainingTiles = remainingTiles[:delta*3]
		positions = append(positions, ca.ComputeExplorerPositions(remainingTiles, delta)...)
	} else {
		positions = ca.ComputeExplorerPositions(emptyTiles, int(StratDict["Knowledge"]))
	}

	// Stats Computing
	stats = petitpois.PetitpoisStat{}
	stats.AttackValue = tribes.AttackBaseBonus + ca.StatsValue["Attack"] + tribes.ExplorerAttackBonus
	stats.DefenseValue = ca.StatsValue["Defense"] + tribes.ExplorerDefenseBonus
	stats.WisdomCoef = 1 + (float64(ca.StatsValue["Wisdom"]) / tribes.WisdomDivider)
	stats.SeeRange = tribes.SeeRangeBaseBonus + int(math.Ceil(float64(ca.StatsValue["Perception"]+tribes.ExplorerPerceptionBonus)/tribes.SeeRangeDivider))
	stats.MoveSpeed = ca.ComputeMoveSpeed(tribes.ExplorerSpeedBonus)
	stats.CollectSpeed = tribes.CollectSpeedBaseBonus + ca.StatsValue["Gathering"] + tribes.ExplorerCollectBonus
	stats.LuckCoef = 1 + (float64(ca.StatsValue["Luck"]) / tribes.LuckDivider)

	inventorySize = tribes.InventoryBaseSize + tribes.ExplorerInventorySizeBonus + ca.StatsValue["Gathering"]

	// Make missions
	for i := 0; i < int(StratDict["Knowledge"]); i++ {
		newMission := petitpois.Mission{
			Role:            &petitpois.ExplorerRole{},
			Goal:            petitpois.Goal{Position: positions[i]},
			Stats:           stats,
			InventorySize:   inventorySize,
			CaravanPosition: ca.Position,
		}
		missions = append(missions, newMission)
	}

	return
}

// ------- Compute Positions ------- //
func (ca *CaravanAgent) ComputeAttackerPositions(rangeTiles []tribes.Position, nbAttacker int) (positions []tribes.Position) {

	if nbAttacker > 0 {

		for len(rangeTiles) < nbAttacker {
			rangeTiles = append(rangeTiles, rangeTiles...)
		}

		// An enemis caravan has been discovered, attack the nearest with all attackers
		if len(ca.Beliefs.EnemiesCaravanBeliefs) > 0 {
			enemisCaravans := make([]tribes.TileBelief, len(ca.Beliefs.EnemiesCaravanBeliefs))
			for _, pos := range ca.Beliefs.EnemiesCaravanBeliefs {
				enemisCaravans = append(enemisCaravans, ca.Beliefs.MapBeliefs[pos.Y][pos.X])
			}

			sortedCaravans := ca.SortTilesByDistance(enemisCaravans)
			caravanPosition := sortedCaravans[len(sortedCaravans)-1].Position

			for i := 0; i < nbAttacker; i++ {
				positions = append(positions, caravanPosition)
			}

			return
		}

		// No enemis caravan has been discovered, send attackers where enemies were seen
		enemiesTiles := []tribes.TileBelief{}
		// Keep tiles with enemies in range of the caravan
		tiles := make([]tribes.TileBelief, len(rangeTiles))
		for _, pos := range rangeTiles {
			tile := ca.Beliefs.MapBeliefs[pos.Y][pos.X]
			if len(ca.Beliefs.EnemiesMapBeliefs[pos]) > 0 {
				enemiesTiles = append(enemiesTiles, tile)
			} else {
				tiles = append(tiles, tile)
			}
		}

		sortedTiles := ca.SortTilesByEnemies(enemiesTiles)

		// Send attackers by groups of AttackersInGroups
		nbPositions := nbAttacker
		for _, tile := range sortedTiles {
			groupSize := tribes.AttackersInGroups
			nbPositions -= groupSize
			if nbPositions < 0 {
				groupSize = tribes.AttackersInGroups - nbPositions
				nbPositions = 0
			}
			for j := 0; j < groupSize; j++ {
				positions = append(positions, tile.Position)
			}
			if nbPositions == 0 {
				break
			}
		}

		if nbPositions > 0 {
			positions = append(positions, ca.GetFarthestTiles(tiles, nbPositions)...)
		}
	}
	return

}

func (ca *CaravanAgent) ComputeGathererPositions(rangeTiles []tribes.Position, resource string, nbGatherers int) (positions []tribes.Position) {

	if nbGatherers > 0 {

		if len(rangeTiles) < nbGatherers {
			rangeTiles = append(rangeTiles, ca.Position.RangeNeighborhood(len(ca.Beliefs.MapBeliefs), len(ca.Beliefs.MapBeliefs[0]), 2)...)
		}

		for len(rangeTiles) < nbGatherers {
			rangeTiles = append(rangeTiles, rangeTiles...)
		}

		tiles := []tribes.TileBelief{}
		for _, tile := range rangeTiles {
			tiles = append(tiles, ca.Beliefs.MapBeliefs[tile.Y][tile.X])
		}

		mapHeight := len(ca.Beliefs.MapBeliefs)
		mapWidth := len(ca.Beliefs.MapBeliefs[0])

		// Sort tiles by amount of available resource
		sortedTiles := ca.SortTilesByResource(tiles, resource, mapHeight, mapWidth)

		// Assign tiles in this order
		for i := 0; i < len(sortedTiles); i++ {
			positions = append(positions, sortedTiles[i].Position) // if not enough tiles, repeat assignment on sorted tiles
		}

	}

	return
}

func (ca *CaravanAgent) ComputeExplorerPositions(emptyTiles []tribes.TileBelief, nbExplorers int) (positions []tribes.Position) {

	if nbExplorers > 0 {

		positions = ca.GetFarthestTiles(emptyTiles, nbExplorers)

	}
	return
}

// ------- Sort Tiles ------- //
func RemoveAssignedTiles(purposes []petitpois.Mission, rangeTiles []tribes.Position) (tiles []tribes.Position) {

	for _, tile := range rangeTiles {
		notInPurposes := true
		for _, purpose := range purposes {
			if tile == purpose.Goal.Position {
				notInPurposes = false
			}
		}
		if notInPurposes {
			tiles = append(tiles, tile)
		}
	}

	return
}

// Sorts tiles by farthest distance from position
func (ca *CaravanAgent) SortTilesByEnemies(tiles []tribes.TileBelief) (sortedTiles []tribes.TileBelief) {
	sortedTiles = tiles[:]
	sort.SliceStable(sortedTiles, func(i, j int) bool {
		iScore := 0
		iPos := tiles[i].Position
		for k := 0; k < len(ca.Beliefs.EnemiesMapBeliefs[iPos]); k++ {
			idx := tribes.Modulo((ca.CurrentDay - k), tribes.NbDaysOfMemory)
			weight := len(ca.Beliefs.EnemiesMapBeliefs[iPos]) - k
			iScore += weight * ca.Beliefs.EnemiesMapBeliefs[iPos][idx]
		}
		jScore := 0
		jPos := tiles[j].Position
		for k := 0; k < len(ca.Beliefs.EnemiesMapBeliefs[jPos]); k++ {
			idx := tribes.Modulo((ca.CurrentDay - k), tribes.NbDaysOfMemory)
			weight := len(ca.Beliefs.EnemiesMapBeliefs[jPos]) - k
			jScore += weight * ca.Beliefs.EnemiesMapBeliefs[jPos][idx]
		}
		return iScore > jScore
	})
	return
}

func (ca *CaravanAgent) SortTilesByDistance(tiles []tribes.TileBelief) (sortedTiles []tribes.TileBelief) {
	mapHeight := len(ca.Beliefs.MapBeliefs)
	mapWidth := len(ca.Beliefs.MapBeliefs[0])
	sortedTiles = tiles[:]
	sort.SliceStable(sortedTiles, func(i, j int) bool {
		iDistance := tribes.TaxiCabDistance(tiles[i].Position, ca.Position, mapHeight, mapWidth)
		jDistance := tribes.TaxiCabDistance(tiles[j].Position, ca.Position, mapHeight, mapWidth)
		return iDistance > jDistance
	})
	return
}

func (ca *CaravanAgent) GetFarthestTiles(tiles []tribes.TileBelief, nbPetitpois int) (positions []tribes.Position) {

	for len(tiles) < nbPetitpois {
		tiles = append(tiles, tiles...)
	}

	mapHeight := len(ca.Beliefs.MapBeliefs)
	mapWidth := len(ca.Beliefs.MapBeliefs[0])

	sortedTiles := ca.SortTilesByDistance(tiles)

	// Take the farthest tile as a first tile
	positions = append(positions, sortedTiles[0].Position)
	sortedTiles = sortedTiles[1:] // pop used tile

	// Take the farthest tiles from all already selected tiles
	for i := 1; i < nbPetitpois; i++ {
		bestDistanceScore := 0
		bestTile := sortedTiles[0]
		bestIdx := 0
		for idx, tile := range sortedTiles[1:] {
			distanceScore := 1
			for _, posTile := range positions {
				distanceScore *= tribes.TaxiCabDistance(posTile, tile.Position, mapHeight, mapWidth)
			}

			if distanceScore > bestDistanceScore {
				bestDistanceScore = distanceScore
				bestTile = tile
				bestIdx = idx + 1
			}
		}
		positions = append(positions, bestTile.Position)
		sortedTiles = append(sortedTiles[:bestIdx], sortedTiles[bestIdx+1:]...)
	}
	return
}

func (ca *CaravanAgent) SortTilesByResource(tiles []tribes.TileBelief, resource string, mapHeight, mapWidth int) (sortedTiles []tribes.TileBelief) {
	sortedTiles = tiles[:]
	sort.SliceStable(sortedTiles, func(i, j int) bool {
		iResource, jResource := 0., 0.
		if resource == "Food" {
			iResource = float64(tiles[i].Elements.Items["Food"]) + GetRegenerationRate(ca.Beliefs.BiomBeliefs[tiles[i].Biome])
			jResource = float64(tiles[j].Elements.Items["Food"]) + GetRegenerationRate(ca.Beliefs.BiomBeliefs[tiles[j].Biome])
		} else if resource == "Material" {
			iResource = float64(tiles[i].Elements.Items["Materials"]) + GetRegenerationRate(ca.Beliefs.BiomBeliefs[tiles[i].Biome])
			jResource = float64(tiles[j].Elements.Items["Materials"]) + GetRegenerationRate(ca.Beliefs.BiomBeliefs[tiles[j].Biome])
		} else if resource == "Both" {
			iResource = float64(tiles[i].Elements.Items["Food"]+tiles[i].Elements.Items["Materials"]) + GetRegenerationRate(ca.Beliefs.BiomBeliefs[tiles[i].Biome])
			jResource = float64(tiles[j].Elements.Items["Food"]+tiles[j].Elements.Items["Materials"]) + GetRegenerationRate(ca.Beliefs.BiomBeliefs[tiles[j].Biome])
		}

		return iResource > jResource
	})
	return
}

func GetRegenerationRate(bb *agents.BiomeBelief) float64 {
	if bb == nil {
		return 0
	}
	return bb.RegenerationRate
}

func (ca *CaravanAgent) SortTilesBySeniority(tiles []tribes.TileBelief) (sortedTiles []tribes.TileBelief) {
	sortedTiles = tiles[:]
	sort.SliceStable(sortedTiles, func(i, j int) bool {
		iTileBelief := ca.Beliefs.MapBeliefs[tiles[i].Position.Y][tiles[i].Position.X]
		jTileBelief := ca.Beliefs.MapBeliefs[tiles[j].Position.Y][tiles[j].Position.X]
		return iTileBelief.Turn < jTileBelief.Turn
	})
	return
}

func (ca *CaravanAgent) SortBiomeByResource(resourceType string) (sortedBiomes []*agents.BiomeBelief) {
	biomes := []*agents.BiomeBelief{}
	for _, b := range ca.Beliefs.BiomBeliefs {
		biomes = append(biomes, b)
	}
	sortedBiomes = biomes[:]
	sort.SliceStable(sortedBiomes, func(i, j int) bool {
		iMaxResource := 0
		jMaxResource := 0
		if resourceType == "Food" {
			iMaxResource = biomes[i].FoodLimit
			jMaxResource = biomes[j].FoodLimit
		} else if resourceType == "Materials" {
			iMaxResource = biomes[i].MaterialsLimit
			jMaxResource = biomes[j].MaterialsLimit
		} else {
			iMaxResource = biomes[i].MaterialsLimit + biomes[i].FoodLimit
			jMaxResource = biomes[j].MaterialsLimit + biomes[j].FoodLimit
		}

		return iMaxResource > jMaxResource
	})
	return
}

func (ca *CaravanAgent) ComputeMoveSpeed(speedBonus int) (moveSpeed float64) {
	totalSpeed := ca.StatsValue["Speed"] + speedBonus
	if totalSpeed < tribes.MoveSpeedBaseBonus {
		moveSpeed = float64(tribes.MoveSpeedBaseBonus - totalSpeed)
	} else {
		moveSpeed = 1 / (float64(totalSpeed - tribes.MoveSpeedBaseBonus - 2))
	}
	return
}
