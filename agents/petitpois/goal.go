package petitpois

import (
	"errors"
	world "tribes"
)

// Goal is a struct that contains the information about the goal of a Petitpois: where he has to go and what proportions of materials/food he has to get (if he's a collector).
type Goal struct {
	Position world.Position
	Collect  map[world.ItemType]float64
}

// NewGoal returns a new Goal with the given position and collect proportions.
func NewGoal(position world.Position, collect map[world.ItemType]float64) (*Goal, error) {
	s := 0.0
	for _, percentage := range collect {
		s += percentage
	}
	if s != 1.0 {
		return nil, errors.New("Goal percentages must sum to 1")
	}
	return &Goal{
		Position: position,
		Collect:  collect,
	}, nil
}
