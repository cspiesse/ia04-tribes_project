package petitpois

import (
	"fmt"
	world "tribes"
)

// Defender Role
type DefenderRole struct {
}

func NewDefenderRole() *DefenderRole {
	return &DefenderRole{}
}

func (dr *DefenderRole) Name() string {
	return "Defender"
}

func (dr *DefenderRole) Think(p *Petitpois) RoadMap {
	// ------- When it's night
	if p.TimeLeft == 0 && p.CurrentPosition == p.CaravanPosition {
		p.WaitingTime = 0
		fmt.Println(p.Id, " : I'm sleepy (No time left)")
		return SleepRoadMap{}
	}
	if p.TimeLeft == -1 {
		p.WaitingTime = 0
		return DieRoadMap{}
	}
	// ------- When the agent has to fight
	if len(world.Last(p.Belief[p.CurrentPosition]).Elements.Enemies) > 0 {
		return AttackRoadMap{}
	}
	return IdleRoadMap{}
}
