package petitpois

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	world "tribes"
)

type RoadMap interface{}

type MoveRoadMap struct {
	Position world.Position
}

type CollectRoadMap struct {
	CollectType     world.ItemType
	CollectQuantity int
}

type AttackRoadMap struct {
}

type RaidRoadMap struct {
}

type IdleRoadMap struct {
}

type SleepRoadMap struct {
}

type DieRoadMap struct {
}

func (p *Petitpois) Idle(rm IdleRoadMap) error {
	fmt.Println("Idle")
	req := world.ActRequest{
		PetitpoisID: p.Id,
		Type:        "IdleRequest",
		Request: world.IdleRequest{
			PetitpoisID: p.Id,
		},
	}
	data, err := json.Marshal(req)
	if err != nil {
		return err
	}
	_, err = http.Post(p.ServerUrl+"act", "application/json", bytes.NewBuffer(data))
	if err != nil {
		return err
	}
	return nil
}

// Asks the server to move the petitpois to the desired position
func (p *Petitpois) Move(rm MoveRoadMap) error {
	to := rm.Position
	req := world.ActRequest{
		PetitpoisID: p.Id,
		Type:        "MoveRequest",
		Request: world.MoveRequest{
			PetitpoisID: p.Id,
			Luck:        p.Stats.LuckCoef,
			To:          to,
			Pv:          p.Pv,
		},
	}
	data, err := json.Marshal(req)
	if err != nil {
		return err
	}
	tresp, err := http.Post(p.ServerUrl+"act", "application/json", bytes.NewBuffer(data))
	if err != nil {
		return err
	}
	buf := new(bytes.Buffer)
	buf.ReadFrom(tresp.Body)
	var resp world.MoveResponse
	json.Unmarshal(buf.Bytes(), &resp)

	// Handling of the event
	switch resp.AffectedStatByEvent {
	case "Food":
		p.Inventory.AddOrRemoveFood(resp.Quantity)
	case "Materials":
		p.Inventory.AddOrRemoveMaterials(resp.Quantity)
	case "HP":
		p.GetPv(resp.Quantity)
	case "XP":
		p.Inventory.AddOrRemoveXp(resp.Quantity)
	}

	if resp.Quantity < 0 {
		p.BadEvent = true
	}

	// Handling of the move
	p.Inventory.AddOrRemoveXp(int(float64(world.TaxiCabDistance(p.CurrentPosition, to, world.Height, world.Width)) * p.Stats.WisdomCoef))
	p.CurrentPosition = to
	return nil
}

// Asks to collect a given quantity of a given item type
func (p *Petitpois) Collect(rm CollectRoadMap) error {
	itemType := rm.CollectType
	quantity := rm.CollectQuantity
	// Create the request
	req := world.ActRequest{
		PetitpoisID: p.Id,
		Type:        "CollectRequest",
		Request: world.CollectRequest{
			PetitpoisID: p.Id,
			ItemType:    itemType,
			Quantity:    quantity,
		},
	}

	// Marshal the request
	data, err := json.Marshal(req)
	if err != nil {
		return err
	}

	// Send the request to the server and return the response
	tresp, err := http.Post(p.ServerUrl+"act", "application/json", bytes.NewBuffer(data))
	if err != nil {
		return err
	}

	buf := new(bytes.Buffer)
	buf.ReadFrom(tresp.Body)
	var resp world.CollectResponse
	json.Unmarshal(buf.Bytes(), &resp)

	switch resp.ItemType {
	case "Food":
		p.Inventory.AddOrRemoveFood(resp.Quantity)
	case "Materials":
		p.Inventory.AddOrRemoveMaterials(resp.Quantity)
	}
	p.Inventory.AddOrRemoveXp(int(float64(resp.Quantity) * p.Stats.WisdomCoef))
	return nil
}

func (p *Petitpois) Attack(rm AttackRoadMap) error {
	req := world.ActRequest{
		PetitpoisID: p.Id,
		Type:        "AttackRequest",
		Request: world.AttackRequest{
			PetitpoisID: p.Id,
			Attack:      p.Stats.AttackValue,
			Defense:     p.Stats.DefenseValue,
			Pv:          p.Pv,
		},
	}
	data, err := json.Marshal(req)
	if err != nil {
		return err
	}
	tresp, err := http.Post(p.ServerUrl+"act", "application/json", bytes.NewBuffer(data))
	if err != nil {
		return err
	}
	buf := new(bytes.Buffer)
	buf.ReadFrom(tresp.Body)
	var resp world.AttackResponse
	json.Unmarshal(buf.Bytes(), &resp)

	p.GetPv(resp.DamageReceived)
	p.Inventory.AddOrRemoveXp(int(p.Stats.WisdomCoef * 10))
	return nil
}

func (p *Petitpois) Raid(rm RaidRoadMap) error {
	req := world.ActRequest{
		PetitpoisID: p.Id,
		Type:        "RaidPPRequest",
		Request: world.RaidPPRequest{
			PetitpoisID: p.Id,
			Attack:      p.Stats.AttackValue,
		},
	}
	data, err := json.Marshal(req)
	if err != nil {
		return err
	}
	tresp, err := http.Post(p.ServerUrl+"act", "application/json", bytes.NewBuffer(data))
	if err != nil {
		return err
	}
	buf := new(bytes.Buffer)
	buf.ReadFrom(tresp.Body)
	var resp world.RaidPPResponse
	json.Unmarshal(buf.Bytes(), &resp)

	p.Inventory.AddOrRemoveXp(int(p.Stats.WisdomCoef * 15))
	return nil
}

func (p *Petitpois) Sleep() error {
	p.GiveBeliefs()
	p.GiveInventory()
	p.Idle(IdleRoadMap{})
	p.See(p.CurrentPosition)
	p.TakeOrder()
	return nil
}
