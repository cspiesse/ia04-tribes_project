package petitpois

import (
	"fmt"
	world "tribes"
	"tribes/randutil"
)

// Collector Role
type CollectorRole struct {
}

func NewCollectorRole() *CollectorRole {
	return &CollectorRole{}
}

func (cr *CollectorRole) Name() string {
	return "Collector"
}

func (cr *CollectorRole) Think(p *Petitpois) RoadMap { // TODO: fuite si on voit des ennemis

	// ------- When it's night
	if p.TimeLeft == 0 && p.CurrentPosition == p.CaravanPosition {
		p.WaitingTime = 0
		fmt.Println(p.Id, " : I'm sleepy (No time left)")
		return SleepRoadMap{}
	}
	if p.TimeLeft == -1 {
		p.WaitingTime = 0
		return DieRoadMap{}
	}
	// ------- Calculating the move stats
	var MoveTimeCost int
	var distanceCapacity int
	if p.Stats.MoveSpeed >= 1 {
		MoveTimeCost = int(p.Stats.MoveSpeed) - 1
		distanceCapacity = 1
	} else {
		MoveTimeCost = 0
		distanceCapacity = int(1 / p.Stats.MoveSpeed)
	}
	// ------- When the agent has to fight
	if len(world.Last(p.Belief[p.CurrentPosition]).Elements.Enemies) > 0 {
		if len(p.CurrentRoadMap) > 0 && world.TypeOf(p.CurrentRoadMap[0]) == "MoveRoadMap" {
			p.WaitingTime = MoveTimeCost
		} else {
			p.WaitingTime = 0
		}
		return AttackRoadMap{}
	}
	// ------- When an action has been planned
	if len(p.CurrentRoadMap) > 0 {
		// ------- When the agent isn't allowed to act
		if p.WaitingTime > 0 {
			p.WaitingTime--
			return IdleRoadMap{}
		}
		// ------- When the agent can do what he planned to do
		if p.WaitingTime == 0 {
			var rm RoadMap
			rm, p.CurrentRoadMap = world.Pop(p.CurrentRoadMap)
			return rm
		}
	} else {
		PvTreshold := world.PtgRemainingHPBeforeRetreat
		CollectCapacity := p.Stats.CollectSpeed
		itemWanted, MaxQuantity := cr.DecideNextTurnCollect(&p.Inventory, p.Goal.Collect)
		itemQuantity := MaxQuantity - CollectCapacity

		// ------- Changing the Goal Position to the caravan
		// When the goal is to go to the caravan because there is no time left
		posToCaravan := float64(world.TaxiCabDistance(p.CurrentPosition, p.CaravanPosition, world.Height, world.Width))
		if (float64(p.TimeLeft)-(posToCaravan*(p.Stats.MoveSpeed+2)) <= 2+p.Stats.MoveSpeed+(world.SafeReturnMargin*float64(world.DayDuration))) && posToCaravan != 0 {
			p.Goal.Position = p.CaravanPosition
		}
		// When the goal is to go to the caravan because the inventory is full and has the good ressources proportions
		if itemWanted == "None" {
			p.Goal.Position = p.CaravanPosition
		}
		// When the goal is to go to the caravan because the agent is low on health
		if float64(p.Pv)/float64(p.Stats.MaxHP) < PvTreshold {
			p.Goal.Position = p.CaravanPosition
		}

		mergedBeliefs := mergeBeliefs(p.CaravanBelief, p.Belief)
		// ------- Move or Collect decision
		// When the goal is to go to the caravan -> Move
		if p.Goal.Position == p.CaravanPosition {
			// When the agent is at the goal position
			if p.CurrentPosition == p.Goal.Position {
				p.WaitingTime = 0
				fmt.Println(p.Id, " : I'm sleepy (returned)")
				return IdleRoadMap{}
			}
			// When the agent isn't at the goal position
			rm := MoveRoadMap{
				Position: cr.DecideNextTurnPosition(mergedBeliefs, p.CurrentPosition, p.Goal.Position, distanceCapacity, itemWanted),
			}
			p.CurrentRoadMap = append(p.CurrentRoadMap, rm)
			p.WaitingTime = MoveTimeCost
			return IdleRoadMap{}
		}
		// When there is the item wanted at the current position -> Collect
		if world.Last(mergedBeliefs[p.CurrentPosition]).Elements.Items[itemWanted] > 0 {
			return CollectRoadMap{
				CollectType:     itemWanted,
				CollectQuantity: itemQuantity,
			}
		} else {
			// When there is no sufficient quantity of the item wanted at the current position -> Move
			// When the agent is at the goal position
			if p.CurrentPosition == p.Goal.Position {
				// When the current position doesn't have the wanted item the agent goes to a position nearby that has it
				if world.Last(mergedBeliefs[p.CurrentPosition]).Elements.Items[itemWanted] == 0 {
					bestPosition := p.CurrentPosition
					for _, position := range p.CurrentPosition.RangeNeighborhood(world.Height, world.Width, distanceCapacity) {
						if world.Last(mergedBeliefs[position]).Elements.Items[itemWanted] > world.Last(mergedBeliefs[bestPosition]).Elements.Items[itemWanted] {
							bestPosition = position
						}
					}
					if bestPosition != p.CurrentPosition {
						p.Goal.Position = bestPosition
					} else {
						// If there is no position nearby with the wanted item the agent goes to a random position
						newGoal := randutil.ChoicePosition(p.CurrentPosition.RangeNeighborhood(world.Height, world.Width, distanceCapacity))
						p.Goal.Position = newGoal
					}
				} else {
					return CollectRoadMap{
						CollectType:     itemWanted,
						CollectQuantity: itemQuantity,
					}
				}
			} else {
				rm := MoveRoadMap{
					Position: cr.DecideNextTurnPosition(mergedBeliefs, p.CurrentPosition, p.Goal.Position, distanceCapacity, itemWanted),
				}
				p.CurrentRoadMap = append(p.CurrentRoadMap, rm)
				p.WaitingTime = MoveTimeCost
				return IdleRoadMap{}
			}
		}
	}
	return IdleRoadMap{}
}

// Gives the position where the petitpois should go to next turn
func (cr *CollectorRole) DecideNextTurnPosition(belief world.MapBelief, currentPosition world.Position, goalPosition world.Position, distanceCapacity int, itemWanted world.ItemType) world.Position {
	possiblePositions := PositionsInDirection(currentPosition, goalPosition, distanceCapacity)
	fmt.Println("Possible positions : ", possiblePositions)
	bestPosition := possiblePositions[0]
	fmt.Println("Best position : ", bestPosition)
	for _, position := range possiblePositions {
		if !world.Last(belief[position]).Danger && len(world.Last(belief[position]).Elements.Enemies) < len(world.Last(belief[bestPosition]).Elements.Enemies) {
			if world.Last(belief[position]).Elements.Items[itemWanted] > world.Last(belief[bestPosition]).Elements.Items[itemWanted] {
				bestPosition = position
			}
			if world.Last(belief[position]).Elements.Items[itemWanted] == world.Last(belief[bestPosition]).Elements.Items[itemWanted] && abs(position.X-position.Y) < abs(bestPosition.X-bestPosition.Y) {
				bestPosition = position
			}
		}
	}
	if world.Last(belief[bestPosition]).Danger || len(world.Last(belief[bestPosition]).Elements.Enemies) > 0 {
		for _, position := range currentPosition.RangeNeighborhood(world.Height, world.Width, distanceCapacity) {
			if world.Last(belief[bestPosition]).Danger && !world.Last(belief[position]).Danger {
				bestPosition = position
			} else {
				if world.TaxiCabDistance(position, goalPosition, world.Height, world.Width) < world.TaxiCabDistance(bestPosition, goalPosition, world.Height, world.Width) {
					bestPosition = position
				}
			}
		}
	}
	return bestPosition
}

// Returns a list of positions in the direction of the goalPosition
func PositionsInDirection(currentPosition world.Position, goalPosition world.Position, distanceCapacity int) []world.Position {
	if world.TaxiCabDistance(currentPosition, goalPosition, world.Height, world.Width) <= distanceCapacity {
		return []world.Position{goalPosition}
	}
	fmt.Println("Goal position : ", goalPosition)
	positionList := []world.Position{}
	for _, pos := range currentPosition.RangeNeighborhood(world.Height, world.Width, distanceCapacity) {
		if len(positionList) == 0 {
			positionList = append(positionList, pos)
		} else {
			if world.TaxiCabDistance(pos, goalPosition, world.Height, world.Width) == world.TaxiCabDistance(positionList[0], goalPosition, world.Height, world.Width) {
				positionList = append(positionList, pos)
			} else {
				if world.TaxiCabDistance(pos, goalPosition, world.Height, world.Width) < world.TaxiCabDistance(positionList[0], goalPosition, world.Height, world.Width) {
					positionList = []world.Position{pos}
				}
			}
		}
	}
	return positionList
}

// Returns a list of all the permutations of nbitems in baskets
func Permutations(baskets int, nbitems int) [][]int {
	if baskets == 1 {
		return [][]int{{nbitems}}
	}
	var permutations [][]int
	for i := 0; i <= nbitems; i++ {
		for _, permutation := range Permutations(baskets-1, nbitems-i) {
			permutations = append(permutations, append([]int{i}, permutation...))
		}
	}
	return permutations
}

// Returns the absolute value of x
func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

// Returns the next item to collect and the quantity to collect
func (cr *CollectorRole) DecideNextTurnCollect(inventory *world.Inventory, goalCollectProportions map[world.ItemType]float64) (world.ItemType, int) {
	if inventory.IsFull() {
		return "None", 0
	}
	currentProportions := inventory.CurrentProportions()
	if currentProportions["Food"] < goalCollectProportions["Food"] {
		return "Food", inventory.SizeLeft()
	} else if currentProportions["Materials"] < goalCollectProportions["Materials"] {
		return "Materials", inventory.SizeLeft()
	}
	return "Food", inventory.SizeLeft()
}
