package petitpois

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strings"
	world "tribes"
	"tribes/agents"
)

// Type of roles and goals to communicate to the petitpois
type Mission struct {
	Role            Role
	Goal            Goal
	Stats           PetitpoisStat
	InventorySize   int
	CaravanPosition world.Position
}

type MissionRegistration struct {
	ID   world.PetitpoisID
	Role string
	Goal world.Position
}

type Role interface {
	Name() string
	Think(p *Petitpois) RoadMap
}

type PetitpoisStat struct {
	AttackValue  int     // Attack per Turn
	DefenseValue int     // Defense per Turn
	WisdomCoef   float64 // XP earning multiplier
	MaxHP        int     // HP
	SeeRange     int     // Tiles
	MoveSpeed    float64 // Turn per Tile
	CollectSpeed int     // Resource per Turn
	LuckCoef     float64 // Luck events multiplier
}

type Petitpois struct {
	// Static Attributes defining the agent
	Id      world.PetitpoisID
	Caravan world.CaravanID
	Stats   PetitpoisStat

	// Dynamic Attributes defining the agent
	Pv       int
	Isliving bool
	Goal     Goal
	Role     Role

	// Attributes defining the agent's Belief
	CaravanBelief   world.MapBelief
	Belief          world.MapBelief
	CaravanPosition world.Position
	TimeLeft        int

	// Attributes defining the agent's thoughts
	WaitingTime    int
	CurrentRoadMap []RoadMap
	BadEvent       bool

	// Attributes defining the agent's possessions
	Inventory world.Inventory

	// Attributes defining the agent's current state on the map
	CurrentPosition world.Position

	// Channels to communicate with the caravan
	GiveBeliefsChannel             chan ([]world.TileBelief)
	GiveResourcesChannel           chan (world.Inventory)
	TakeOrderChannel               chan (Mission)
	GetNewStatsChannel             chan (agents.TribeStats)
	GiveMissionRegistrationChannel chan (MissionRegistration)

	ServerUrl string
}

// The Petitpois constructor
func NewPetitpois(id world.PetitpoisID, caravan world.CaravanID, position world.Position, giveBeliefsChannel chan []world.TileBelief, giveResourcesChannel chan world.Inventory, takeOrderChannel chan Mission, giveRegistrationChannel chan MissionRegistration, url string) *Petitpois {
	return &Petitpois{
		Id:                             id,
		Isliving:                       false,
		Caravan:                        caravan,
		CaravanBelief:                  make(world.MapBelief),
		Belief:                         make(world.MapBelief),
		Inventory:                      *world.NewInventory(id, 0),
		CurrentPosition:                position,
		GiveBeliefsChannel:             giveBeliefsChannel,
		GiveResourcesChannel:           giveResourcesChannel,
		TakeOrderChannel:               takeOrderChannel,
		GiveMissionRegistrationChannel: giveRegistrationChannel,
		ServerUrl:                      url,
	}
}

func (p *Petitpois) GiveBeliefs() {
	var beliefs []world.TileBelief
	for _, value := range p.Belief {
		beliefs = append(beliefs, value...)
	}
	p.GiveBeliefsChannel <- beliefs
}

func (p *Petitpois) GiveInventory() {
	p.GiveResourcesChannel <- p.TakeInventory()
}

func (p *Petitpois) TakeOrder() {
	order := <-p.TakeOrderChannel
	fmt.Println("On m'a donné un role :", order.Role.Name())
	p.TimeLeft = int(world.DayDuration) // TODO : transmettre l'info par la caravane qui la reçoit du serveur ?
	p.Role = order.Role
	p.Goal = order.Goal
	p.Stats = order.Stats
	p.Pv = order.Stats.MaxHP
	p.CaravanPosition = order.CaravanPosition
	p.Inventory.Size = order.InventorySize
	p.GiveMissionRegistrationChannel <- MissionRegistration{
		ID:   p.Id,
		Role: p.Role.Name(),
		Goal: p.Goal.Position,
	}
}

func (p *Petitpois) GetPv(nb int) {
	p.Pv += nb
	if p.Pv > p.Stats.MaxHP {
		p.Pv = p.Stats.MaxHP
	}
	if p.Pv <= 0 {
		p.Die()
	}
}

func (p *Petitpois) Die() error {
	req := world.DieRequest{
		PetitpoisID: p.Id,
		// NotePad:     p.Belief, TODO : peut être plus tard !!!
		Inventory: p.Inventory,
	}

	// Marshal the request
	data, err := json.Marshal(req)
	if err != nil {
		return err
	}

	// Send the request to the server and return the response
	_, err = http.Post(p.ServerUrl+"die", "application/json", bytes.NewBuffer(data))
	if err != nil {
		return err
	}

	p.Isliving = false
	p = nil
	return nil
}

func (p *Petitpois) TakeInventory() world.Inventory {
	i := p.Inventory
	p.Inventory = *world.NewInventory(i.PetitpoisID, i.Size)
	return i
}

// Asks the server to see the world around the petitpois
func (p *Petitpois) See(position world.Position) error {
	// Get the perception stat
	perceptionStat := p.Stats.SeeRange

	// Create the request
	req := world.SeeRequest{
		PetitpoisID: p.Id,
		Range:       perceptionStat,
		Position:    position,
	}

	// Marshal the request
	data, err := json.Marshal(req)
	if err != nil {
		return err
	}

	// Send the request to the server and return the response
	tresp, err := http.Post(p.ServerUrl+"see", "application/json", bytes.NewBuffer(data))

	if err != nil {
		return err
	}

	// Get the response
	buf := new(bytes.Buffer)
	buf.ReadFrom(tresp.Body)
	var resp world.SeeResponse
	err = json.Unmarshal(buf.Bytes(), &resp)

	if err != nil {
		return err
	}

	p.TimeLeft = resp.RemainingTurns

	for _, newInfo := range resp.NewBeliefs { // TODO : finaliser le cast
		tmpEnemies := []world.PetitpoisID{}
		for _, ptpid := range newInfo.Elements.Enemies {
			if PetitpoisID2CaravanID(ptpid) != p.Caravan {
				tmpEnemies = append(tmpEnemies, ptpid)
			}
		}
		newInfo.Elements.Enemies = tmpEnemies
		newTileBelief := world.TileBelief{
			Day:      newInfo.Day,
			Turn:     newInfo.Turn,
			Position: newInfo.Position,
			Biome:    newInfo.BiomeName,
			Elements: newInfo.Elements,
		}
		if newTileBelief.Position == p.CurrentPosition && p.BadEvent {
			newTileBelief.Danger = true
		}
		p.Belief[newInfo.Position] = append(p.Belief[newInfo.Position], newTileBelief)
	}

	return nil
}

func PetitpoisID2CaravanID(id world.PetitpoisID) world.CaravanID {
	return world.CaravanID(strings.Split(string(id), "_")[1])
}

func (p *Petitpois) Think() RoadMap {
	return p.Role.Think(p)
}

func (p *Petitpois) Act(rm interface{}) error {
	fmt.Println(p.Id, " : ", p.Role.Name())
	fmt.Println("I'm acting : ", world.TypeOf(rm))
	switch t := world.TypeOf(rm); t {
	case "petitpois.MoveRoadMap":
		return p.Move(rm.(MoveRoadMap))
	case "petitpois.CollectRoadMap":
		return p.Collect(rm.(CollectRoadMap))
	case "petitpois.AttackRoadMap":
		return p.Attack(rm.(AttackRoadMap))
	case "petitpois.IdleRoadMap":
		return p.Idle(rm.(IdleRoadMap))
	case "petitpois.SleepRoadMap":
		return p.Sleep()
	case "petitpois.RaidRoadMap":
		return p.Raid(rm.(RaidRoadMap))
	case "petitpois.DieRoadMap":
		return p.Die()
	}
	return errors.New("unknown RoadMap type")
}

// func (p *Petitpois) Live() error {
// 	for {
// 		if !p.IsNight() {
// 			roadMap := p.Think()
// 			err := p.Act(roadMap)
// 			if err != nil {
// 				return err
// 			}
// 			p.See(p.CurrentPosition)
// 		} else {
// 			if p.CurrentPosition != p.CaravanPosition || (p.CurrentPosition == p.CaravanPosition && world.Last(p.Belief[p.CaravanPosition]).Elements.Caravan == "") { // TODO mettre le bon str qui indique pas de caravane
// 				p.Kill()
// 			} else {
// 				p.Sleep()
// 			}
// 		}
// 	}
// }

func (p *Petitpois) Live() error {
	p.Isliving = true
	fmt.Println("It's alive ", p.Id)
	p.TakeOrder()
	p.See(p.CurrentPosition)
	for p.Isliving {
		fmt.Println(p.Id, " : Je suis en", p.CurrentPosition)
		roadMap := p.Think()
		err := p.Act(roadMap)
		if err != nil {
			return err
		}
		if p.Isliving {
			p.See(p.CurrentPosition)
		}
	}
	return nil
}
