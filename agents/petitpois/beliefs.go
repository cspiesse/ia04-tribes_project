package petitpois

import (
	world "tribes"
)

// Gives the petitpois the updated information about the world from the caravan.
func (p *Petitpois) UpdateCaravanBelief(Belief world.MapBelief) {
	p.CaravanBelief = Belief
}

// Gives the petitpois some new information about the world.
func (p *Petitpois) UpdateSelfBelief(Belief []world.TileBelief) {
	for _, belief := range Belief {
		p.AddTileBelief(belief)
	}
}

// Adds a new TileBelief to the Petitpois' Belief.
func (p *Petitpois) AddTileBelief(atom world.TileBelief) {
	// If the last belief is the same as the new one, we don't add it.
	if world.CompareTileBeliefs(world.Last(p.Belief[atom.Position]), atom) {
		// Adds the danger if the last belief was a danger.
		if world.Last(p.Belief[atom.Position]).Danger {
			atom.Danger = true
		}
		p.Belief[atom.Position] = append(p.Belief[atom.Position], atom)
	}
}

// Merges two BeliefsMaps into one. Need to give the oldest beliefs first.
func mergeBeliefs(BeliefsMaps ...world.MapBelief) world.MapBelief {
	merged := make(world.MapBelief)
	for _, m := range BeliefsMaps {
		for pos, beliefList := range m {
			merged[pos] = append(merged[pos], beliefList...)
		}
	}
	return merged
}
