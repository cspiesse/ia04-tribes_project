package petitpois

import (
	"fmt"
	world "tribes"
	"tribes/randutil"
)

// Attacker Role
type AttackerRole struct {
}

func NewAttackerRole() *AttackerRole {
	return &AttackerRole{}
}

func (ar *AttackerRole) Name() string {
	return "Attacker"
}

func (ar *AttackerRole) Think(p *Petitpois) RoadMap {
	// ------- When it's night
	if p.TimeLeft == 0 && p.CurrentPosition == p.CaravanPosition {
		p.WaitingTime = 0
		fmt.Println(p.Id, " : I'm sleepy (No time left)")
		return SleepRoadMap{}
	}
	if p.TimeLeft == -1 {
		p.WaitingTime = 0
		return DieRoadMap{}
	}
	// ------- Calculating the move stats
	var MoveTimeCost int
	var distanceCapacity int
	if p.Stats.MoveSpeed >= 1 {
		MoveTimeCost = int(p.Stats.MoveSpeed) - 1
		distanceCapacity = 1
	} else {
		MoveTimeCost = 0
		distanceCapacity = int(1 / p.Stats.MoveSpeed)
	}
	// ------- When the agent has to fight
	if len(world.Last(p.Belief[p.CurrentPosition]).Elements.Enemies) > 0 {
		if len(p.CurrentRoadMap) > 0 && world.TypeOf(p.CurrentRoadMap[0]) == "MoveRoadMap" {
			p.WaitingTime = MoveTimeCost
		} else {
			p.WaitingTime = 0
		}
		return AttackRoadMap{}
	}
	// ------- When an action has been planned
	if len(p.CurrentRoadMap) > 0 {
		// ------- When the agent isn't allowed to act
		if p.WaitingTime > 0 {
			p.WaitingTime--
			return IdleRoadMap{}
		}
		// ------- When the agent can do what he planned to do
		if p.WaitingTime == 0 {
			var rm RoadMap
			rm, p.CurrentRoadMap = world.Pop(p.CurrentRoadMap)
			return rm
		}
	} else {
		PvTreshold := world.PtgRemainingHPBeforeRetreat
		// ------- Changing the Goal Position to the caravan
		// When the goal is to go to the caravan because there is no time left
		posToCaravan := float64(world.TaxiCabDistance(p.CurrentPosition, p.CaravanPosition, world.Height, world.Width))
		if (float64(p.TimeLeft)-(posToCaravan*(p.Stats.MoveSpeed+2)) <= 2+p.Stats.MoveSpeed+(world.SafeReturnMargin*float64(world.DayDuration))) && posToCaravan != 0 {
			p.Goal.Position = p.CaravanPosition
		}
		// When the goal is to go to the caravan because the agent is low on health
		if float64(p.Pv)/float64(p.Stats.MaxHP) < PvTreshold {
			p.Goal.Position = p.CaravanPosition
		}
		// When the goal is another position
		if p.CurrentPosition == p.Goal.Position && p.Goal.Position != p.CaravanPosition {
			newGoal := randutil.ChoicePosition(p.CurrentPosition.RangeNeighborhood(world.Height, world.Width, distanceCapacity))
			p.Goal.Position = newGoal
		}
		mergedBeliefs := mergeBeliefs(p.CaravanBelief, p.Belief)
		// When the goal is to go to the caravan
		if p.Goal.Position == p.CaravanPosition {
			// When the agent is at the goal position
			if p.CurrentPosition == p.Goal.Position {
				p.WaitingTime = 0
				fmt.Println(p.Id, " : I'm sleepy (returned)")
				return IdleRoadMap{}
			}
			// When the agent isn't at the goal position
			rm := MoveRoadMap{
				Position: ar.DecideNextTurnPositionToRetreat(mergedBeliefs, p.CurrentPosition, p.Goal.Position, distanceCapacity),
			}
			p.CurrentRoadMap = append(p.CurrentRoadMap, rm)
			p.WaitingTime = MoveTimeCost
			return IdleRoadMap{}
		} else {
			// When the agent is at at a caravan position -> Raid
			if world.Last(mergedBeliefs[p.CurrentPosition]).Elements.Caravan != "" {
				p.WaitingTime = 0
				return RaidRoadMap{}
			}
			rm := MoveRoadMap{
				Position: ar.DecideNextTurnPositionToAttack(mergedBeliefs, p.CurrentPosition, p.Goal.Position, distanceCapacity),
			}
			p.CurrentRoadMap = append(p.CurrentRoadMap, rm)
			p.WaitingTime = MoveTimeCost
			return IdleRoadMap{}
		}
	}
	return IdleRoadMap{}
}

// Gives the position where the petitpois should go to next turn when he wants to attack
func (ar *AttackerRole) DecideNextTurnPositionToAttack(belief world.MapBelief, currentPosition world.Position, goalPosition world.Position, distanceCapacity int) world.Position {
	positionsInRange := currentPosition.RangeNeighborhood(world.Height, world.Width, distanceCapacity)
	bestPosition := positionsInRange[0]
	for _, position := range positionsInRange {
		if world.Last(belief[position]).Elements.Caravan != "" {
			return position
		}
		if (len(world.Last(belief[position]).Elements.Enemies) > 0) && (len(world.Last(belief[position]).Elements.Enemies) < len(world.Last(belief[bestPosition]).Elements.Enemies)) {
			bestPosition = position
		}
	}
	if world.Last(belief[bestPosition]).Elements.Caravan == "" && len(world.Last(belief[bestPosition]).Elements.Enemies) == 0 {
		possiblePositions := PositionsInDirection(currentPosition, goalPosition, distanceCapacity)
		return randutil.ChoicePosition(possiblePositions)
	}
	return bestPosition
}

// Gives the position where the petitpois should go to next turn when he wants to retreat
func (ar *AttackerRole) DecideNextTurnPositionToRetreat(belief world.MapBelief, currentPosition world.Position, goalPosition world.Position, distanceCapacity int) world.Position {
	possiblePositions := PositionsInDirection(currentPosition, goalPosition, distanceCapacity)
	bestPosition := possiblePositions[0]
	for _, position := range possiblePositions {
		if !world.Last(belief[position]).Danger && len(world.Last(belief[position]).Elements.Enemies) < len(world.Last(belief[bestPosition]).Elements.Enemies) {
			bestPosition = position
		}
	}
	if world.Last(belief[bestPosition]).Danger || len(world.Last(belief[bestPosition]).Elements.Enemies) > 0 {
		for _, position := range currentPosition.RangeNeighborhood(world.Height, world.Width, distanceCapacity) {
			if world.Last(belief[bestPosition]).Danger && !world.Last(belief[position]).Danger {
				bestPosition = position
			} else {
				if world.TaxiCabDistance(position, goalPosition, world.Height, world.Width) < world.TaxiCabDistance(bestPosition, goalPosition, world.Height, world.Width) {
					bestPosition = position
				}
			}
		}
	}
	return bestPosition
}
