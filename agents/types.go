package agents

// Stats
type TribeStats map[string]int

var TribeStatsNames = []string{"Attack", "Defense", "Wisdom", "Stamina", "Perception", "Speed", "Gathering", "Luck"}

type BiomeBelief struct {
	Name                      string
	RegenerationRate          float64
	FoodLimit, MaterialsLimit int
	DangerRate                float64
	NbTimesExplored           int
}
