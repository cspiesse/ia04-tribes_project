package server

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"sync"
	"time"

	"tribes"
	"tribes/main_server/mapagent"

	"github.com/gorilla/websocket"
)

// Informations à envoyer à l'UI
type SimplifiedPetitpoisInfo struct {
	Role               string           `json:"role"`
	Position           tribes.Position  `json:"position"`
	BelongingCaravanID tribes.CaravanID `json:"belongingCaravanID"`
}

type SimplifiedCaravanInfo struct {
	Position     tribes.Position `json:"position"`
	CaravanStats map[string]int  `json:"caravanStats"`
}

type SimplifiedGrid struct {
	Matrix [][]tribes.Tile `json:"mapInfo"`
}

type RestServerAgent struct {
	id   string
	addr string

	Grid        mapagent.Map
	CurrentTurn tribes.Turn
	CurrentDay  tribes.Day
	sync.Mutex

	attackResponses  map[tribes.PetitpoisID]tribes.AttackResponse
	raidPPResponses  map[tribes.PetitpoisID]tribes.RaidPPResponse
	moveResponses    map[tribes.PetitpoisID]tribes.MoveResponse
	collectResponses map[tribes.PetitpoisID]tribes.CollectResponse
	idleResponses    map[tribes.PetitpoisID]tribes.IdleResponse

	seeResponses map[tribes.PetitpoisID]tribes.SeeResponse

	firstLaunch          bool
	okChan               chan (bool)
	actRequestChan       chan (tribes.ActRequest)
	actResponseReadyChan chan (bool)
	seeRequestChan       chan (tribes.SeeRequest)
	seeResponseReadyChan chan (bool)
	stateActSee          string

	// Compteurs pour l'organisation des phases
	nbTotalPetitpois int

	// Informations pour l'UI
	CaravansInfo  map[tribes.CaravanID]SimplifiedCaravanInfo
	PetitpoisInfo map[tribes.PetitpoisID]SimplifiedPetitpoisInfo // trucs à update et set avec la nuit ou intialisation

	CaravansAddr map[tribes.CaravanID]string

	// Test WSTCP
	mapUpgrader websocket.Upgrader
	NeedUpdate  bool
}

type UIUpdateData struct {
	Height         int
	Width          int
	SimplifiedGrid mapagent.Map                                   `json:"SimplifiedGrid"`
	CaravansInfo   map[tribes.CaravanID]SimplifiedCaravanInfo     `json:"CaravansInfo"`
	PetitpoisInfo  map[tribes.PetitpoisID]SimplifiedPetitpoisInfo `json:"PetitpoisInfo"`
	CurrentTurn    tribes.Turn
	CurrentDay     tribes.Day
}

func NewRestWebService(_id string, _addr string, _caravansAddr map[tribes.CaravanID]string, _grid mapagent.Map, _caravansInfo map[tribes.CaravanID]SimplifiedCaravanInfo) *RestServerAgent {

	var upgrader = websocket.Upgrader{
		CheckOrigin: func(r *http.Request) bool { return true },
	}

	return &RestServerAgent{
		id:           _id,
		addr:         _addr,
		CaravansAddr: _caravansAddr,

		Grid: _grid,

		CurrentTurn: 0,
		CurrentDay:  -1,

		nbTotalPetitpois: 0,
		CaravansInfo:     _caravansInfo,

		attackResponses:  make(map[tribes.PetitpoisID]tribes.AttackResponse),
		raidPPResponses:  make(map[tribes.PetitpoisID]tribes.RaidPPResponse),
		moveResponses:    make(map[tribes.PetitpoisID]tribes.MoveResponse),
		collectResponses: make(map[tribes.PetitpoisID]tribes.CollectResponse),
		idleResponses:    make(map[tribes.PetitpoisID]tribes.IdleResponse),
		seeResponses:     make(map[tribes.PetitpoisID]tribes.SeeResponse),

		mapUpgrader: upgrader,
		NeedUpdate:  false,
	}
}

func (rws *RestServerAgent) LaunchSimulation() {

	// As if beginning of the night
	rws.Lock()
	fmt.Println("Launch : number of caravan : ", len(rws.CaravansInfo))
	rws.PetitpoisInfo = make(map[tribes.PetitpoisID]SimplifiedPetitpoisInfo)
	rws.CaravansInfo = make(map[tribes.CaravanID]SimplifiedCaravanInfo)
	_caravansAddr := rws.CaravansAddr
	_currentDay := rws.CurrentDay

	rws.okChan = make(chan bool, tribes.MaxPetitpois)
	rws.actRequestChan = make(chan tribes.ActRequest, tribes.MaxPetitpois)
	rws.actResponseReadyChan = make(chan bool, tribes.MaxPetitpois)
	rws.seeRequestChan = make(chan tribes.SeeRequest, tribes.MaxPetitpois)
	rws.seeResponseReadyChan = make(chan bool, tribes.MaxPetitpois)
	rws.stateActSee = "See"
	rws.Unlock()

	go rws.ProcessSee()
	go rws.ProcessAct()

	var wgCaravan sync.WaitGroup
	for id := range _caravansAddr {
		wgCaravan.Add(1)
		go rws.doNightfallRequest(id, _currentDay, &wgCaravan)
	}

	wgCaravan.Wait()

	rws.firstLaunch = true
	rws.okChan <- true

	// End of the night
	rws.Lock()
	rws.CurrentDay = 0
	rws.CurrentTurn = 0
	fmt.Println("end launch : total pp ", rws.nbTotalPetitpois)
	rws.Unlock()
}

/* ------READER AND HANDLER WEBSOCKET------ */
func (rws *RestServerAgent) handleWs(w http.ResponseWriter, r *http.Request) {
	// upgrade this connection to a WebSocket connection
	conn, err := rws.mapUpgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
	}
	defer conn.Close()

	// listen indefinitely for new messages coming
	// through on our WebSocket connection
	listenWs(rws, conn)
}

func listenWs(rws *RestServerAgent, conn *websocket.Conn) {
	for {
		// read in a message
		messageType, message, err := conn.ReadMessage()
		if err != nil {
			log.Println(err)
			return
		}

		//log.Println(string(message))
		if string(message) == "launchSimulation" {
			fmt.Println(">>>>>>>>>>>>>>>> ", string(message))
			go rws.LaunchSimulation()
		}

		message, _ = json.Marshal(nil)

		if rws.NeedUpdate {
			fmt.Println("##################### SHOULD UPDATE ###################")
			rws.Lock()
			var update = UIUpdateData{
				Height:         rws.Grid.Height(),
				Width:          rws.Grid.Width(),
				SimplifiedGrid: rws.Grid,
				CaravansInfo:   rws.CaravansInfo,
				PetitpoisInfo:  rws.PetitpoisInfo,
				CurrentTurn:    rws.CurrentTurn,
				CurrentDay:     rws.CurrentDay,
			}
			rws.Unlock()
			message, err = json.Marshal(update)
			if err != nil {
				log.Println(err)
			}
			rws.Lock()
			rws.NeedUpdate = false
			rws.Unlock()
		}

		if err := conn.WriteMessage(messageType, message); err != nil {
			log.Println(err)
		}

	}
}

/* ------METHODS FOT PETITPOIS------- */

/* ------Handle functions and process------- */

func (rws *RestServerAgent) handleActReception(w http.ResponseWriter, r *http.Request) {

	time.Sleep(time.Duration(tribes.WaitTime) * time.Millisecond)

	// Recevoir la requete et la vérifier
	// Vérifie que la méthode est POST
	if !rws.checkMethod("POST", w, r) {
		return
	}
	// Décode le body JSON de la requête
	var req tribes.ActRequest
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err := json.Unmarshal(buf.Bytes(), &req)

	if err != nil {
		w.WriteHeader(http.StatusNotImplemented)
		w.Write([]byte(err.Error()))
		return
	}

	// Send request to ProcessAct funtion
	rws.actRequestChan <- req

	// wait until response is ready
	<-rws.actResponseReadyChan

	var serial []byte

	rws.Lock()
	switch req.Type {
	case "AttackRequest":
		serial, _ = json.Marshal(rws.attackResponses[req.PetitpoisID])
	case "RaidPPRequest":
		serial, _ = json.Marshal(rws.raidPPResponses[req.PetitpoisID])
	case "MoveRequest":
		serial, _ = json.Marshal(rws.moveResponses[req.PetitpoisID])
	case "CollectRequest":
		serial, _ = json.Marshal(rws.collectResponses[req.PetitpoisID])
	case "IdleRequest":
		serial, _ = json.Marshal(rws.idleResponses[req.PetitpoisID])
	default:
	}
	rws.Unlock()

	w.WriteHeader(200)
	w.Write(serial)
}

func (rws *RestServerAgent) ProcessAct() {

	for {

		<-rws.okChan

		if rws.firstLaunch {
			rws.okChan <- true
		} else {

			fmt.Println("Process Act")

			if rws.nbTotalPetitpois == 0 {
				fmt.Println()
				fmt.Println("Everyone died !")
				return
			}

			nbResponses := rws.nbTotalPetitpois
			remainingPPT := rws.nbTotalPetitpois
			rws.CurrentTurn++

			actResponses := []tribes.ActRequest{}
			fmt.Println("I'm waiting to act", remainingPPT, "ppt !")
			for remainingPPT > 0 {
				req := <-rws.actRequestChan

				remainingPPT--

				// Check if it is a death request
				if (req == tribes.ActRequest{}) {
					nbResponses--
				} else {
					actResponses = append(actResponses, req)
				}
			}

			rws.Lock()
			rws.attackResponses = make(map[tribes.PetitpoisID]tribes.AttackResponse)
			rws.raidPPResponses = make(map[tribes.PetitpoisID]tribes.RaidPPResponse)
			rws.moveResponses = make(map[tribes.PetitpoisID]tribes.MoveResponse)
			rws.collectResponses = make(map[tribes.PetitpoisID]tribes.CollectResponse)
			rws.idleResponses = make(map[tribes.PetitpoisID]tribes.IdleResponse)
			rws.Unlock()

			var wg sync.WaitGroup

			// check Fight
			tmpFights := make(map[tribes.Position][]tribes.AttackRequest)
			rws.Lock()
			loopLen := len(actResponses)
			rws.Unlock()

			for ind := 0; ind < loopLen; {
				rws.Lock()
				obj := actResponses[ind]
				rws.Unlock()
				if obj.Type == "AttackRequest" {

					// Interprete the true request
					marshalledAttackRequest, _ := json.Marshal(obj.Request)

					var unmarshalledAttackRequest tribes.AttackRequest
					_ = json.Unmarshal(marshalledAttackRequest, &unmarshalledAttackRequest)

					// Agglomerate fighters on their respective position
					rws.Lock()
					petitpoisPos := rws.PetitpoisInfo[unmarshalledAttackRequest.PetitpoisID].Position
					rws.Unlock()
					tmpFights[petitpoisPos] = append(tmpFights[petitpoisPos], unmarshalledAttackRequest)
					// pop ActTmpObject from actResponses
					rws.Lock()
					actResponses[ind] = actResponses[len(actResponses)-1]
					actResponses = actResponses[:len(actResponses)-1]
					loopLen = len(actResponses)
					rws.Unlock()
				} else {
					ind++
				}
			}
			// Launch each fight for each position
			for _, fightersList := range tmpFights {
				wg.Add(1)
				go rws.handleFight(fightersList, 200, &wg)
			}

			// check Raid
			tmpRaid := make(map[tribes.Position][]tribes.RaidPPRequest)
			rws.Lock()
			loopLen = len(actResponses)
			rws.Unlock()
			for ind := 0; ind < loopLen; {
				rws.Lock()
				obj := actResponses[ind]
				rws.Unlock()
				if obj.Type == "RaidPPRequest" {

					// Interprete the true request
					marshalledRaidRequest, _ := json.Marshal(obj.Request)

					var unmarshalledRaidRequest tribes.RaidPPRequest
					_ = json.Unmarshal(marshalledRaidRequest, &unmarshalledRaidRequest)

					raidObj := unmarshalledRaidRequest

					// Agglomerate attackers on their respective caravan position
					rws.Lock()
					petitpoisPos := rws.PetitpoisInfo[raidObj.PetitpoisID].Position
					tmpRaid[petitpoisPos] = append(tmpRaid[petitpoisPos], raidObj)
					// pop ActTmpObject from actResponses
					actResponses[ind] = actResponses[len(actResponses)-1]
					actResponses = actResponses[:len(actResponses)-1]
					loopLen = len(actResponses)
					rws.Unlock()
				} else {
					ind++
				}
			}
			// Launch each raids for each position
			for _, raidersList := range tmpRaid {
				go rws.handleRaid(raidersList, 200, &wg)
			}

			// check Move
			rws.Lock()
			loopLen = len(actResponses)
			rws.Unlock()
			for ind := 0; ind < loopLen; {
				rws.Lock()
				obj := actResponses[ind]
				rws.Unlock()
				if obj.Type == "MoveRequest" {

					// Interprete the true request
					marshalledMoveRequest, _ := json.Marshal(obj.Request)

					var unmarshalledMoveRequest tribes.MoveRequest
					_ = json.Unmarshal(marshalledMoveRequest, &unmarshalledMoveRequest)

					moveObj := unmarshalledMoveRequest

					// GO !!!
					wg.Add(1)
					go rws.handleMove(moveObj, 200, &wg)
					// pop ActTmpObject from actResponses
					rws.Lock()
					actResponses[ind] = actResponses[len(actResponses)-1]
					actResponses = actResponses[:len(actResponses)-1]
					loopLen = len(actResponses)
					rws.Unlock()
				} else {
					ind++
				}
			}

			// check Collect
			tmpCollect := make(map[tribes.Position][]tribes.CollectRequest)
			rws.Lock()
			loopLen = len(actResponses)
			rws.Unlock()
			for ind := 0; ind < loopLen; {
				rws.Lock()
				obj := actResponses[ind]
				rws.Unlock()
				if obj.Type == "CollectRequest" {

					// Interprete the true request
					marshalledCollectRequest, _ := json.Marshal(obj.Request)

					var unmarshalledCollectRequest tribes.CollectRequest
					_ = json.Unmarshal(marshalledCollectRequest, &unmarshalledCollectRequest)

					collectObj := unmarshalledCollectRequest

					// Agglomerate collecters on their respective position
					rws.Lock()
					petitpoisPos := rws.PetitpoisInfo[collectObj.PetitpoisID].Position
					tmpCollect[petitpoisPos] = append(tmpCollect[petitpoisPos], collectObj)
					// pop ActTmpObject from actResponses
					actResponses[ind] = actResponses[len(actResponses)-1]
					actResponses = actResponses[:len(actResponses)-1]
					loopLen = len(actResponses)
					rws.Unlock()
				} else {
					ind++
				}
			}
			// Launch each mutual collect for each position
			for _, collectersList := range tmpCollect {
				wg.Add(1)
				go rws.handleCollect(collectersList, 200, &wg)
			}

			// check Idle

			rws.Lock()
			loopLen = len(actResponses)
			rws.Unlock()
			for ind := 0; ind < loopLen; {
				rws.Lock()
				obj := actResponses[ind]
				rws.Unlock()
				if obj.Type == "IdleRequest" {

					// Interprete the true request
					marshalledIdleRequest, _ := json.Marshal(obj.Request)

					var unmarshalledIdleRequest tribes.IdleRequest
					_ = json.Unmarshal(marshalledIdleRequest, &unmarshalledIdleRequest)

					idleObj := unmarshalledIdleRequest

					// Go !!!
					wg.Add(1)
					go rws.handleIdle(idleObj, 200, &wg)
					// pop ActTmpObject from actResponses
					rws.Lock()
					actResponses[ind] = actResponses[len(actResponses)-1]
					actResponses = actResponses[:len(actResponses)-1]
					loopLen = len(actResponses)
					rws.Unlock()
				} else {
					ind++
				}
			}

			wg.Wait()

			rws.stateActSee = "See"

			// Responses are ready
			for i := 0; i < nbResponses; i++ {
				rws.actResponseReadyChan <- true
			}

			rws.okChan <- true
		}
	}

}

func (rws *RestServerAgent) handleSeeReception(w http.ResponseWriter, r *http.Request) {

	time.Sleep(time.Duration(tribes.WaitTime) * time.Millisecond)

	// Recevoir la requete et la vérifier
	// Vérifie que la méthode est POST
	if !rws.checkMethod("POST", w, r) {
		fmt.Println("checkMethod fait de la bouse")
		return
	}
	// Décode le body JSON de la requête
	var req tribes.SeeRequest
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err := json.Unmarshal(buf.Bytes(), &req)

	if err != nil {
		w.WriteHeader(http.StatusNotImplemented)
		w.Write([]byte(err.Error()))
		return
	}

	// Send request to ProcessSee funtion
	rws.seeRequestChan <- req

	// wait until response is ready
	<-rws.seeResponseReadyChan

	w.WriteHeader(200)
	rws.Lock()
	serial, _ := json.Marshal(rws.seeResponses[req.PetitpoisID])
	rws.Unlock()
	w.Write(serial)
}

func (rws *RestServerAgent) ProcessSee() {

	for {
		<-rws.okChan

		if rws.firstLaunch {
			rws.firstLaunch = false
		}

		fmt.Println("ProcessSee")

		if rws.nbTotalPetitpois == 0 {
			fmt.Println()
			fmt.Println("Everyone died !")
			return
		}
		nbResponses := rws.nbTotalPetitpois
		remainingPPT := rws.nbTotalPetitpois

		// calcul nombre de tours restants à envoyer à chaque handleFunction
		rws.Lock()
		remainingTurns := tribes.DayDuration - rws.CurrentTurn
		rws.Unlock()

		var wg sync.WaitGroup
		rws.seeResponses = make(map[tribes.PetitpoisID]tribes.SeeResponse)
		fmt.Println("I'm waiting to see", remainingPPT, "ppt !")
		for remainingPPT > 0 {
			req := <-rws.seeRequestChan
			remainingPPT--

			// Check if it is a death request
			if (req == tribes.SeeRequest{}) {
				nbResponses--
			} else {
				wg.Add(1)
				go rws.handleIndividualSee(req, int(remainingTurns-1), 200, &wg)
			}
		}

		rws.Lock()
		_currenTurn := rws.CurrentTurn
		rws.Unlock()

		wg.Wait()

		rws.stateActSee = "Act"

		// Responses are ready
		for i := 0; i < nbResponses; i++ {
			rws.seeResponseReadyChan <- true
		}

		rws.Lock()
		fmt.Println("\n\nJour ", rws.CurrentDay, ", Tour ", rws.CurrentTurn+1)
		// Update UI
		// TODO send new info to UI through websocket here
		rws.NeedUpdate = true
		fmt.Println("##################### NEED UPDATE TRUE ###################")
		rws.Unlock()

		// TIME we can wait a certain time here at the end of a turn

		time.Sleep(time.Duration(tribes.WaitTime) * time.Millisecond)

		// check à la fin de cette fonction si c'est la fin de la journée (LastTurn)
		if _currenTurn == tribes.DayDuration {

			rws.stateActSee = "See"

			time.Sleep(time.Duration(tribes.WaitTime) * 3 * time.Millisecond)

			fmt.Println("\n\nNight is beginning here")

			// Ecraser tout ce qu'on savait des PP et Caravanes
			rws.Lock()
			rws.PetitpoisInfo = make(map[tribes.PetitpoisID]SimplifiedPetitpoisInfo)
			rws.CaravansInfo = make(map[tribes.CaravanID]SimplifiedCaravanInfo)
			rws.nbTotalPetitpois = 0
			_caravansAddr := rws.CaravansAddr
			rws.Unlock()
			// LOOP envoyer requete d'activation aux Caravan-servers bloquante jusqu'à la fin de la nuit (avec le jour actuel)
			var wgCaravan sync.WaitGroup
			for id := range _caravansAddr {
				fmt.Println("Caravan ", id, " is warned of Nightfall")
				wgCaravan.Add(1)
				go rws.doNightfallRequest(id, rws.CurrentDay, &wgCaravan)
			}

			// On en profite pour régèn les ressources de la Grid
			rws.Lock()
			rws.Grid.DailyRegeneration()
			rws.Unlock()

			wgCaravan.Wait()

			rws.Lock()
			rws.CurrentTurn = 0
			rws.CurrentDay++
			fmt.Println("nbTotalPetitpois : ", rws.nbTotalPetitpois)
			rws.Unlock()
		}

		if _currenTurn == tribes.DayDuration {
			rws.firstLaunch = true
		}
		rws.okChan <- true
	}
}

/* ------Methods for Petitpois------- */

// Synchronises various Petitpois on the order of a fight. It tells who loses how much health
func (rws *RestServerAgent) handleFight(fightersList []tribes.AttackRequest, statusError int, wg *sync.WaitGroup) {

	defer wg.Done()

	fightersTribeBelonging := make(map[tribes.CaravanID][]tribes.PetitpoisID) // association fighter - belongingCaravan
	tribesAttack := make(map[tribes.CaravanID]int)                            // cumulated attacks of the fighters from each caravan
	ppReceivedDamages := make(map[tribes.PetitpoisID]int)
	ppId2FighterReqIndex := make(map[tribes.PetitpoisID]int)

	stringFighters := ""

	// Loop through each fighter to sort them by tribes and add their attack
	// For each tribe, aggregate the number of damages they will distribute to each other tribes
	for index, fighterReq := range fightersList {

		ppId2FighterReqIndex[fighterReq.PetitpoisID] = index

		addedAttack := fighterReq.Attack
		rws.Lock()
		fighterCaravanID := rws.PetitpoisInfo[fighterReq.PetitpoisID].BelongingCaravanID
		rws.Unlock()
		tribesAttack[fighterCaravanID] += addedAttack
		if (fightersTribeBelonging[fighterCaravanID] == nil) || (len(fightersTribeBelonging[fighterCaravanID]) == 0) {
			fightersTribeBelonging[fighterCaravanID] = make([]tribes.PetitpoisID, 0)
			fightersTribeBelonging[fighterCaravanID] = append(fightersTribeBelonging[fighterCaravanID], fighterReq.PetitpoisID)
		} else {
			fightersTribeBelonging[fighterCaravanID] = append(fightersTribeBelonging[fighterCaravanID], fighterReq.PetitpoisID)
		}

		stringFighters += string(fighterReq.PetitpoisID) + " x "

	}

	fightPos := rws.PetitpoisInfo[fightersList[0].PetitpoisID].Position
	fmt.Println("A fight is happening on position : ", fightPos.Pos2String(), "\nopposing : ", stringFighters)

	// Distribute the damages of the tribes fighter gradually to each other fighters
	numOfOpponents := (len(tribesAttack) - 1)
	for tribeId, tribeAtt := range tribesAttack {
		index := 0

		for otherTribeId, listOfPpId := range fightersTribeBelonging {

			if otherTribeId != tribeId {

				damage2Distribute := tribeAtt / numOfOpponents // this formula implies that very few damages are inflicted if a lots of tribes are fighting at the same location
				if index < tribeAtt%numOfOpponents {           // avoid the loss of damages
					damage2Distribute += 1
				}

				for _, ppId := range listOfPpId {

					if damage2Distribute > 0 {
						Pv := fightersList[ppId2FighterReqIndex[ppId]].Pv
						Def := fightersList[ppId2FighterReqIndex[ppId]].Defense
						number2Kill := Pv + Def

						if damage2Distribute > number2Kill {
							ppReceivedDamages[ppId] = Pv
							damage2Distribute -= number2Kill
						} else {
							ppReceivedDamages[ppId] = damage2Distribute - Def
							damage2Distribute = 0
						}
					}
				}
				index++
			}
		}
	}

	// Loop through each fighter to send the number of received damages
	for _, fighterReq := range fightersList {

		resp := tribes.AttackResponse{
			DamageReceived: ppReceivedDamages[fighterReq.PetitpoisID],
		}
		rws.Lock()
		rws.attackResponses[fighterReq.PetitpoisID] = resp
		rws.Unlock()
	}
}

// Synchronises various allied Petitpois attacking the same Caravan. It deals damage to the Caravan and provide xp to the attackers
func (rws *RestServerAgent) handleRaid(raidersList []tribes.RaidPPRequest, statusError int, wg *sync.WaitGroup) error {

	defer wg.Done()

	// get the position then the id of the Caravan
	rws.Lock()
	raidedPos := rws.PetitpoisInfo[raidersList[0].PetitpoisID].Position
	_caravansInfo := rws.CaravansInfo
	rws.Unlock()
	var caravanId tribes.CaravanID

	for id, caravanInfo := range _caravansInfo {
		if caravanInfo.Position == raidedPos {
			caravanId = id
		}
	}

	fmt.Println("The Caravan ", caravanId, " is raided !")

	// --- Sum the damages of every raiders on the location ---
	raidersDamages := 0

	for _, raiderReq := range raidersList {

		addedAttack := raiderReq.Attack
		raidersDamages += addedAttack
	}

	// --- Send the damages inflicted to the caravan ---
	req := tribes.RaidCaravanRequest{
		DamageReceived: raidersDamages,
	}
	// Sérialisation de la requête
	rws.Lock()
	url := rws.CaravansAddr[caravanId] + "raid" // TODO : check endpoint
	rws.Unlock()
	data, _ := json.Marshal(req)

	// Envoi de la requête
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))

	// TODO checker les infos reçues
	// Traitement de la réponse
	if err != nil {
		log.Printf("Impossible to send damages to the caravan !")
		log.Print(err)
		return err
	}
	if resp.StatusCode != http.StatusOK {
		err = fmt.Errorf("[%d] %s", resp.StatusCode, resp.Status)
		log.Print(err)
		return err
	}

	defer resp.Body.Close() // TODO check at which moment it is better to Close()
	var newData tribes.RaidCaravanResponse
	json.NewDecoder(resp.Body).Decode(&newData)
	if err != nil {
		print(err)
	}

	// Update Caravan informations
	if newData.RemainingHP <= 0 {
		// Drop resources on the floor
		rws.Lock()
		rws.Grid.Matrix[raidedPos.Y][raidedPos.X].Elements.Items["Food"] += newData.RemainingFood // TODO : check with others Items and ItemsAtArrival
		rws.Grid.Matrix[raidedPos.Y][raidedPos.X].Elements.Items["Materials"] += newData.RemainingMaterials

		// Kill the caravan, remove their info from the map
		delete(rws.CaravansAddr, caravanId)
		delete(rws.CaravansInfo, caravanId)
		rws.Unlock()
	}

	// Confirm to every raiders that they managed to attack the caravan
	for _, raiderReq := range raidersList {
		resp := tribes.RaidPPResponse{}

		rws.Lock()
		rws.raidPPResponses[raiderReq.PetitpoisID] = resp
		rws.Unlock()
	}

	return nil
}

// Provides the asked resources to the allied Petitpois on the same location
func (rws *RestServerAgent) handleCollect(collectersList []tribes.CollectRequest, statusError int, wg *sync.WaitGroup) {

	defer wg.Done()

	//if numCollecters > 1 { // allied collecters are sharing the same resources

	foodCollectersIndexes := make(map[tribes.PetitpoisID]int)
	materialsCollectersIndexes := make(map[tribes.PetitpoisID]int)
	foodCollectersAttribution := make(map[tribes.PetitpoisID]int)
	materialsCollectersAttribution := make(map[tribes.PetitpoisID]int)
	neededFood := 0
	neededMaterials := 0
	rws.Lock()
	pos := rws.PetitpoisInfo[collectersList[0].PetitpoisID].Position
	currentTile := rws.Grid.Matrix[pos.Y][pos.X]
	rws.Unlock()

	initialFoodQty := currentTile.Elements.Items["Food"]
	initialMaterialsQty := currentTile.Elements.Items["Materials"]
	currentFoodQty := initialFoodQty
	currentMaterialsQty := initialMaterialsQty

	// Associate collecters by ItemType, sum the needed resources
	for index, collecterReq := range collectersList {

		if collecterReq.ItemType == "Food" {

			foodCollectersIndexes[collecterReq.PetitpoisID] = index
			neededFood += collecterReq.Quantity

		} else if collecterReq.ItemType == "Materials" {

			materialsCollectersIndexes[collecterReq.PetitpoisID] = index
			neededMaterials += collecterReq.Quantity

		}
	}

	// share the FOOD among the collecters
	for neededFood > 0 && currentFoodQty > 0 {
		for collecterId, index := range foodCollectersIndexes {

			collectReq := collectersList[index]
			quantity := collectReq.Quantity

			if foodCollectersAttribution[collecterId] < quantity {
				foodCollectersAttribution[collecterId] += 1
				neededFood--
				currentFoodQty--
			}
		}
	}
	// Send FOOD responses and actualize the current tile
	for collecterId := range foodCollectersIndexes {
		resp := tribes.CollectResponse{
			ItemType: "Food",
			Quantity: foodCollectersAttribution[collecterId],
		}
		rws.Lock()
		rws.collectResponses[collecterId] = resp
		rws.Unlock()
	}
	rws.Lock()
	rws.Grid.Matrix[pos.Y][pos.X].Elements.Items["Food"] += currentFoodQty - initialFoodQty
	rws.Unlock()

	// share the MATERIALS among the collecters
	for neededMaterials > 0 && currentMaterialsQty > 0 {
		for collecterId, index := range materialsCollectersIndexes {

			collectReq := collectersList[index]
			quantity := collectReq.Quantity

			if materialsCollectersAttribution[collecterId] < quantity {
				materialsCollectersAttribution[collecterId] += 1
				neededMaterials--
				currentMaterialsQty--
			}
		}
	}
	// Send MATERIALS responses and actualize the current tile
	for collecterId := range materialsCollectersIndexes {
		resp := tribes.CollectResponse{
			ItemType: "Materials",
			Quantity: materialsCollectersAttribution[collecterId],
		}
		rws.Lock()
		rws.collectResponses[collecterId] = resp
		rws.Unlock()
	}
	rws.Lock()
	rws.Grid.Matrix[pos.Y][pos.X].Elements.Items["Materials"] += currentMaterialsQty - initialMaterialsQty
	rws.Unlock()
}

// Set the new position of a Petitpois on the referenced tile (map updated). It then confirms the move to the Petitpois.
func (rws *RestServerAgent) handleMove(req tribes.MoveRequest, statusError int, wg *sync.WaitGroup) {

	defer wg.Done()

	//MàJ de la map
	ppId := req.PetitpoisID
	newPos := req.To
	ok := false

	rws.Lock()
	oldPos := rws.PetitpoisInfo[ppId].Position
	// remove petitpois from old position
	for ind := 0; ind < len(rws.Grid.Matrix[oldPos.Y][oldPos.X].Elements.Enemies) && !ok; ind++ {
		if rws.Grid.Matrix[oldPos.Y][oldPos.X].Elements.Enemies[ind] == ppId {
			rws.Grid.Matrix[oldPos.Y][oldPos.X].Elements.Enemies[ind] = rws.Grid.Matrix[oldPos.Y][oldPos.X].Elements.Enemies[len(rws.Grid.Matrix[oldPos.Y][oldPos.X].Elements.Enemies)-1]
			rws.Grid.Matrix[oldPos.Y][oldPos.X].Elements.Enemies = rws.Grid.Matrix[oldPos.Y][oldPos.X].Elements.Enemies[:len(rws.Grid.Matrix[oldPos.Y][oldPos.X].Elements.Enemies)-1]

			ok = true
		}
	}
	// add petitpois to new position
	rws.Grid.Matrix[newPos.Y][newPos.X].Elements.Enemies = append(rws.Grid.Matrix[newPos.Y][newPos.X].Elements.Enemies, ppId)

	//MàJ des infos dans PetitpoisInfo
	copy := rws.PetitpoisInfo[ppId]
	copy.Position = newPos
	rws.PetitpoisInfo[ppId] = copy
	_biomeName := rws.Grid.Matrix[newPos.Y][newPos.X].BiomeName
	rws.Unlock()

	selectedEvent := "NeutralEvent"

	// trigger event selon le biome de déplacement (random basé sur luck cf equilibrage)
	if _biomeName != tribes.DefaultTileBiome {

		rws.Lock()
		dicoEvents := rws.Grid.EventBiomes[rws.Grid.Matrix[newPos.Y][newPos.X].BiomeName]
		rws.Unlock()

		// creation of the lottery wheel of events
		lotteryOfEvents := make([]string, 100) // to be more precise, the total number of allocated slots should be the sum of the proba of each events. We assume it fits in 100 slots
		slot := 0
		for eventName, proba := range dicoEvents {
			if proba > 0 {
				numAllocatedSlots := int(proba * 100)
				lastFilledSlot := slot + numAllocatedSlots
				for slot < lastFilledSlot {
					lotteryOfEvents[slot] = eventName
					slot++
				}
			}
		}
		for slot < len(lotteryOfEvents) {
			lotteryOfEvents[slot] = "NeutralEvent"
			slot++
		}

		rand.Seed(time.Now().UnixNano())
		randomSlot := rand.Intn(len(lotteryOfEvents))
		selectedEvent = lotteryOfEvents[randomSlot]
		respQuantity, respItemType := tribes.EventCalculator(req.Luck, selectedEvent)

		// send response

		resp := tribes.MoveResponse{
			AffectedStatByEvent: respItemType,
			Quantity:            respQuantity,
		}
		rws.Lock()
		rws.moveResponses[req.PetitpoisID] = resp
		rws.Unlock()
	}
}

// Selon que ce soit volontaire ou par la mort, ce n'est peut être pas le même comportement
// Add on the tile referenced a dropped item(material/food). It confirms to the Petitpois that this object is lost
//func (rws *RestServerAgent) handleDropItem(w http.ResponseWriter, r *http.Request) {}

// Give the item dropped on the referenced tile to the Petitpois. It then removes the item from the map
//func (rws *RestServerAgent) handleGetItem(w http.ResponseWriter, r *http.Request) {}

func (rws *RestServerAgent) handleIdle(req tribes.IdleRequest, statusError int, wg *sync.WaitGroup) {

	defer wg.Done()

	resp := tribes.IdleResponse{} // pretty empty (^u^')

	rws.Lock()
	rws.idleResponses[req.PetitpoisID] = resp
	rws.Unlock()
}

// Sends the information on the looked tiles asked by a Petitpois.
func (rws *RestServerAgent) handleIndividualSee(req tribes.SeeRequest, remainingTurns int, statusError int, wg *sync.WaitGroup) {

	defer wg.Done()

	// TODO relire
	pos := req.Position

	// // check si Position valide
	// if pos != rws.PetitpoisInfo[req.requestObject.PetitpoisID].Position {
	// 	errors.New("see : position of the petitpois doesn't match the server info")
	// }

	// calcul nombre de cases à montrer en fonction de la Perception
	// depuis la position, remplis une liste de TileBeliefs suivant une forme de losange sur la map
	rws.Lock()
	h, w := rws.Grid.Height(), rws.Grid.Width()
	_currenTurn := rws.CurrentTurn
	_currentDay := rws.CurrentDay
	_currentTile := rws.Grid.Matrix[pos.Y][pos.X]
	rws.Unlock()
	seenTilesPos := pos.RangeNeighborhood(h, w, req.Range)
	seenTilesBeliefs := []tribes.SeenTileBelief{} // don't forget current pos
	// fmt.Println(req.requestObject.PetitpoisID, " init seenTilesPos ", seenTilesPos)
	// fmt.Println(req.requestObject.PetitpoisID, " init seenTilesPBeliefs ", seenTilesBeliefs)
	// current seen position of the petitpois
	beliefOnPos := tribes.SeenTileBelief{
		Day:       rws.CurrentDay,
		Turn:      rws.CurrentTurn,
		Position:  pos,
		BiomeName: _currentTile.BiomeName,
		Elements:  _currentTile.Elements,
	}
	seenTilesBeliefs = append(seenTilesBeliefs, beliefOnPos)

	// seen tiles different from the position of the petitpois
	// fmt.Println("before")
	for ind := range seenTilesPos {

		rws.Lock()
		concernedTile := rws.Grid.Matrix[pos.Y][pos.X]
		rws.Unlock()
		unknownItems := map[tribes.ItemType]int{
			"Food":      -1,
			"Materials": -1,
		}
		ele := tribes.Element{
			Items:   unknownItems,
			Enemies: concernedTile.Elements.Enemies,
			Caravan: concernedTile.Elements.Caravan,
		}

		newTileBelief := tribes.SeenTileBelief{
			Day:       _currentDay,
			Turn:      _currenTurn,
			Position:  seenTilesPos[ind],
			BiomeName: concernedTile.BiomeName,
			Elements:  ele, // For now on, doesn't see dropped object from far away
		}
		seenTilesBeliefs = append(seenTilesBeliefs, newTileBelief)
	}
	// fmt.Println("after")

	// fmt.Println(req.requestObject.PetitpoisID, " filled exterior seenTilesPos ", seenTilesPos)
	// fmt.Println(req.requestObject.PetitpoisID, " filled exterior seenTilesPBeliefs ", seenTilesBeliefs)

	// fmt.Println(req.requestObject.PetitpoisID, " full seenTilesPBeliefs ", seenTilesBeliefs)

	// renvoie la réponse avec reaminingtruns
	resp := tribes.SeeResponse{
		RemainingTurns: remainingTurns,
		NewBeliefs:     seenTilesBeliefs,
	}

	rws.Lock()
	rws.seeResponses[req.PetitpoisID] = resp
	rws.Unlock()
}

// Remove a Petitpois from the list of all referenced Petitpois. It decreases the total number of Petitpois
func (rws *RestServerAgent) handleDeath(w http.ResponseWriter, r *http.Request) {

	// Recevoir la requete et la vérifier
	// Vérifie que la méthode est POST
	if !rws.checkMethod("POST", w, r) {
		return
	}
	// Décode le body JSON de la requête
	var req tribes.DieRequest
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err := json.Unmarshal(buf.Bytes(), &req)

	if err != nil {
		w.WriteHeader(http.StatusNotImplemented)
		w.Write([]byte(err.Error()))
		return
	}

	//Decrement total petitpois
	rws.Lock()
	pos := rws.PetitpoisInfo[req.PetitpoisID].Position
	rws.Unlock()

	ppId := req.PetitpoisID

	//Drop inventory
	rws.Lock()
	rws.Grid.Matrix[pos.Y][pos.X].Elements.Items["Food"] += req.Inventory.Food
	rws.Grid.Matrix[pos.Y][pos.X].Elements.Items["Materials"] += req.Inventory.Materials

	//Remove from Grid
	ok := false
	for ind := 0; ind < len(rws.Grid.Matrix[pos.Y][pos.X].Elements.Enemies) && !ok; ind++ {
		if rws.Grid.Matrix[pos.Y][pos.X].Elements.Enemies[ind] == ppId {
			rws.Grid.Matrix[pos.Y][pos.X].Elements.Enemies[ind] = rws.Grid.Matrix[pos.Y][pos.X].Elements.Enemies[len(rws.Grid.Matrix[pos.Y][pos.X].Elements.Enemies)-1]
			rws.Grid.Matrix[pos.Y][pos.X].Elements.Enemies = rws.Grid.Matrix[pos.Y][pos.X].Elements.Enemies[:len(rws.Grid.Matrix[pos.Y][pos.X].Elements.Enemies)-1]
		}
	}

	//Remove from petipoisinfos
	delete(rws.PetitpoisInfo, ppId)
	rws.Unlock()

	fmt.Println("Petitpois ", ppId, " is dead.")

	rws.Lock()
	rws.nbTotalPetitpois--
	fmt.Println("Nb ppt : ", rws.nbTotalPetitpois)
	rws.Unlock()
	if rws.stateActSee == "See" {
		rws.seeRequestChan <- tribes.SeeRequest{}
	} else {
		rws.actRequestChan <- tribes.ActRequest{}
	}

	resp := tribes.DieResponse{}

	w.WriteHeader(200)
	serial, _ := json.Marshal(resp)
	w.Write(serial)

}

/* ------METHODS FOR CARAVAN------- */

func (rws *RestServerAgent) doNightfallRequest(id tribes.CaravanID, currentDay tribes.Day, wg *sync.WaitGroup) (err error) {

	defer wg.Done()

	fmt.Println(id, " nightfall request ", currentDay)

	// Reset server channels just in case
	rws.stateActSee = "See"
	for len(rws.actRequestChan) > 0 {
		<-rws.actRequestChan
	}
	for len(rws.actResponseReadyChan) > 0 {
		<-rws.actResponseReadyChan
	}
	for len(rws.seeRequestChan) > 0 {
		<-rws.seeRequestChan
	}
	for len(rws.seeResponseReadyChan) > 0 {
		<-rws.seeResponseReadyChan
	}
	for len(rws.okChan) > 0 {
		<-rws.okChan
	}

	// Nightfall begining
	req := tribes.NightfallRequest{
		CurrentDay: currentDay,
	}

	// Sérialisation de la requête
	rws.Lock()
	url := rws.CaravansAddr[id] + "nightfall" // TODO : check endpoint
	data, _ := json.Marshal(req)
	rws.Unlock()

	// Envoi de la requête
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))

	// Traitement de la réponse
	if err != nil {
		log.Printf("failed to receive confirmation for nightfall response !")
		log.Print(err)
		return err
	}
	if resp.StatusCode != http.StatusOK {
		err = fmt.Errorf("[%d] %s", resp.StatusCode, resp.Status)
		log.Print(err)
		return err
	}

	defer resp.Body.Close()
	var newData tribes.NightfallResponse
	json.NewDecoder(resp.Body).Decode(&newData)
	if err != nil {
		print(err)
	}

	rws.Lock()

	rws.CaravansInfo[newData.CaravanID] = SimplifiedCaravanInfo{
		Position:     newData.CaravanPos,
		CaravanStats: newData.CaravanStats,
	}

	for id, role := range newData.Petitpois {
		rws.PetitpoisInfo[id] = SimplifiedPetitpoisInfo{
			Role:               role,
			Position:           newData.CaravanPos,
			BelongingCaravanID: newData.CaravanID,
		}
		fmt.Println(rws.PetitpoisInfo[id])
	}
	fmt.Println(rws.CaravansInfo[id])

	rws.nbTotalPetitpois += len(newData.Petitpois)
	fmt.Println(rws.nbTotalPetitpois)

	rws.Unlock()

	fmt.Println("Caravan ", newData.CaravanID, " is now at position ", newData.CaravanPos.Pos2String(), "\nIt possesses a tribe of ", len(newData.Petitpois), " petitpois.")

	return
}

/* ------Methods for Server------- */

// Test la validité du type de méthode
func (rws *RestServerAgent) checkMethod(method string, w http.ResponseWriter, r *http.Request) bool {
	if r.Method != method {
		w.WriteHeader(http.StatusMethodNotAllowed)
		fmt.Fprintf(w, "method %q not allowed", r.Method)
		return false
	}
	return true
}

func (rws *RestServerAgent) handleBasicGet(w http.ResponseWriter, r *http.Request) {
	rws.checkMethod("GET", w, r)
	fmt.Printf("simulation is consulted online\n")
}

// The function starts the server and creates the various endpoints of the app (Petitpois and Caravans)
func (rsa *RestServerAgent) Start() {
	//ici ou autre part pour générer la Map ? Surement dans le main
	// Création du multiplexer
	mux := http.NewServeMux()

	// TODO ajouter des endpoints pour chaque phase, caravan ?
	mux.HandleFunc("/act", rsa.handleActReception)
	mux.HandleFunc("/see", rsa.handleSeeReception)
	mux.HandleFunc("/die", rsa.handleDeath)
	mux.HandleFunc("/", rsa.handleBasicGet)

	mux.HandleFunc("/ws", rsa.handleWs)

	// Création du serveur http
	s := &http.Server{
		Addr:           ":" + rsa.addr,
		Handler:        mux,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20}

	// Lancement du serveur
	log.Println("Server : Listening on ", rsa.addr)
	go log.Fatal(s.ListenAndServe())
}
