package mapagent

import (
	"fmt"
	"math"
	"math/rand"
	"time"
	"tribes"
	rad "tribes"
	randu "tribes/randutil"
)

type Map struct {
	Matrix [][]rad.Tile
	Biomes map[string]rad.Biome
	EventBiomes map[string]rad.EventProbability
}

func NewMap(m [][]rad.Tile, b map[string]rad.Biome, e map[string]rad.EventProbability) *Map {
	return &Map{m, b, e}
}

// Return the height of the map
func (m *Map) Height() int {
	return len(m.Matrix)
}

// Return the width of the map
func (m *Map) Width() int {
	return len(m.Matrix[0])
}

// Return the area/number of tiles of the map
func (m *Map) Area() int {
	return (m.Height() * m.Width())
}

func (m *Map) DisplayMap() {
	fmt.Println("---MAP---")
	// symbols := []string{"O","M","J","S"}
	var UI = map[string]string{
		"desert":   " ",
		"oasis":    "O",
		"mountain": "M",
		"jungle":   "J",
		"savanna":  "S",
	}
	for _, v := range m.Matrix {
		row := ""
		for _, v2 := range v {
			row += UI[v2.BiomeName] + " "
		}
		fmt.Print(row, "\n")
	}
}

// TODO implement a DailyRegeneration function
// TODO add initial resource on map at full
func (m *Map) DailyRegeneration() {
	// Loop through the whole map
	height := m.Height()
	width := m.Width()
	for i:=0; i < height; i++ {
		for j:=0; j < width; j++ {
			// Add resources according to a ratio FoodLimit/MaterialsLimit
			name := m.Matrix[i][j].BiomeName
			foodLim := m.Biomes[name].FoodLimit
			materialsLim := m.Biomes[name].MaterialsLimit
			resources2Distribute := m.Biomes[name].RegenerationRate
			ratio := float64(foodLim) / float64(materialsLim)

			food2Distribute := int(math.Ceil(float64(resources2Distribute) * ratio))
			materials2Distribute := int(resources2Distribute - float32(food2Distribute))

			currentNaturalFood := m.Matrix[i][j].Elements.Items["NaturalFood"]
			if (foodLim - (food2Distribute + currentNaturalFood) >= 0) {
				m.Matrix[i][j].Elements.Items["NaturalFood"] += food2Distribute
				m.Matrix[i][j].Elements.Items["Food"] += food2Distribute
			} else {
				m.Matrix[i][j].Elements.Items["NaturalFood"] = foodLim
				m.Matrix[i][j].Elements.Items["Food"] += foodLim - currentNaturalFood
			}

			currentNaturalMaterials := m.Matrix[i][j].Elements.Items["NaturalMaterials"]
			if (materialsLim - (materials2Distribute + currentNaturalMaterials) >= 0) {
				m.Matrix[i][j].Elements.Items["NaturalMaterials"] += materials2Distribute
				m.Matrix[i][j].Elements.Items["Materials"] += materials2Distribute
			} else {
				m.Matrix[i][j].Elements.Items["NaturalMaterials"] = materialsLim
				m.Matrix[i][j].Elements.Items["Materials"] += materialsLim - currentNaturalMaterials
			}
		}

	}

}

// Create a given biome on the map according to a given spawn point.
// The pattern tends to be a circle centered on the spawn point.
func (m *Map) BiomeGenerationV1(biomeName string, spawnPoint rad.Position, freeCells *[]rad.Position) {
	// fmt.Println("--BiomeGenerationV1-- ", biomeName)
	rand.Seed(time.Now().UnixNano())

	// Variable initialization
	biome := m.Biomes[biomeName]
	biomeRim := make([]rad.Position, 0)
	choices := make([]randu.Choice, 0)
	string2Pos := make(map[string]rad.Position)

	deltaTiles := biome.NbTilesMax - biome.NbTilesMin
	tiles2Fill := rand.Intn(deltaTiles) + 1 + biome.NbTilesMin

	// Adding the spawnPoint of the biome in the map
	items := map[tribes.ItemType]int {
		"Food" : biome.FoodLimit,
		"Materials" : biome.MaterialsLimit,
	}

	ele := rad.Element {
		Items: items,
		Enemies: make([]rad.PetitpoisID, 0),
	}
	m.Matrix[spawnPoint.Y][spawnPoint.X] = rad.Tile{
		spawnPoint, 
		biome.Name, 
		ele,
	}

	// Initializing the rim of the biome and the weight of the random choices
	adjPos := spawnPoint.Adjacent(m.Height(), m.Width())
	for _, pos := range adjPos {
		x := pos.X
		y := pos.Y
		if m.Matrix[y][x].BiomeName == rad.DefaultTileBiome {
			biomeRim = append(biomeRim, pos) // same indexes for biomeRim positions and choices
			choices = append(choices, randu.Choice{tiles2Fill, pos.Pos2String()})
			string2Pos[pos.Pos2String()] = pos
		}
	}

	// Filling the map with the biome
	// fmt.Println("\n-Filling the map with the biome-")
	for tiles2Fill > 0 && len(biomeRim) > 0 {

		newTileChoice, _ := randu.WeightedChoice(choices)
		newTilePos := string2Pos[newTileChoice.Item]

		// Tile filling and removing position from the rim and free cells
		m.Matrix[newTilePos.Y][newTilePos.X] = rad.Tile{
			newTilePos, 
			biome.Name, 
			ele,
		}

		indRemove := 0
		for i, v := range biomeRim {
			if v == newTilePos {
				indRemove = i
				break
			}
		}
		biomeRim = rad.RemovePosition(biomeRim, indRemove)
		choices = randu.RemoveChoice(choices, indRemove)
		delete(string2Pos, newTilePos.Pos2String())

		indRemove = 0
		for i, v := range *freeCells {
			if v == newTilePos {
				indRemove = i
				break
			}
		}

		*freeCells = rad.RemovePosition(*freeCells, indRemove)
		// fmt.Println("biome filling, free cells remaining :", len(*freeCells))

		// Biome rim update
		adjPos := newTilePos.Adjacent(m.Height(), m.Width())
		for _, pos := range adjPos {
			x := pos.X
			y := pos.Y
			if m.Matrix[y][x].BiomeName == rad.DefaultTileBiome {
				// verifying that adjacent pos is not already in the biome rim
				found := false
				for _, v := range biomeRim {
					if v == pos {
						found = true
					}
				}
				// if not, we are adding it
				if !found {
					biomeRim = append(biomeRim, pos)
					choices = append(choices, randu.Choice{newTileChoice.Weight * 4 / 7, pos.Pos2String()})
					string2Pos[pos.Pos2String()] = pos
				}
			}
		}

		tiles2Fill--
	}
	// fmt.Println("--end of BiomeGenerationV1-- ", biomeName)
}

// Returns a Map object given its dimension, a list of biomes you want on the map, and the proportion of default biome(desert).
// This Map is generated randomly
func RandomMapGeneration(height, width int, biomes map[string]rad.Biome, events map[string]rad.EventProbability, defaultBiomeRatio float32) *Map {
	fmt.Println("--RandomMapGeneration--")

	rand.Seed(time.Now().UnixNano())

	// Empty matrix generation
	newMatrix := make([][]rad.Tile, height)
	for i := 0; i < height; i++ {
		newMatrix[i] = make([]rad.Tile, width)
	}

	// Number of biomes created on the map
	area := height * width
	nbBiomes := int((float32(area) - defaultBiomeRatio*float32(area))) / 15
	biomes2Distribute := nbBiomes

	fmt.Println("area : ", area)
	fmt.Println("nb biomes : ", nbBiomes)

	// Occurences of each biome in a map
	biomesOccurences := make([]string, 0)

	choices := make([]randu.Choice, 0)

	// biomes have no occurence at first and they have the same probability to be sorted
	for _, biome := range biomes {
		if biome.Name != rad.DefaultTileBiome {
			choices = append(choices, randu.Choice{biomes2Distribute, biome.Name})
		}
	}

	// fmt.Println("choices :\n", choices)
	// fmt.Println("number of biomesOccurences at first ", len(biomesOccurences))

	// Distribution of the occurences
	// we change the weight of the choice to improve the chance to have a biome of each type at least once
	for biomes2Distribute > 0 {
		// sorting randomly a biome name to be spawned
		result, _ := randu.WeightedChoice(choices)
		biomesOccurences = append(biomesOccurences, result.Item)
		
		// reducing the probability to sort this biome again
		ok := false
		for i := 0; i < len(choices) && !ok; i++ {
			if choices[i].Item == result.Item {
				oldWeight := choices[i].Weight
				choices = randu.RemoveChoice(choices, i)
				// fmt.Println(choices)
				newWeight := math.Max(float64(oldWeight)/2, 1)
				choices = append(choices, randu.Choice{int(newWeight), result.Item})
				ok = true
			}
		}

		biomes2Distribute--

	}
	// fmt.Println("biomesOccurences ", len(biomesOccurences), "\n", biomesOccurences)

	// Fill the map with Desert Biome, creating list of free cells
	freeCells := make([]rad.Position, 0)

	for i := 0; i < height; i++ {
		for j := 0; j < width; j++ {
			ele := rad.Element{
				Items: map[rad.ItemType]int{"Food": 0, "Materials": 0},
				Enemies: make([]rad.PetitpoisID, 0),
			}
			newMatrix[i][j] = rad.Tile{rad.Position{j, i}, rad.DefaultTileBiome, ele}
			freeCells = append(freeCells, rad.Position{j, i})
		}
	}

	// Biome generation
	newMap := NewMap(newMatrix, biomes, events)

	for len(biomesOccurences) > 0 && len(freeCells) > 0 {
		// fmt.Println("Still ", len(biomesOccurences), " biomes to spawn")

		// Choosing a new spawn point for the newcoming biome
		var spawnPoint rad.Position

		// Verifying that there is enough space around the spawn point
		ok := false
		for !ok {
			// fmt.Println("number of free cells: ", len(freeCells), " X total area: ", area)
			indexFreeSpawn := rand.Intn(len(freeCells))
			spawnPoint = freeCells[indexFreeSpawn]

			// If the spawn point is unexploitable OR if the spawn point is selected, we remove it from the free cells in any case
			freeCells = rad.RemovePosition(freeCells, indexFreeSpawn)
			adjSpawn := spawnPoint.Adjacent(height, width)
			nbAdjDesert := 0
			for i := 0; i < len(adjSpawn); i++ {
				x := adjSpawn[i].X
				y := adjSpawn[i].Y
				if newMap.Matrix[y][x].BiomeName == rad.DefaultTileBiome {
					nbAdjDesert++
				}
			}
			// If there is only one free cell around the spawn point, we retry
			if nbAdjDesert > 1 {
				ok = true
			}
		}

		// Selected biome generation
		nameBiomeSelected := biomesOccurences[0]

		// fmt.Println("spawn point ", spawnPoint, " is a ", nameBiomeSelected)
		newMap.BiomeGenerationV1(nameBiomeSelected, spawnPoint, &freeCells)
		biomesOccurences = rad.RemoveString(biomesOccurences, 0)
	}
	fmt.Println("full map generated")

	return newMap
}
