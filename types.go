package tribes

// ----- MAP RELATIVES TYPES -----//
type Position struct {
	X int //`json:"x"`
	Y int //`json:"y"`
}

// X correspond à Width correspond à j correspond à Matrix[] [j]
// Y correspond à Height correspond à i correspond à Matrix [i] []
// Matrix[pos.Y][pos.X]

type BiomeEvent struct {
	Probability float32
	Nature      string //A définir : perte/gain PV, perte/gain nourriture etc.
}

type Biome struct {
	Name                      string
	NbTilesMax, NbTilesMin    int
	RegenerationRate          float32
	FoodLimit, MaterialsLimit int
}

type ItemType string

type Tile struct {
	Position          //`json:"position"`
	BiomeName string  //`json:"biome-name"`
	Elements  Element //`json:"elements"`
}

type Element struct {
	Items          map[ItemType]int `json:"items"`
	ItemsAtArrival map[ItemType]int `json:"items-arrival"`
	Enemies        []PetitpoisID    `json:"enemies"`
	Caravan        CaravanID        `json:"caravan-id"`
}

func CompareElements(e1, e2 Element) bool {

	if e1.Items["Food"] != e2.Items["Food"] {
		return false
	} else if e1.Items["Materials"] != e2.Items["Materials"] {
		return false
	} else if len(e1.Enemies) != len(e2.Enemies) {
		return false
	} else if e1.Caravan != e2.Caravan {
		return false
	}

	for i, enemi := range e1.Enemies {
		if enemi != e2.Enemies[i] {
			return false
		}
	}

	return true
}

// A struct that contains all the information that a Petitpois can know about a tile at a specific turn. It is used to store the Belief of the Petitpois and to send it to the caravan.
type TileBelief struct {
	Day      Day      `json:"Day"`
	Turn     Turn     `json:"Turn"`
	Position Position `json:"Position"`
	Biome    string   `json:"Biome"`
	Elements Element  `json:"Elements"` //A définir : liste petitpois présents dessus, présence d'une caravane, présence de ressources au sol
	Danger   bool     `json:"Danger"`
}

// A type that stores all the beliefs of a Petitpois concerning the world.
type MapBelief map[Position]([]TileBelief)

// ----- PHASE RELATIVE TYPES ----- //

type Turn int
type Day int

// ----------- STRATEGIES ----------- //
type StratType map[string]float64

type Strategy struct {
	XPStrat        XPStrategy
	FoodStrat      FoodStrategy
	ResourcesStrat ResourcesStrategy
	RolesStrat     RolesStrategy
}

// XP allocation strategy ("Attack", "Defense", "Wisdom", "Stamina", "Perception", "Speed", "Gathering", "Luck")
type XPStrategy StratType

// Food repartition strategy ("Population", "Stamina", "Save")
type FoodStrategy StratType

// Materials repartition strategy ("Attack", "Defense" "Speed", "Gathering")
type ResourcesStrategy StratType

// Role created by the strategy
type RolesStrategy string

// ---- AGENTS ----//

type PetitpoisID string
type CaravanID string

type SeenTileBelief struct {
	Day       Day
	Turn      Turn
	Position  Position
	BiomeName string
	Elements  Element //A définir : liste petitpois présents dessus, présence d'une caravane, présence de ressources au sol
}
